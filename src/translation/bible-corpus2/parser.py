import xml.etree.ElementTree as ET
import os
from tqdm import tqdm
import pandas as pd
import subprocess

# parse language from file
def parse_language(lang):
    tree = ET.parse(lang)
    root = tree.getroot()

    txt = root[1]
    books = txt[0]
    
    n_missing = 0
    n_tot = 0
    books_by_book = {}
    for book in books:
        
        book_id = book.attrib['id']
        book_txt = ''

        for chapter in book:
            
            for verse in chapter:
                #print(verse.tag, verse.attrib, verse.text)
                n_tot += 1
                if verse.text is None:
                #    print(lang, verse.attrib)
                    n_missing += 1
                else:
                    book_txt += verse.text

        books_by_book[book_id] = book_txt.replace('\t', '')
    if n_missing/n_tot > 0.03:
        print('%f%% missing, %s' % (n_missing/n_tot*100, lang))
    return books_by_book

# compress given text using PAQ8A compression
def compress(txt):
    if os.path.exists('archive'):
        os.remove('archive')

    f = open('file.txt', 'w+')
    f.write(txt)
    f.close()
    
    subprocess.run(['./paq8a', '-0', 'archive', 'files', 'file.txt'], stdout=subprocess.DEVNULL)
#    os.system('./paq8a archive files file.txt')
    return os.stat('archive').st_size

by_lang = {}
chapter_names = set()
lang_names = []
compr = {}

# parse all texts
for fn in tqdm(os.listdir('Bibles')):
    if fn[-7:] != '-NT.xml' and (len(fn) < 9 or fn[-9:] != '-PART.xml'):
        
        lang = fn[:-4]
        lang_names.append(lang)
        by_lang[lang] = parse_language('Bibles/' + fn)
        
        for chapter_name in by_lang[lang].keys():
            chapter_names.add(chapter_name)
print(lang_names, chapter_names)

# calculation compression benefit
compr_len = pd.DataFrame(index=lang_names, columns = chapter_names)
for lang in tqdm(lang_names):
    compr[lang] = {}
    for chapter in by_lang[lang].keys():
        compr_len.loc[lang, chapter] = compress(by_lang[lang][chapter])

# write to csv
compr_len.to_csv('compression_lengths_per_chapter.csv')
print(compr)
