import xml.etree.ElementTree as ET
import os
import subprocess

import matplotlib.pyplot as plt
import seaborn as sns

from tqdm import tqdm

from random import shuffle

def parse_language(lang):
    tree = ET.parse(lang)
    root = tree.getroot()

    txt = root[1]
    books = txt[0]
    
    n_missing = 0
    n_tot = 0
    books_by_book = {}
    for book in books:
        
        book_id = book.attrib['id']
        book_txt = ''

        for chapter in book:
            
            for verse in chapter:
                #print(verse.tag, verse.attrib, verse.text)
                n_tot += 1
                if verse.text is None:
                #    print(lang, verse.attrib)
                    n_missing += 1
                else:
                    book_txt += verse.text

        books_by_book[book_id] = book_txt.replace('\t', '')
    if n_missing/n_tot > 0.03:
        print('%f%% missing, %s' % (n_missing/n_tot*100, lang))
    return books_by_book

books = set()
languages = []
by_lang = {}
for fn in os.listdir('Bibles'):
    if fn[-7:] != '-NT.xml' and (len(fn) < 9 or fn[-9:] != '-PART.xml') and (len(fn) < 11 or fn[-11:] != '-NT-SIL.xml'):
        languages.append(fn[:-4])
        by_lang[fn[:-4]] = parse_language('Bibles/' + fn)
        print(fn[:-4], len(by_lang[fn[:-4]].keys()))
        for book in by_lang[fn[:-4]].keys():
            books.add(book)
        print(len(by_lang[fn[:-4]]))
books = list(books)

shuffle(languages)
by_lang2 = {}
print(len(languages))
for lang in languages[:15]:
    by_lang2[lang] = by_lang[lang]

by_lang = by_lang2

shuffle(books)
books = books[:15]

class MatchingMethod:
    def rank(self, A, B):
        pass

    def metric(self, rank_matr):
        qual = 0
        mx = 0
        for i in range(len(rank_matr)):
            qual += rank_matr[i][i]**2
            
            mx = max(mx, rank_matr[i][i])
            if(rank_matr[i][i]==65):
                print(books[i])
        
        return qual/(len(books)*((len(books)-1)**2))

class MatchByLength(MatchingMethod):
    def rank(self, A, B):
        rank_matr = [len(books)*[0] for _ in range(len(books))]
        
        A_len = []
        B_len = []
        for book_i in range(len(books)):
            A_len.append((len(A[books[book_i]]), book_i))
            B_len.append((len(B[books[book_i]]), book_i))
        A_len.sort()
        B_len.sort()

        A_rank = len(books)*[0]
        B_rank = len(books)*[0]

        idx = 0
        for i in range(len(books)):
            (_, A_i) = A_len[idx]
            A_rank[A_i] = idx

            (_, B_i) = B_len[idx]
            B_rank[B_i] = idx
            idx += 1

        # for every book in language A
        for book_i in range(len(books)):

            # rank the books of language B by similarity
            A_len = A_rank[book_i]
            
            B_books_delta = []
            for book_j in range(len(books)):
                B_books_delta.append((abs(B_rank[book_j]-A_len), book_j))
            B_books_delta.sort()
            
            idx = 0
            for (length, book_j) in B_books_delta:
                if(books[book_j]=='b.PSA' and books[book_i]=='b.PSA' and idx==65):
                    print(length, idx, B_books_delta, book_i, len(A['b.PSA']), len(B['b.PSA']))
                rank_matr[book_i][book_j] = idx
                idx += 1
            
        return rank_matr
    
    def matching_quality(self, A, B):
        return self.metric(self.rank(A, B))

class MatchByLengthgzip(MatchingMethod):
    # compress given text using PAQ8A compression
    def compress(self, txt):
        if os.path.exists('archive'):
            os.remove('archive')

        f = open('file.txt', 'w+')
        f.write(txt)
        f.close()
        
        f = open('archive', 'w+')
        subprocess.run(['gzip', '-c', 'file.txt'], stdout=f)
        f.close()
    #    os.system('./paq8a archive files file.txt')
        return os.stat('archive').st_size
    


    def rank(self, A, B):
        rank_matr = [len(books)*[0] for _ in range(len(books))]
        
        A_len = []
        B_len = []
        for book_i in range(len(books)):
            A_len.append((self.compress(A[books[book_i]]), book_i))
            B_len.append((self.compress(B[books[book_i]]), book_i))
        A_len.sort()
        B_len.sort()

        A_rank = len(books)*[0]
        B_rank = len(books)*[0]

        idx = 0
        for i in range(len(books)):
            (_, A_i) = A_len[idx]
            A_rank[A_i] = idx

            (_, B_i) = B_len[idx]
            B_rank[B_i] = idx
            idx += 1

        # for every book in language A
        for book_i in range(len(books)):

            # rank the books of language B by similarity
            A_len = A_rank[book_i]
            
            B_books_delta = []
            for book_j in range(len(books)):
                B_books_delta.append((abs(B_rank[book_j]-A_len), book_j))
            B_books_delta.sort()
            
            idx = 0
            for (length, book_j) in B_books_delta:
                if(books[book_j]=='b.PSA' and books[book_i]=='b.PSA' and idx==65):
                    print(length, idx, B_books_delta, book_i, len(A['b.PSA']), len(B['b.PSA']))
                rank_matr[book_i][book_j] = idx
                idx += 1
            
        return rank_matr
    
    def matching_quality(self, A, B):
        return self.metric(self.rank(A, B))

class MatchByLengthbzip2(MatchingMethod):
    def compress(self, txt):
        if os.path.exists('file.txt.bz2'):
            os.remove('file.txt.bz2')

        f = open('file.txt', 'w+')
        f.write(txt)
        f.close()
        
        subprocess.run(['bzip2', 'file.txt'])
        f.close()
    #    os.system('./paq8a archive files file.txt')
        return os.stat('file.txt.bz2').st_size


    def rank(self, A, B):
        rank_matr = [len(books)*[0] for _ in range(len(books))]
        
        A_len = []
        B_len = []
        for book_i in range(len(books)):
            A_len.append((self.compress(A[books[book_i]]), book_i))
            B_len.append((self.compress(B[books[book_i]]), book_i))
        A_len.sort()
        B_len.sort()

        A_rank = len(books)*[0]
        B_rank = len(books)*[0]

        idx = 0
        for i in range(len(books)):
            (_, A_i) = A_len[idx]
            A_rank[A_i] = idx

            (_, B_i) = B_len[idx]
            B_rank[B_i] = idx
            idx += 1

        # for every book in language A
        for book_i in range(len(books)):

            # rank the books of language B by similarity
            A_len = A_rank[book_i]
            
            B_books_delta = []
            for book_j in range(len(books)):
                B_books_delta.append((abs(B_rank[book_j]-A_len), book_j))
            B_books_delta.sort()
            
            idx = 0
            for (length, book_j) in B_books_delta:
                if(books[book_j]=='b.PSA' and books[book_i]=='b.PSA' and idx==65):
                    print(length, idx, B_books_delta, book_i, len(A['b.PSA']), len(B['b.PSA']))
                rank_matr[book_i][book_j] = idx
                idx += 1
            
        return rank_matr
    
    def matching_quality(self, A, B):
        return self.metric(self.rank(A, B))


class MatchPAQ8A(MatchingMethod):
    # compress given text using PAQ8A compression
    def compress(self, txt):
        if os.path.exists('archive'):
            os.remove('archive')

        f = open('file.txt', 'w+')
        f.write(txt)
        f.close()
        
        subprocess.run(['./lpaq1', '9', 'file.txt', 'archive'])#, stdout=subprocess.DEVNULL)
    #    os.system('./paq8a archive files file.txt')
        return os.stat('archive').st_size
    
    def NCD(self, A, B):
        return (self.compress(A+B)-min(self.compress(A), self.compress(B)))/max(self.compress(A), self.compress(B))

    def rank(self, A, B):
        rank_matr = [len(books)*[0] for _ in range(len(books))]
        
        for book_i in range(len(books)):
            sorted_by_NCD = []
            for book_j in range(len(books)):

                book_A = A[books[book_i]]
                book_B = B[books[book_j]]
                
                sorted_by_NCD.append((self.NCD(book_A, book_B), book_j))

            sorted_by_NCD.sort()
            
            indx = 0
            for (_, book_j) in sorted_by_NCD:
                rank_matr[book_i][book_j] = indx
                indx += 1
        return rank_matr

    def matching_quality(self, A, B):
        return self.metric(self.rank(A, B))

class Matchgzip(MatchingMethod):
    # compress given text using PAQ8A compression
    def compress(self, txt):
        if os.path.exists('archive'):
            os.remove('archive')

        f = open('file.txt', 'w+')
        f.write(txt)
        f.close()
        
        f = open('archive', 'w+')
        subprocess.run(['gzip', '-c', 'file.txt'], stdout=f)
        f.close()
    #    os.system('./paq8a archive files file.txt')
        return os.stat('archive').st_size
    
    def NCD(self, A, B):
        A_c = self.compress(A)
        B_c = self.compress(B)
        return (self.compress(A+B)-min(A_c, B_c))/max(A_c, B_c)
    
    def rank(self, A, B):
        rank_matr = [len(books)*[0] for _ in range(len(books))]
        
        for book_i in range(len(books)):
            sorted_by_NCD = []
            for book_j in range(len(books)):

                book_A = A[books[book_i]]
                book_B = B[books[book_j]]
                
                sorted_by_NCD.append((self.NCD(book_A, book_B), book_j))

            sorted_by_NCD.sort()
            
            indx = 0
            for (_, book_j) in sorted_by_NCD:
                rank_matr[book_i][book_j] = indx
                indx += 1
        return rank_matr

    def matching_quality(self, A, B):
        return self.metric(self.rank(A, B))

class Matchbzip2(MatchingMethod):
    # compress given text using PAQ8A compression
    def compress(self, txt):
        if os.path.exists('file.txt.bz2'):
            os.remove('file.txt.bz2')

        f = open('file.txt', 'w+')
        f.write(txt)
        f.close()
        
        subprocess.run(['bzip2', 'file.txt'])
        f.close()
    #    os.system('./paq8a archive files file.txt')
        return os.stat('file.txt.bz2').st_size
    
    def NCD(self, A, B):
        A_c = self.compress(A)
        B_c = self.compress(B)
        return (self.compress(A+B)-min(A_c, B_c))/max(A_c, B_c)

    def rank(self, A, B):
        rank_matr = [len(books)*[0] for _ in range(len(books))]
        
        for book_i in range(len(books)):
            sorted_by_NCD = []
            for book_j in range(len(books)):

                book_A = A[books[book_i]]
                book_B = B[books[book_j]]
                
                sorted_by_NCD.append((self.NCD(book_A, book_B), book_j))

            sorted_by_NCD.sort()
            
            indx = 0
            for (_, book_j) in sorted_by_NCD:
                rank_matr[book_i][book_j] = indx
                indx += 1
        return rank_matr

    def matching_quality(self, A, B):
        return self.metric(self.rank(A, B))

for lang in by_lang:
    mn_length = 1e9
    for book in books:
        mn_length = min(len(by_lang[lang][book]), mn_length)

    for book in books:
        by_lang[lang][book] = by_lang[lang][book][:mn_length]
        print(len(by_lang[lang][book]), lang, book)

by_length = MatchByLengthgzip()


#print(by_length.metric(by_length.rank(by_lang['Afrikaans'], by_lang['Dutch'])))
#print(by_length.matching_quality(by_lang['Afrikaans'], by_lang['Dutch']))
#print(by_length.matching_quality(by_lang['Afrikaans'], by_lang['French']))

dists = [by_length.matching_quality(by_lang[key_A], by_lang[key_B]) for key_A in tqdm(by_lang.keys()) for key_B in tqdm(by_lang.keys())]
dists_idem = [by_length.matching_quality(by_lang[key_A], by_lang[key_A]) for key_A in by_lang.keys()]

print(dists, dists_idem)
# plot
sns.set()
fig, ax = plt.subplots(1, 2)
ax[0].hist(dists)
ax[1].hist(dists_idem)

ax[0].set_xlabel('Error')
ax[1].set_xlabel('Error')

ax[0].set_ylabel('Count')
ax[1].set_ylabel('Count')

plt.tight_layout()
plt.savefig('gzip_matching_length_norm.png', figsize=(12, 4))

print('Average: %f', sum(dists)/len(dists))
print('Average (idem): %f', sum(dists_idem)/len(dists_idem))
