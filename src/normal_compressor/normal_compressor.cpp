#include <iostream>
#include <fstream>
#include <cmath>
#include <filesystem>
#include <vector>

#include <chrono>

using namespace std;

vector<string> texts;

class Compressor {

public:
	Compressor(){}
    virtual long double compress(string A){}
};

class bzip2 : public Compressor {
public:
	bzip2(){}
    long double compress(string A){
		string folder = "bz2s/";
		string file_A = folder + "file_A";
		
		ofstream out1(file_A + ".txt");
		
		out1<<A;
		
		out1.close();
		
		system(("rm " + folder + "*" + ".txt.bz2").c_str()); 

		system(("bzip2 " + file_A + ".txt").c_str());
		
		long double C_A = filesystem::file_size(file_A + ".txt.bz2");
		return C_A;
    }
};

class gzip : public Compressor {
public:
	gzip(){}
	long double compress(string A){
		string folder = "gzips/";
		string file_A = folder + "file_A";

		ofstream out1(file_A + ".txt");

		out1 << A;

		out1.close();

		system(("rm " + folder + "*" + ".gz").c_str()); 

		system(("gzip -c " + file_A + ".txt > " + file_A + ".gz").c_str());
	
		long double C_A = filesystem::file_size(file_A + ".gz");
		return C_A;
	}
};

class LZMA: public Compressor {
public:
	LZMA(){}
	long double compress(string A){
		string folder = "lzmas/";
		string file_A = folder + "file_A";

		ofstream out1(file_A + ".txt");

		out1 << A;

		out1.close();

		system(("rm " + folder + "*" + ".txt.lzma").c_str()); 

		system(("lzma -1 -z " + file_A + ".txt").c_str());

		long double C_A = filesystem::file_size(file_A + ".txt.lzma");

		return C_A;
	}
};

class lpaq1_6mb: public Compressor {
public:
	lpaq1_6mb(){}
	long double compress(string A){
		string folder = "paqs/";
		string file_A = folder + "file_A";

		ofstream out1(file_A + ".txt");

		out1 << A;

		out1.close();

		system(("rm " + folder + "*" + ".txt.paq").c_str()); 

		system(("./lpaq1 0 " + file_A + ".txt " + file_A + ".txt.paq > trashcan").c_str());

		long double C_A = filesystem::file_size(file_A + ".txt.paq");

		return C_A;
	}
};

class ppmz: public Compressor {
public:
	ppmz(){}
	long double compress(string A){
		string folder = "ppmzs/";
		string file_A = folder + "file_A";

		ofstream out1(file_A + ".txt");

		out1 << A;

		out1.close();

		system(("rm " + folder + "*" + ".txt.ppmz").c_str()); 

		system(("./ppmz -c8 " + file_A + ".txt " + file_A + ".txt.ppmz  > trashcan 2> trashcan").c_str());

		long double C_A = filesystem::file_size(file_A + ".txt.ppmz");

		return C_A;
	}
};

long double idempotency_score(Compressor *C){
	long double score = 0.0;
	int n_texts = 0;
	for(string text: texts){
		n_texts++;
		long double c1 = C->compress(text);
		long double c2 = C->compress(text+text);

		long double denom = log2(8.0L*2.0L*((long double)text.size()));
		long double nom = 8.0L*abs(c1-c2);

		cout << denom << " " << nom << endl;
		score += pow(nom/denom, 2.0L);
	}

	return score/((long double)n_texts);
}

long double monotonicity_score(Compressor *C){
	long double score = 0.0;

	for(string text1: texts){
		for(string text2: texts){
			long double c1 = C->compress(text1 + text2);
			long double c2 = C->compress(text1);
			
			long double denom = log2(8.0L*((long double)max(text1.size(), text2.size())));
			long double nom = 8.0L*max(0.0L, c2-c1);

			cout << denom << " " << nom << endl;
			score += pow(nom/denom, 2.0L);
		}
	}
	return score/pow((long double)texts.size(), 2.0L);
}

long double symmetry_score(Compressor *C){
	long double score = 0.0;
	for(int i=0; i<texts.size(); i++){
		for(int j=0; j<texts.size(); j++){
			long double c1 = C->compress(texts[i] + texts[j]);
			long double c2 = C->compress(texts[j] + texts[i]);
			long double nom = 8.0L*abs(c1-c2);
			long double denom = log2(8.0L*((long double)max(texts[i].size(), texts[j].size())));
			cout << denom << " " << nom << endl;
		
			score += pow(nom/denom, 2.0L);
		}
	}
	return score/pow((long double)texts.size(), 2.0L);
}

long double distributivity_score(Compressor *C){
	long double score = 0.0;
	for(string x: texts){
		for(string y: texts){
			for(string z: texts){
				long double cxy = C->compress(x+y);
				long double cz = C->compress(z);
				long double cxz = C->compress(x+z);
				long double cyz = C->compress(y+z);
				
				long double denom = log2(8.0L*(((long double)max(max(x.length()+y.length(), z.length()), max(x.length()+z.length(), y.length()+z.length())))));
				long double nom = 8.0L*max((cxy+cz)-(cxz+cyz), 0.0L);
				score += pow(nom/denom, 2.0L);
			}
		}
	}
	return score/pow((long double)texts.size(), 3.0L);
}

uint64_t gettime() {
  using namespace std::chrono;
  return duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
}

long double speed(Compressor *C){
    long double tot = 0.0L, chars = 0.0L;
    for(string x: texts){
	for(string y: texts){
	    long double tmp = gettime();
	    C->compress(x);
	    C->compress(y);
	    C->compress(x+y);
	    tmp = gettime()-tmp;
	    tot += (long double)tmp;
	    chars += 2.0L*((long double)x.length())+2.0L*((long double)y.length());
	}
    }
    return tot/chars;
}
int main(){
	long double (*metrics[])(Compressor *C) = {idempotency_score};//, monotonicity_score, symmetry_score, distributivity_score};
	string metric_names[] = {"idempotency", "monotonicity", "symmetry", "distributivity"};
	const int N_METRICS = 1;

	Compressor* (compressors)[] = {new ppmz(), new lpaq1_6mb(), new bzip2(), new gzip(), new LZMA()};
	string compressor_names[] = {"ppmz", "lpaq1a_6mb", "bzip2", "gzip", "lzma"};
	const int N_COMPRESSORS = 5;

	for(auto const & entry: filesystem::directory_iterator("texts")){
		cout << entry.path() << endl;
		ifstream in(entry.path());

		string res = "", line;

		while(getline(in, line))
			res += line += "\n";
		texts.push_back(res);
	}

	for(int i=0; i<N_COMPRESSORS; i++){
		cout << "\t" << compressor_names[i];
	}
	cout << endl;
	for(int i=0; i<N_COMPRESSORS; i++){
	    /*cout << compressor_names[i] << endl;
	    for(int j=0; j<N_METRICS; j++){
		cout << metric_names[j] << endl;
		cout << metrics[j](compressors[i]) << endl;
	    }
	    cout << endl;*/
	    cout << "\t";
	    cout << speed(compressors[i]);
	}
	cout << endl;
	return 0;
}
