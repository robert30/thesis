import matplotlib.pyplot as plt
import seaborn as sns

compressors = ['bzip2', 'gzip', 'lzma']
compressors = ['ppmz', 'lpaq1a_6mb', 'bzip2', 'gzip', 'lzma']
metrics = ['idempotency']

per_metric = {x:{} for x in metrics}


c_i = 0
m_i = 0

current_metric = None
current_compressor = None

log = open('log_idem.txt').read().split('\n')
for txt in log:
    print(txt)
    if c_i < len(compressors) and txt == compressors[c_i]:
        current_compressor = compressors[c_i]
        c_i += 1
        m_i = 0
    
    elif m_i<len(metrics) and txt == metrics[m_i]:
        current_metric = metrics[m_i]
        m_i += 1
        per_metric[current_metric][current_compressor] = []

    else:
        if current_compressor not in compressors:
            continue
        nums = txt.split(' ')

        if len(nums) == 2:
            per_metric[current_metric][current_compressor].append((float(nums[0]), float(nums[1])))

fig, ax = plt.subplots(2, 2)

for metric_i in range(len(metrics)):
    metric = metrics[metric_i]

    ax[metric_i//2][metric_i%2].set_title(metric)

    for compressor in compressors:
        xv = [p[0] for p in per_metric[metric][compressor]]
        yv = [p[1] for p in per_metric[metric][compressor]]
        sns.regplot(ax=ax[metric_i // 2][metric_i%2], x=xv, y=yv, fit_reg=True, scatter=True, order=2, label=compressor, marker='.')

    ax[metric_i//2][metric_i%2].legend()
    ax[metric_i//2][metric_i%2].set_xlabel('max log(n)')
    ax[metric_i//2][metric_i%2].set_ylabel('Error')
fig.set_figheight(8)
fig.set_figwidth(12)
plt.tight_layout()
plt.savefig('error_idem.png')
plt.show()
