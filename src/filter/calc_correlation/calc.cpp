#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
#include <tuple>

#include <chrono>
#include <random>
#include <filesystem>

#include <algorithm>

#include <omp.h>

using namespace std;

const int N_AUTHORS = 5;
const int N_WORKS_PER_AUTHOR = 4;
const int N_POS = 18;
const int N_PARAMS = N_POS*2+2;
const int N_EPOCHS = 5;


const int N_POPULATION = 64;

const long double PARAMETER_AMPLITUDE = 2.0;

const int MUTATION_RATE = 10*N_PARAMS; // mutate once in 10N_PARAMS digits
const int N_KEEP = N_POPULATION/8; // number to keep by elitist selection
const int CROSSOVER_RATE = 1; //how often to crossover

string AUTHORS[] = {"austen", "conan_doyle", "dickens", "twain", "wilde"};
string BOOKS_PER_AUTHOR[][4] = {{"1_pride_and_prejudice", "2_emma", "3_persuasion", "4_sense_and_sensibility"},
			{"1_adventures_sherlock", "2_baskervilles", "3_study_in_scarlet", "4_sign_of_the_four"},
			{"1_two_cities", "2_christmas_carol", "3_great_expectations", "4_oliver_twist"},
			{"1_tom_sawyer", "2_huckleberry_finn", "3_prince_and_pauper", "4_innocents_abroad"},
			{"1_dorian_gray", "2_earnest", "3_happy_prince", "4_de_profundis"}};
string POS[] = {"ADJ", "ADP", "ADV", "AUX", "CCONJ", "CONJ", "DET", "INTJ", "NOUN", "NUM", "PART", "PRON", "PROPN", "PUNCT", "SCONJ", "SYM", "VERB", "X"};
vector<vector<vector<tuple<string, int, long double> > > > works;
ofstream out("log.txt");

template <typename T1, typename T2>
std::ostream& operator<< (std::ostream& out, const std::pair<T1, T2>& v) {
    cout << "{" << v.first << ", " << v.second << "}" << endl;
    return out;
}

template <typename T>
std::ostream& operator<< (std::ostream& out, const std::vector<T>& v) {
    cout << "[";

    for(int n=0; n<v.size(); n++){
	if(n)
	    out << " ";
	out << v[n];
    }
    cout << "]" << endl;
    return out;
}

long double NCD(string A, string B){
    int thread_id = omp_get_thread_num();
    string file_A = "file_A_" + to_string(thread_id);
    string file_B = "file_B_" + to_string(thread_id);
    string file_AB = "file_AB_" + to_string(thread_id); 
    
    ofstream out1(file_A + ".txt");
    ofstream out2(file_B + ".txt");
//    ofstream out3("file_C.txt");
    out1<<A;
    out2<<B;
//    out3<<A<<B;
    out1.close();
    out2.close();
//    out3.close();
    
    
    system(("rm *_" + to_string(thread_id) + ".txt.bz2").c_str()); 

    system(("tar cfj " + file_AB + ".txt.bz2 " +  file_A + ".txt " + file_B + ".txt").c_str());
    system(("bzip2 " + file_A + ".txt").c_str());
    system(("bzip2 " + file_B + ".txt").c_str());
//    system("bzip2 file_C.txt");

    long double C_A = filesystem::file_size(file_A + ".txt.bz2");
    long double C_B = filesystem::file_size(file_B + ".txt.bz2");
    long double C_AB = filesystem::file_size(file_AB + ".txt.bz2");
//    cout << C_A << " " << C_B << " " << C_AB << " " << endl; //filesystem::file_size("file_C.txt.bz2") << endl;
    return (C_AB-min(C_A, C_B))/max(C_A, C_B);
}

bool keep(int pos_idx, long double freq, vector<long double>& p){
    if(pos_idx==-1)
	return true;
    return /*p[0] + freq*p[1] +*/ p[2*(pos_idx+1)] + p[2*(pos_idx+1)+1]*freq >=-1e-9L;
}

int calc_appearance(int POS_i){
    int tot = 0;
    for(vector<vector<tuple<string, int, long double> > > author: works){
	for(vector<tuple<string, int, long double> > work: author){
	    for(auto const& [token, pos_idx, freq]: work){
		tot += (pos_idx==POS_i);
	    }
	}
    }
    return tot;
}

string txt_from_params(vector<tuple<string, int, long double> >& work, vector<long double>& p){
    string txt = "";
    int ndel = 0;
    for(auto const& [token, pos_idx, freq] : work){
	if(keep(pos_idx, freq, p))
	    txt += token;
	else {
	    txt += string(token.length(), '*');
	    ndel++;
	}
    }
    return txt;
}

long double eval(vector<long double>& params){
    vector<vector<string> > texts(works.size(), vector<string>());

    long double res = 0.0L;
    
    for(int auth=0; auth<works.size(); auth++){
	for(auto work: works[auth]){
	    texts[auth].push_back(txt_from_params(work, params));
	}
    }
    for(int auth_1=0; auth_1<works.size(); auth_1++){
	for(int auth_2=auth_1+1; auth_2<works.size(); auth_2++){
	    if(auth_1==auth_2)
		continue;
	    for(int work_1=0; work_1<works[auth_1].size(); work_1++){
		for(int work_2=0; work_2<works[auth_2].size(); work_2++){
		    res += 2*pow(1.0L - NCD(texts[auth_1][work_1], texts[auth_2][work_2]), 2.0L);
		}
	    }
	}
    }

    for(int auth=0; auth<works.size(); auth++){
	for(int work_1=0; work_1<works[auth].size(); work_1++){
	    for(int work_2=0; work_2<works[auth].size(); work_2++){
		res += (works.size()-1)*pow(NCD(texts[auth][work_1], texts[auth][work_2]), 2.0L);
	    }
	}
    }
    return res;
}

uint64_t gettime() {
  using namespace std::chrono;
  return duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
}

class ProgressBar {
private:
    int n_items, width = 100;
    int n_items_seen = 0;
    int start_time;

    string format(int seconds){
	string res = "";
	if(seconds>=60)
	    res += to_string(seconds/60) + "m";
	res += to_string(seconds%60) + "s";
	return res;
    }
public:
    void print(){
	double n_seen = ((double)this->n_items_seen)/((double) this->n_items);
	cout << "\r[" << string((int)(n_seen*((double)this->width))-1, '=') << ">" << string(this->width-(int)(n_seen*((double)this->width)), '-')  << ']';

	int time_passed = gettime()-this->start_time;

	int total_time = this->n_items * (time_passed/this->n_items_seen);

	cout << " [" << this->format(time_passed/1000.0) << "|" << this->format((total_time-time_passed)/1000.0) << " to go] (" << this->n_items_seen << " out of " << this->n_items << " items, " << this->format((time_passed/this->n_items_seen)/1000.0) << "/item)";
	
    }

    ProgressBar(int n_items){
	this->n_items = n_items;
	this->start_time = gettime();
	cout << '[' << string(this->width, '-') << ']';
    }
    ProgressBar(int n_items, int width){
	this->n_items = n_items;
	this->width = width;
	this->start_time = gettime();
	cout << '[' << string(this->width, '-') << ']';
    }


    void update(){
	#pragma omp atomic
	this->n_items_seen += 1;
	#pragma omp critical	
	this->print();
    }

    void close(){
	cout << endl;
    }
};


vector<pair<long double, int> > test(vector<vector<long double> > to_test){
    vector<pair<long double, int> > res(to_test.size());
    
    ProgressBar pb(to_test.size());

    #pragma omp parallel for schedule(static) 
    for(int n=0; n<to_test.size(); n++){
	res[n] = {eval(to_test[n]), n};
	pb.update();
    }
    pb.close();	

    return res; 
}

int main(){
    
    omp_set_num_threads(8);

    works.assign(N_AUTHORS, vector<vector<tuple<string, int, long double> > >(N_WORKS_PER_AUTHOR, vector<tuple<string, int, long double> >()));

    for(int i=0; i<N_AUTHORS; i++){
	for(int j=0; j<N_WORKS_PER_AUTHOR; j++){
	    ifstream in("../books_processed_cpp/" + AUTHORS[i] + "/" + BOOKS_PER_AUTHOR[i][j] + ".txt");
	    

	    string token;
	    int tokentype;
	    long double freq;
	    string line;

	    while(getline(in, line)){
		stringstream sstream;
		sstream << line;
		sstream>>token>>tokentype>>freq;
		works[i][j].push_back(make_tuple(token.substr(1, token.length()-2), tokentype, freq));
	    }
	}
    }
    
    for(int i=0; i<N_POS; i++){
	cout << POS[i] << " " << calc_appearance(i) << endl;
    }
    vector<long double> params(N_PARAMS, 1);
    long double val_0 = eval(params);
    cout << val_0 << endl; 
    vector<vector<long double> > to_test(N_POS, vector<long double>(N_PARAMS, 0));
    params[0] = 0; 
    for(int pos_i=0; pos_i < N_POS; pos_i++){
	to_test[pos_i][2*(pos_i+1)] = -1;
    }
    cout << to_test << endl;
    vector<pair<long double, int> > res = test(to_test);
    for(int i=0; i<res.size(); i++){
	cout << POS[i] << " " << res[i].first << " " << res[i].first-val_0 << " Appearances: " << calc_appearance(i) << endl;
	if(res[i].first-val_0 <= -2.0L)
	    params[2*(i+1)] = -1;
    }
    cout << eval(params) << endl;
    return 0;
    for(int param_i=1; param_i < params.size(); param_i++){
	// vary params[param_i], select optimum
	long double val = -2.0L;
	for(int j=0; j<=20; j++, val += 0.2L){
	    to_test[j] = params;
	    to_test[j][param_i] = val;
	}
	vector<pair<long double, int> > res = test(to_test);
	sort(res.begin(), res.end());
	reverse(res.begin(), res.end());
	cout << res << endl;
	params[param_i] = to_test[res[0].second][param_i];
	cout << res[0].first << " " << params << endl;

    }
    cout << "done" << endl;
    
    return 0;
}
