import matplotlib.pyplot as plt
import seaborn as sns
sns.set()
from matplotlib.ticker import MaxNLocator
POS = ['ADJ', 'ADP', 'ADV', 'AUX', 'CCONJ', 'DET', 'INTJ', 'NOUN', 'NUM', 'PART', 'PRON', 'PROPN', 'PUNCT', 'SCONJ', 'SYM', 'VERB', 'X']

def getloss(txt):
    pos_idx = 0
    loss = [float(txt[0][6:])]
    for line_i in range(len(txt)):
        if txt[line_i][:len(POS[pos_idx])] == POS[pos_idx]:
            print(txt[line_i], POS[pos_idx])
#            loss.append(float(txt[line_i+2]))
            pos_idx += 1
            if(pos_idx == len(POS)):
                break
    return loss

#f1 = open('log_asterisks.txt')
#txt =f1.read().split('\n')
#f1.close()

#loss_f1 = getloss(txt)

f2 = open('log2.txt')
#f2 = open('log_maximizing.txt')
txt =f2.read().split('\n')
f2.close()

loss_f2 = getloss(txt)


#plt.plot(list(range(len(loss_f2))), loss_f2, label='Removed')
#plt.plot(list(range(len(loss_f1))), loss_f1, label='Asterisks')
#plt.axes().xaxis.set_major_locator(MaxNLocator(integer=True))
#plt.legend()
#plt.xlabel('Iterations')
#plt.ylabel('Loss')
#plt.savefig('loss_over_time.png')
#plt.show()
