#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
#include <tuple>

#include <chrono>
#include <random>
#include <filesystem>

#include <algorithm>

#include <omp.h>

using namespace std;

const int N_AUTHORS = 5;
const int N_WORKS_PER_AUTHOR = 4;
const int N_POS = 18;
const int N_PARAMS = N_POS*2+2;
const int N_EPOCHS = 5;


const int N_POPULATION = 64;

const long double PARAMETER_AMPLITUDE = 2.0;

const int MUTATION_RATE = 10*N_PARAMS; // mutate once in 10N_PARAMS digits
const int N_KEEP = N_POPULATION/8; // number to keep by elitist selection
const int CROSSOVER_RATE = 1; //how often to crossover

string AUTHORS[] = {"austen", "conan_doyle", "dickens", "twain", "wilde"};
string BOOKS_PER_AUTHOR[][4] = {{"1_pride_and_prejudice", "2_emma", "3_persuasion", "4_sense_and_sensibility"},
			{"1_adventures_sherlock", "2_baskervilles", "3_study_in_scarlet", "4_sign_of_the_four"},
			{"1_two_cities", "2_christmas_carol", "3_great_expectations", "4_oliver_twist"},
			{"1_tom_sawyer", "2_huckleberry_finn", "3_prince_and_pauper", "4_innocents_abroad"},
			{"1_dorian_gray", "2_earnest", "3_happy_prince", "4_de_profundis"}};
string POS[] = {"ADJ", "ADP", "ADV", "AUX", "CCONJ", "CONJ", "DET", "INTJ", "NOUN", "NUM", "PART", "PRON", "PROPN", "PUNCT", "SCONJ", "SYM", "VERB", "X"};

vector<vector<vector<tuple<string, int, long double> > > > works;
ofstream out("log.txt");

long double NCD(string A, string B){
    int thread_id = omp_get_thread_num();
    string file_A = "file_A_" + to_string(thread_id);
    string file_B = "file_B_" + to_string(thread_id);
    string file_AB = "file_AB_" + to_string(thread_id); 
    
    ofstream out1(file_A + ".txt");
    ofstream out2(file_B + ".txt");
//    ofstream out3("file_C.txt");
    out1<<A;
    out2<<B;
//    out3<<A<<B;
    out1.close();
    out2.close();
//    out3.close();
    
    
    system(("rm *_" + to_string(thread_id) + ".txt.bz2").c_str()); 

    system(("tar cfj " + file_AB + ".txt.bz2 " +  file_A + ".txt " + file_B + ".txt").c_str());
    system(("bzip2 " + file_A + ".txt").c_str());
    system(("bzip2 " + file_B + ".txt").c_str());
//    system("bzip2 file_C.txt");

    long double C_A = filesystem::file_size(file_A + ".txt.bz2");
    long double C_B = filesystem::file_size(file_B + ".txt.bz2");
    long double C_AB = filesystem::file_size(file_AB + ".txt.bz2");
//    cout << C_A << " " << C_B << " " << C_AB << " " << endl; //filesystem::file_size("file_C.txt.bz2") << endl;
    return (C_AB-min(C_A, C_B))/max(C_A, C_B);
}

bool keep(int pos_idx, long double freq, vector<long double>& p){
    if(pos_idx==-1)
	return true;
    return /*p[0] + freq*p[1] +*/ p[2*(pos_idx+1)] + p[2*(pos_idx+1)+1]*freq >=-1e-9L;
}

string txt_from_params(vector<tuple<string, int, long double> >& work, vector<long double>& p){
    string txt = "";
    for(auto const& [token, pos_idx, freq] : work){
	if(keep(pos_idx, freq, p))
	    txt += token;
    }
    return txt;
}

long double eval(vector<long double>& params){
    vector<vector<string> > texts(works.size(), vector<string>());

    long double res = 0.0L;
    
    for(int auth=0; auth<works.size(); auth++){
	for(auto work: works[auth]){
	    texts[auth].push_back(txt_from_params(work, params));
	}
    }
    for(int auth_1=0; auth_1<works.size(); auth_1++){
	for(int auth_2=auth_1+1; auth_2<works.size(); auth_2++){
	    if(auth_1==auth_2)
		continue;
	    for(int work_1=0; work_1<works[auth_1].size(); work_1++){
		for(int work_2=0; work_2<works[auth_2].size(); work_2++){
		    res += 2*pow(1-NCD(texts[auth_1][work_1], texts[auth_2][work_2]), 2.0L);
		}
	    }
	}
    }

    for(int auth=0; auth<works.size(); auth++){
	for(int work_1=0; work_1<works[auth].size(); work_1++){
	    for(int work_2=0; work_2<works[auth].size(); work_2++){
		res += (works.size()-1)*pow(NCD(texts[auth][work_1], texts[auth][work_2]), 2.0L);
	    }
	}
    }
    return res;
}
template <typename T1, typename T2>
std::ostream& operator<< (std::ostream& out, const std::pair<T1, T2>& v) {
    cout << "{" << v.first << ", " << v.second << "}" << endl;
    return out;
}

template <typename T1, typename T2, typename T3>
std::ostream& operator<< (std::ostream& out, const std::tuple<T1, T2, T3>& v) {
    cout << "{" << get<0>(v) << ", " << get<1>(v) << ", " << get<2>(v) << "}" << endl;
    return out;
}

template <typename T>
std::ostream& operator<< (std::ostream& out, const std::vector<T>& v) {
    cout << "[";

    for(int n=0; n<v.size(); n++){
	if(n)
	    out << " ";
	out << v[n];
    }
    cout << "]" << endl;
    return out;
}

uint64_t gettime() {
  using namespace std::chrono;
  return duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
}

class ProgressBar {
private:
    int n_items, width = 100;
    int n_items_seen = 0;
    int start_time;

    string format(int seconds){
	string res = "";
	if(seconds>=60)
	    res += to_string(seconds/60) + "m";
	res += to_string(seconds%60) + "s";
	return res;
    }
public:
    void print(){
	double n_seen = ((double)this->n_items_seen)/((double) this->n_items);
	cout << "\r[" << string((int)(n_seen*((double)this->width))-1, '=') << ">" << string(this->width-(int)(n_seen*((double)this->width)), '-')  << ']';

	int time_passed = gettime()-this->start_time;

	int total_time = this->n_items * (time_passed/this->n_items_seen);

	cout << " [" << this->format(time_passed/1000.0) << "|" << this->format((total_time-time_passed)/1000.0) << " to go] (" << this->n_items_seen << " out of " << this->n_items << " items, " << this->format((time_passed/this->n_items_seen)/1000.0) << "/item)";
	
    }

    ProgressBar(int n_items){
	this->n_items = n_items;
	this->start_time = gettime();
	cout << '[' << string(this->width, '-') << ']';
    }
    ProgressBar(int n_items, int width){
	this->n_items = n_items;
	this->width = width;
	this->start_time = gettime();
	cout << '[' << string(this->width, '-') << ']';
    }


    void update(){
	#pragma omp atomic
	this->n_items_seen += 1;
	#pragma omp critical	
	this->print();
    }

    void close(){
	cout << endl;
    }
};


vector<pair<long double, int> > test(vector<vector<long double> > to_test){
    vector<pair<long double, int> > res(to_test.size());
    
    ProgressBar pb(to_test.size());

    #pragma omp parallel for schedule(static) 
    for(int n=0; n<to_test.size(); n++){
	res[n] = {eval(to_test[n]), n};
	pb.update();
    }
    pb.close();	

    return res; 
}

int main(){
    
    omp_set_num_threads(8);

    works.assign(N_AUTHORS, vector<vector<tuple<string, int, long double> > >(N_WORKS_PER_AUTHOR, vector<tuple<string, int, long double> >()));

    for(int i=0; i<N_AUTHORS; i++){
	for(int j=0; j<N_WORKS_PER_AUTHOR; j++){
	    ifstream in("../books_processed_cpp/" + AUTHORS[i] + "/" + BOOKS_PER_AUTHOR[i][j] + ".txt");
	    

	    string token;
	    int tokentype;
	    long double freq;
	    string line;

	    while(getline(in, line)){
		stringstream sstream;
		sstream << line;
		sstream>>token>>tokentype>>freq;
		works[i][j].push_back(make_tuple(token.substr(1, token.length()-2), tokentype, freq));
	    }
	}
    }
    
    vector<long double> params(N_PARAMS, 0);
    vector<long double> new_params(N_PARAMS, 0);
    long double val_0 = eval(params);
    
        

    for(int i=0; i<N_POS; i++){
	
	vector<tuple<long double, long double, long double> > opt_param;
	// range alpha over -1, 0 and 1
	for(long double alpha = -0.1L; alpha < 0.3L; alpha += 0.2L){
	    vector<vector<long double> > to_test(6, params);

	    int indx = 0;
	    vector<long double> vals;

	    long double beta_begin = -5.0, beta_end = 5.5L;
	    if(alpha<0.0L) //makes no sense to consider negative beta if alpha<0, everything rejected anyway
		beta_begin = 0.0L;
	    if(alpha>0.0L) //makes no sense to consider positive beta for positive alpha, everything accepted anyway
		beta_end = 0.5L;

	    for(long double beta=beta_begin; beta<beta_end; beta += 1.0L){
		to_test[indx][2*(i+1)] = alpha;
		to_test[indx][2*(i+1)+1] = beta;
		vals.push_back(beta);
	    }
	    vector<pair<long double, int> > res = test(to_test);
	    
	    sort(res.begin(), res.end());
	    to_test.assign(33, params);
	    
	    cout << "res1: " << res << endl; 

	    vector<long double> vals2;
	    indx = 0;
	    for(int t=0; t<3; t++){
		long double old_beta = vals[res[t].second];
		for(long double beta=old_beta - 0.5L; beta < old_beta+0.55L; beta += 0.1L, indx++){
		    cout << beta << endl;
		    to_test[indx][2*(i+1)] = alpha;
		    to_test[indx][2*(i+1)+1] = beta;
		    vals2.push_back(beta);
		}
	    }
	    cout << to_test << endl; 
	    res = test(to_test);
	    sort(res.begin(), res.end());
	    cout << "res2 " << res << endl;
	    to_test.assign(33, params);
	    vals.resize(0);
	    indx = 0;
	    for(int t=0; t<3; t++){
		long double old_beta = vals2[res[t].second];
		for(long double beta=old_beta-0.05L; beta < old_beta+0.055L; beta += 0.01L, indx++){
		    cout << beta << endl;
		    to_test[indx][2*(i+1)] = alpha;
		    to_test[indx][2*(i+1)+1] = beta;
		    vals.push_back(beta);
		}
	    }

	    res = test(to_test);
	    sort(res.begin(), res.end());
	    
	    cout << "finres" << " " << res << endl;
	    opt_param.push_back(make_tuple(res[0].first, alpha, vals[res[0].second]));
	}

	sort(opt_param.begin(), opt_param.end());
	cout << POS[i] << " " << opt_param[0] << endl;
	out << POS[i] << " " << opt_param[0] << endl;
	new_params[2*(i+1)] = get<1>(opt_param[0]);
	new_params[2*(i+1)+1] = get<2>(opt_param[0]);
	params[2*(i+1)] = get<1>(opt_param[0]);
	params[2*(i+1)+1] = get<2>(opt_param[0]);
    } 
    long double newv = eval(new_params);
    cout << newv << " " << " improvement: " << newv-val_0 << endl;
    cout << new_params << endl;  
    return 0;
}
