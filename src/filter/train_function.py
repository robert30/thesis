import os
import spacy
from wordfreq import word_frequency
from tqdm import tqdm
from textdistance import lzma_ncd as NCD
import numpy as np

nlp = spacy.load('en_core_web_sm', exclude=['parser', 'ner', 'lemmatizer'])
nlp.max_length = 2000000
print(nlp.pipeline)
books_by_author = {}

POS = ['ADJ', 'ADP', 'ADV', 'AUX', 'CCONJ', 'CONJ', 'DET', 'INTJ', 'NOUN', 'NUM', 'PART', 'PRON', 'PROPN', 'PUNCT', 'SCONJ', 'SYM', 'VERB', 'X']
POS2idx = {pos: POS.index(pos) for pos in POS}

read_from_cache = False
write_to_cpp = True

for author in tqdm(os.listdir('./books')):
    if read_from_cache:
        break

    books_by_author[author] = {}

    for book in os.listdir('./books/%s' % author):
        f = open('./books/%s/%s' % (author, book))
        books_by_author[author][book] = nlp(f.read())
        f.close()
        print(author, book)
        books_by_author[author][book].to_disk('./books_processed/%s/%s.spacy' % (author, book)) 
#        books_by_author_freq[author][book] = [word_frequency(token.text, 'en') for token in books_by_author[author][book]]

for author in tqdm(os.listdir('./books_processed')):
    if not read_from_cache:
        break

    books_by_author[author] = {}

    for book in os.listdir('./books_processed/%s' % author):
        if book[-5:] != 'spacy':
            continue
        book = book[:-6]
        books_by_author[author][book] = nlp('').from_disk('./books_processed/%s/%s.spacy' % (author, book))

def get_code(pos):
    if pos=='SPACE':
        return -1
    else:
        return POS2idx[pos]

for author in tqdm(books_by_author):
    for book in books_by_author[author]:
        books_by_author[author][book] = [(token.text, token.pos_, word_frequency(token.text, 'en')) for token in books_by_author[author][book]]
        if not write_to_cpp:
            continue
        f = open('./books_processed_cpp/%s/%s' % (author, book), 'w+')
        f.write('\n'.join(['%s %d %f' % (repr(token[0]), get_code(token[1]), token[2]) for token in books_by_author[author][book]]))
        f.close()

def not_omit(token, params):
    if token[1] not in POS:
        return True
    idx = POS2idx[token[1]]
    frq = token[2]
    return (params[0]*frq + params[2*idx] + params[2*idx+1]*frq) > 0

def txt_from_params(doc, params):
    txt = ''
    for token in doc:
        if not_omit(token, params):
            txt += token[0]
    return txt

def evaluate(p):
    result = 0
    
    txt = {}
    for author in tqdm(books_by_author):
        txt[author] = []
        for book_i in books_by_author[author]:
            txt[author].append(txt_from_params(books_by_author[author][book_i], p))

    for author_1 in tqdm(books_by_author):
        for author_2 in books_by_author:
            if author_1 == author_2:
                continue

            for book_i in range(len(books_by_author[author_1])):
                for book_j in range(len(books_by_author[author_2])):
                    result += NCD(txt[author_1][book_i], txt[author_2][book_j])
    
    for author in books_by_author:
        for book_i in range(len(books_by_author[author])):
            for book_j in range(len(books_by_author[author])):
                result += 1-NCD(txt[author][book_i], txt[author][book_j])
    return result

N_POPULATION = 16
N_EPOCHS = 5

gene_pool = np.random.rand(N_POPULATION, 1+2*len(POS))

for i in tqdm(range(N_EPOCHS)):
    # evaluate
    metr = evaluate(gene_pool[0, :])
    metr_sorted = list(metr)
    metr_sorted.sort()

    threshold = metr_sorted[(N_POPULATION/4)-(N_POPULATION % 4)]

    
    print(metr)
    break
