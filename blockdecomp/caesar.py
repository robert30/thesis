from pybdm import BDM
import numpy as np
import random

def viginere(s, key):
    key = [ord(i)-ord('a') for i in key]

    res = ''
    for i in range(len(s)):
        res += chr(ord('a') + ((key[i%len(key)]+ord(s[i])-ord('a'))%26))

    return res

bdm = BDM(ndim=1, nsymbols=2)

def C(s):
    a = []
    
    for c in s:
        for i in bin(ord(c)-ord('a'))[2:]:
            a.append(ord(i)-ord('0'))
    a = np.array(a)
    return bdm.bdm(np.array(a))

key = 'armine'
s1 = 'abcdefghij'
s2 = [chr(random.randint(0, 25)+ord('a')) for i in range(10)]
print(C(s1), C(s2))
print(C(viginere(s1, key)), C(viginere(s2, key)))
print(viginere(s1, key), viginere(s2, key))
