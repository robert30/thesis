\documentclass{article}

\usepackage{amsmath}
\usepackage{hyperref}
\usepackage{amsfonts}
\usepackage{amsthm}

\newtheorem{definition}{Definition}[section]
\newtheorem{lemma}[definition]{Lemma}
\newtheorem{example}[definition]{Example}
\newtheorem{theorem}[definition]{Theorem}
\newtheorem{remark}[definition]{Remark}
\newtheorem{corollary}[definition]{Corollary}


\title{Notes}
\author{Robert Koprinkov}

\begin{document}
\maketitle
\section{Introduction}
These are notes taken from the fourth edition of Li's and Vitanyi's An Introduction to Kolmogorov Complexity and its applications. These notes are taken for future reference, to be used during my studies in the field of Kolmogorov complexity.
\section{Binary Strings}
We are interested in strings over a basis alphabet $\mathcal{B}$. Unless specified otherwise, $\mathcal{B} = \{0, 1\}$. The set of all finite strings over $\mathcal{B}$ is denoted by $\mathcal{B}^*$ and is given by
\[\mathcal{B}^* = \{\epsilon, 0, 1, 00, 01, 10, 11, 000, \dots\}\]
Trying to create a one-to-one mapping from the natural numbers to the set of binary strings, we find that the standard binary representation has the disadvantage that multiply binary strings can represent the same natural number: in such a representation padding zeroes are neglected. We can create a one-on-one mapping from the binary strings $\mathcal{B}^*$ to the natural numbers $\mathcal{N}$ by lexicographically ordering all strings and associating each binary string with its index. This pairing looks as follows:
\[(\epsilon, 0), (0, 1), (1, 2), (00, 3), (01, 4), (10, 5), (11, 6), (000, 7)\dots\]
We will denote the length of a binary string $x$ by $l(x)$. So, $l(9) = l(010) = 3$. Clearly, $l(xy) = l(x) + l(y)$. Similarly $d$ denotes the cardinality of a finite set. Therefore, $d(\{l(u) = n|u\in\mathcal{B}^*\}) = 2^n$ and $d(\{l(u) \leq n|u\in\mathcal{B}^*\}) = 2^{n+1} - 1$. \\

Let $D:\{0, 1\}^* \to \mathcal{N}$ be any function. Considering the domain of $D$ as the set of code words, and the range as the set of source words, $D(x) = y$ is interpreted as '$x$ is an encoding of $y$, and $D$ is the \textit{decoding function}'. The set of all code words for a given source word $x$ is given by $D^{-1}(x) = \{y | D(y) = x\}$. Hence, $E=D^{-1}$ can be defined as the \textit{encoding substituion} ($E$ is not necessarily a function, as the decoding may not be injective). A set $A$ is called \textit{prefix-free} if no element is the prefix of another, and so there are no $x, y\in A$ such that $x = yz$ for some $z\in\mathcal{B}^*$. A function $D: \{0, 1\}^*\to\mathcal{N}$ defines a prefix code if its domain is prefix-free. A simple prefix-code we use throughout is obtained by reserving a stop symbol, say 0, and encoding $x\in\mathcal{N}$ as $1^x0$. We can also prefix an object with its length and iterate this for ever shorter codes:
\[E_i(x) = \begin{cases}1^x0 \text{ if } i=0 \\ E_{i-1}(l(x))x \text{ if } i>0\end{cases}\]
So, $E_1(x) = 1^{l(x)}0x$ and has length $2l(x)+1$. This encoding is sufficiently important to have a simpler notation:
\begin{align*}
	\bar{x} = 1^{l(x)}0x \\
	l(\bar{x}) = 2l(x)+1
\end{align*}
We call $\bar{x}$ the \textit{self-delimiting} version of the binary string $x$. Due to the prefix-free property of $\bar{x}$, we can effectively recover both $x$ and $y$ from $\bar{x}y$ or $\bar{x}\bar{y}$. This has a special notation:
\[\langle x, y\rangle = \overline{x}y\]
\section{Computability theory}
% computable, enumerable, ...
Each Turing machine defines a partial function from the $n-$tuples onto the integers, $n\geq 1$. We call such a function \textit{partial computable}. If the Turing machine halts for all inputs, then the function computed is defined for all arguments and so it is \textit{total} and \textit{computable}, or simply \textit{computable}. 

A set $A$ is called \textit{computably enumerable} if it is empty or the range of some total computable function $f$. We say that $f$ \textit{enumerates} $A$. An equivalent definition is that $A$ is computably enumerable if it is accepted by some Turing machine. \\

A set $A$ is called \textit{co-computably enumerable} if its complement is computably enumerable.\\

A set $A$ is called \textit{computable} if it possesses a computable characteristic function. That is, $A$ is computable iff there exists a computable function $f$ such that for all $x$, if $x\in A f(x) = 1$ and if $x\in\bar{A} f(x) = 0$. An equivalent definition would be that $A$ is computable iff it is accepted by a Turing machine that always halts. Obviously, all computable sets are computably enumerable and co-computably enumerable.

A total function $f:\mathcal{Q}\to\mathcal{R}$ is called upper semicomputable if it is defined by a rational-valued partial computable function $\phi(x, k)$, with $x$ a rational number and $k$ a nonnegative integer such that $\phi(x, k+1) \leq \phi(x, k)$ for all $k$ and $\lim_{k\to\infty}\phi(x, k) = f(x)$. This means that $f$ can be computationally approximated from above. A function is called \textit{lower semicomputable} if $-f$ is upper semicomputable, and a function is called \textit{semicomputable} if it is upper or lower semicomputable or both. If a function is both upper and lower semicomputable, it is called \textit{computable}. \\

Thus, a total function $f:\mathcal{Q}\to\mathcal{R}$ is called computable iff there is a total computable function $g(x, k)$ such that $|f(x)-g(x, k)| < \frac{1}{k}$. \\

In this way, we extend the notion of integer computable functions to real-valued computable functions with rational arguments, and to real-valued semicomputable functions with rational arguments. The idea is that a semicomputable function can be approximated from one side, but we may never know how close we are to the real value. A computable function can be approximated to any degree of precision.
\section{Kolmogorov complexity}
Let $S$ be a set of objects and $n: S\to\mathcal{N}$ be an arbitrary enumaration. We consider $f: \mathcal{N} \to\mathcal{N}$ to be a method for specifying an object $x\in S$. A specifiation of $x$ is, for a given $f$, then some $p\in\mathcal{B}$ (for program) such that
\[f(p) = n(x)\]
We then denote the \textit{complexity} of $x$ with respect to the specifying method $f$ as
\[C_f(x) = \min \{l(p)| p\in\mathcal{B}, f(p) = n(x)\}\]
And $\infty$ if such a $p$ does not exist. \\
We say that that $f$ \textit{minorizes} $g$ is there exists a constant $c$ such that for all $x\in S$
\[C_f(x) \leq C_g(x)+c\]
And two specifying methods are deemed \textit{equivalent} if each minorizes the other. If $\mathcal{C}$ is a subset of the partial functions and there is a function $f$ that minorizes all other functions in $\mathcal{C}$, $f$ is called \textit{additively optimal}. For all additively optimal methods $f$ and $g$ and for all $x$ it holds that there is a constant $c_{f, g}$ such that
\[|C_f(x)-C_g(x)| \leq c_{f, g}\]
So, when we restrict ourselves to optimal methods of specification the complexity $C$ of an object does not depend on the specification method within the margin of a constant. It is not possible to specify additively optimal function for all partial functions\footnote{example 2.0.1}. However, the class of computable partial functions does have an additively optimal function.
\subsection{Invariance theorem}
We start by proving the existance of the unconditional additively optimal universal partial computable function  $C(x)$.
\begin{lemma}
	The class of partial computable functions has an additively optimal function
\end{lemma}
\begin{proof}
	Fix an enumeration of computable functions $\varphi_1, \varphi_2, \dots$ and corresponding Turing machines $T_1, T_2, \dots$. Let $\varphi_0$ be the partial function computed by a universal Turing machine $U$. $U$ expects input of the format $\langle n, p \rangle$ and simulates $T_n$ with input $p$. If $T_n$ computes the partial computable function $\varphi_n$, we have 
	\[C_{\varphi_0}(\langle n, p\rangle) \leq C_{\varphi_n}(p) + 2l(n)+1 = C_{\varphi_n}(p) + c_{\varphi_n}\]
\end{proof}
The \textit{conditional complexity} of $x$ given $y$ is defined as
\[C_f(x|y) = \min \{l(p) | f(\langle p, y\rangle) = n(x)\}\]
And $C_f(x|y) = \infty$ if there is no such $p$. It can be interpreted as the shortest program that generates $x$, given $y$ as input. We will now see that there is also an additively optimal universal partial computable function for the unconditional complexity.
\begin{lemma}
	There is an additively optimal partial computable universal function $\varphi_0$ for the class of partial computable functions to compute $x$ given $y$. Therefore $C_{\varphi_0}(x|y) \leq C_{\varphi_i}(x|y) + c_{\varphi_i}$ for all $x, y \in S$ and all such partial computable functions $\varphi_{i}$.
\end{lemma}
\begin{proof}
	Again we define $\varphi_0$ as the function that expects input $\langle n, \langle y, p \rangle\rangle$ and simulates the Turing machine $T_n$ with program $p$ and $y$ as input. We have for all $n, y, p \in \mathcal{N}$ that
	\[\varphi_0(\langle n, \langle y, p\rangle\rangle) = \varphi_n(\langle p, y\rangle)\]
	Therefore
	\[C_{\varphi_0}(x|y) \leq C_{\varphi_n}(x|y) + 2l(n) + 1 = C_{\varphi_n}(x|y) + c_{\varphi_n}\]
\end{proof}
Then, for every pair $\varphi, \varphi'$ of additively optimal functions and all $x, y\in S$ we have
\[|\varphi(x|y) - \varphi'(x|y)| \leq c_{\varphi, \varphi'}\]
So, the complexity according to $\varphi$ and $\varphi'$ are equal up to a fixed constant. 
\begin{definition}[Kolmogorov complexity]
	Fix an additively optimal universal $\varphi_0$. The unconditional Kolmogorov complexity is defined as 
	\[C(x) = C_{\varphi_0}(x)\]
	And similarly the conditional Kolmogorov complexity as
	\[C((x|y) = C_{\varphi_0}(x|y)\]
	$\varphi_0$ is called the \textit{reference function} and its corresponding Turing machine $U$ the \textit{reference machine}.
\end{definition}

\subsection{}
\end{document}
