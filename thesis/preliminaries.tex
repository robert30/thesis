\section{Preliminaries}
\subsection{Towards Kolmogorov complexity}
\subsubsection{Computability theory}
We begin by introducting Turing machines. Turing machines, named after the British mathematician and codebreaker Alan Turing,  are a mathematical representation of a computer program. They are widely used in computability theory, the study of whether certain computer programs can be computed by a computer, and in computer science in general. They are also necessary to define for our definition of Kolmogorov complexity. \\
A Turing machine is defined by an automaton, and has access to a so-called tape for storage. The automaton can be thought of as a graph where the nodes represent states and the edges represent transitions between the states, while the tape can be thought of as an infinite sequence of cells, each containing a single character or being empty. The Turing machine can access once cell of the tape at a time, the cell pointed at by the read-write head. The read-write head is a pointer to a location in the cell, and at every step whatever symbol is located on the cell pointed at by the read-write head will be provided to the Turing machine, while the Turing machine moves the read-write head one cell left or one cell right after every iteration. \\
We will first define the Turing machine rigorously, and will then give an example.

\begin{definition}[Turing machine]
    A standard \textbf{Turing machine} $M$ is a five-tuple $(Q, \Sigma, \Gamma, \delta, q_0)$ where
    \begin{itemize}
    \item $Q$ is the set of all the states $\{q_0, q_1, \dots\}$
	\item $\Sigma$ is the input alphabet
	\item $\Gamma$ is the set of characters reserved for use by the Turing machine
	\item $\delta:Q\times\Gamma \to Q\times\Gamma\times\{L, R\}$ is our transition function. The transition function takes the current state and the symbol below the read-write head, and returns the next state, the symbol to write to the tape and the direction in which to move the read-write head.
	\item $q_0$, the starting state of the Turing machine
    \end{itemize}
\end{definition}
\begin{example}[Standard Turing machine]
    Suppose we want to define a Turing machine that calculates the inverse of a binary number. Given a binary number, we want it to flip every digit. Our input will consists of the digits $0$ and $1$, so $\Sigma=\{0, 1\}$, while our Turing machine will use the character $B$ to mark a blank cell, so $\Gamma = \{0, 1, B\}$. The initial tape with the integer $22$ looks like this:
    \begin{center}
	\begin{tabular}{|c|c|c|c|c|c|c|c|}
	    \hline
	    B & 1 & 0 & 1 & 1 & 0 & B & B\\
	    \hline
	    \multicolumn{1}{c}{$\uparrow$} \\
	\end{tabular}
    \end{center}
    Where the arrow points to the read-write head. Our Turing machine might look something like this:
    \begin{center}
     \begin{tikzpicture}[shorten >=1pt, node distance=2.5cm, on grid, auto]
	    \node[state,initial] (q_0) {$q_0$};
	    \node[state] (q_1) [right=of q_0] {$q_1$};
	    \node[state] (q_2) [right=of q_1] {$q_2$};
	    \node[state] (q_3) [right=of q_2] {$q_3$};
	    \node[state] (q_4) [right=of q_3] {$q_4$};
	     \path [->]
		    (q_0) edge node {B/BR} (q_1)
		    (q_1) edge node {B/BL} (q_2)
			      edge [loop above] node [align=center]{0/1R\\1/0R} ()
		    (q_2) edge node [align=center]{B/BR} (q_3)
			edge [loop above] node [align=center]{0/0L\\1/1L} ()
		    (q_3) edge node[align=center] {0/0L\\1/1L} (q_4);
     \end{tikzpicture}
     \end{center}
    This Turing machine consists of five states, and the edges out of each state indicate the operations the Turing machine is allowed to do at a given state. Note that every edge has a label of the form $\gamma_i/\gamma_2D$, where $\gamma_1, \gamma_2\in\Gamma$ and $D\in\{L, R\}$. This indicates that if the cell under the read-write head has the character $\gamma_1$, then the Turing machine takes this transition, replaces $\gamma_1$ with $\gamma_2$ and moves the read-write head in the direction given by $D$. So, this Turing machine moves from left to right, inverting every digit, and then moves the pointer back to the left. The reader is encouraged to verify that this Turing machine indeed inverts the input integer.
\end{example}

We note that the Turing machine is just one of many different mathemtical representations of computers. In this thesis, we will limit ourselves to using Turing machines as they are most widely used, but for the sake of completeness we mention the existence of other models of computation, such as the lambda-calculus, $\mu-$recursive functions and register machines. A landmark theorem states that all of these models of computation are equivalent, and so that limiting ourselves to Turing machines does not result in a loss of generality.
\begin{theorem}[Church-Turing thesis]
    The \textbf{Church-Turing thesis} proves that the following models of computation are equivalent:
    \begin{itemize}
	\item Turing machines
	\item Lambda calculus
	\item Register machines
	\item $\mu-$recursive functions
    \end{itemize}
    That is to say, if you can compute something with any of these models of computation, you can do so with all others.
\end{theorem}

We proceed to define the encoding of Turing machines, which we will use to define the universal Turing machine, which will be of great use later. We first give a definition of the set of binary strings, and then give the definition of the encoding of the Turing machine.

\begin{definition}[Set of binary strings]
    The set of binary strings $\mathcal{B}$ is defined as:
    \[\mathcal{B} = \{0, 1, 00, 01, 10, 11, 000, \dots\}\]
\end{definition}
Does a bijection exist between $\mathbb{N}$ and $\mathcal{B}$? Converting a natural number to its binary representation and back does not comprise a bijection, as we allow zeros as prefixes in the definition of $\mathcal{B}$. We therefore define another bijection.
\begin{example}[Bijection between $\mathbb{N}$ and $\mathcal{B}$]

\end{example}

\begin{definition}
    We can encode a Turing machine as a binary string $b\in\mathcal{B}$. One possible way of doing so is as follows.
\end{definition}

We now define the universal Turing machine, which like a computer can run computer programs.
\begin{definition}
    The \textbf{universal Turing machine} $T$ is a Turing machine that takes in an arbitrary encoded Turing machine along with an input string, runs the encoded Turing machine on the input and writes the output to the tape.
\end{definition}
The universal Turing machine has been proven to exist. % citation
We are now ready to define the Kolmogorov complexity and its related objects.

\subsubsection{Variants of Kolmogorov complexity}
We define the Kolmogorov complexity and denote some of its properties. We consider a decompressing program $D$. 

\begin{definition}[Plain Kolmogorov complexity]
    
\end{definition}


\subsubsection{Nearest Information Distance}

\subsubsection{Algorithmic Probability}

\subsection{Machine Learning}
\subsubsection{Neural Networks}

\subsubsection{Autoencoders}
