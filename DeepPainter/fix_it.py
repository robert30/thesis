import requests as requests
from tqdm import tqdm
import os

out_dir = 'paintings/'

to_expl = []

for author in os.listdir(out_dir):
    for filen in os.listdir(out_dir + author):
        if filen[-8:] != '.jpg.jpg':
            if(filen[-8:] == '.png.jpg.'):
                #rename png
                os.rename(out_dir + author + '/' + filen, out_dir + author + '/' + filen[:-4])
            elif(filen[0]!='#'):
                #explore this directory
                to_expl.append(author + '/' + filen[:-4])

bar = tqdm(to_expl)
for author in bar:
    author_dir = out_dir + author + '/'
    print(author_dir)
    if not os.path.exists(author_dir):
        os.makedirs(author_dir)
    
    author_url = 'https://www.ibiblio.org/wm/paint/auth/' + author
    text = requests.get(author_url).text

    while (text.find('<A HREF="') != -1):
        text = text[text.find('<A HREF="') + 9:]
        painting_name = text[:text.find('"')]
        
        if(painting_name[-1] == '/'):
            painting_name = painting_name[:-1]
        if (painting_name.find('/') == -1 and len(painting_name) >= 3 and (painting_name[-3:] == 'jpg' or painting_name[-3:] == '.png')):
            painting_url = author_url + '/' + str(painting_name)

            f = open(author_dir  + painting_name + '.jpg', 'wb')
            f.write(requests.get(painting_url).content)
        elif(painting_name.find('/') == -1 and painting_name.find('#')==-1 and painting_name != '..'):
            # subdir with more paintings by author
            to_expl.append(author_dir + painting_name)
            bar.total = bar.total+1
