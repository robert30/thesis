from PIL import Image
import os


for file in os.listdir('Images'):
    im = Image.open('Images/' + file)
    
    thumbnail_size = (256,1000)
    
    if im.size[0] > im.size[1]:
        thumbnail_size = (1000, 256)

    im.thumbnail(thumbnail_size)
    im.crop((0, 0, 256, 256)).save('ImagesNorm/' + file)
