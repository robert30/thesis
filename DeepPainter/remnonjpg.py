import os
import shutil

n_painter=0

def rem(dirn):
    global n_painter
    for filen in os.listdir(dirn):
        if os.path.isfile(dirn + filen) and (filen[-8:] != '.jpg.jpg' and filen[-8:] != '.png.jpg'):
            os.remove(dirn+filen)
        elif os.path.isdir(dirn + filen):
            rem(dirn+filen + '/')
        if (os.path.isfile(dirn+filen) and filen[-8:] == '.jpg.jpg'):
            os.rename(dirn+filen, dirn + filen[:-4])
        if (os.path.isfile(dirn+filen) and filen[-8:] == '.png.jpg'):
            os.rename(dirn+filen, dirn + filen[:-4])
    if(len(os.listdir(dirn)) == 0):
        os.rmdir(dirn)


for artist in os.listdir('paintings'):
    rem('paintings/' + artist + '/')

print(n_painter)
