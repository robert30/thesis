import os
import shutil

dest_dir = './Images/'
src_dir = './paintings/'

def copypaintings(paint_path):
    if os.path.isfile(src_dir + paint_path):
        shutil.copyfile(src_dir+paint_path, dest_dir + paint_path.replace('/', '_'))
    else:
        for filen in os.listdir(src_dir+paint_path):
            copypaintings(paint_path + '/' + filen)

copypaintings('')
