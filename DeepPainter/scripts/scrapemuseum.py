import requests as requests
from tqdm import tqdm
import os

out_dir = 'paintings2/'

authors = []

auth_text = requests.get('https://www.ibiblio.org/wm/paint/auth/').text
while (auth_text.find('/auth') != -1):
    auth_text = auth_text[auth_text.find('auth/') + 5:]
    authors.append(str(auth_text[:auth_text.find('/')]))

bar = tqdm(authors)
i=0
for author in bar:
    i=i+1
    author_dir = out_dir + author + '/'
    if not os.path.exists(author_dir):
        os.makedirs(author_dir)
    
    author_url = 'https://www.ibiblio.org/wm/paint/auth/' + author
    text = requests.get(author_url).text

    while (text.find('<A HREF="') != -1):
        text = text[text.find('<A HREF="') + 9:]
        painting_name = text[:text.find('"')]
        
        if(painting_name[-1] == '/'):
            painting_name = painting_name[:-1]
        
        if (painting_name.find('/') == -1 and painting_name.find('#') and len(painting_name) >= 3 and (painting_name[-3:] == 'jpg' or painting_name[-3:] == '.png')):
            painting_url = author_url + '/' + str(painting_name)

            f = open(author_dir  + painting_name, 'wb')
            f.write(requests.get(painting_url).content)
        elif(painting_name.find('/') == -1 and painting_name != '..'):
            # subdir with more paintings by author
            bar.total=bar.total+1
            authors.insert(i, author + "/" + str(painting_name))

