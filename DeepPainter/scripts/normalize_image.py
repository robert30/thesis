from PIL import Image
import os
from tqdm import tqdm

src_dir = './Images/'
out_dir = './ImagesNorm/'

for filename in tqdm(os.listdir(src_dir)):
    im = Image.open(src_dir + str(filename))
    
    thumbnail_size = (256,100000)
    
    if im.size[0] > im.size[1]:
        thumbnail_size = (100000, 256)
    im.thumbnail(thumbnail_size)
    im.crop((0, 0, 256, 256)).save(out_dir + filename)
