/* Generated by GOB (v2.0.14)   (do not edit directly) */

/* End world hunger, donate to the World Food Programme, http://www.wfp.org */

#define GOB_VERSION_MAJOR 2
#define GOB_VERSION_MINOR 0
#define GOB_VERSION_PATCHLEVEL 14

#define selfp (self->_priv)

#include <string.h> /* memset() */

#include "qsearch-maketree.h"

#ifdef G_LIKELY
#define ___GOB_LIKELY(expr) G_LIKELY(expr)
#define ___GOB_UNLIKELY(expr) G_UNLIKELY(expr)
#else /* ! G_LIKELY */
#define ___GOB_LIKELY(expr) (expr)
#define ___GOB_UNLIKELY(expr) (expr)
#endif /* G_LIKELY */

#line 21 "qsearch-maketree.gob"

/*
Copyright (c) 2003-2008 Rudi Cilibrasi, Rulers of the RHouse
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the University nor the names of its contributors
   may be used to endorse or promote products derived from this software
   without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE RULERS AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED.  IN NO EVENT SHALL THE RULERS OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.
*/
#include <stdio.h>
#define _( O ) gettext( O )
#include "qsearch.h"
#include "qsearch-maketree-private.h"

static QSearchMakeTree *qsmaketree;

gboolean is_asymmetric(gsl_matrix *dm)
{
  int i, j;
  if (dm->size1 != dm->size2)
    return TRUE;
  for (i = 0; i < dm->size1; i += 1)
    for (j = i+1; j < dm->size2; j += 1)
      if (gsl_matrix_get(dm,i,j) != gsl_matrix_get(dm,j,i))
        return TRUE;
  return FALSE;
}

void tree_search_started(gsl_matrix *dm, void *user_data)
{
}

void tried_to_improve(gsl_matrix *dm, QSearchTree *old,
                      QSearchTree *improved, void *user_data)
{
  MakeTreeResult *mtr = (MakeTreeResult *) user_data;
  if (improved) {
    printf("%f   (lmsd=%e)\n", qsearch_tree_score_tree(improved, dm), qsearch_treemaster_get_lmsd(mtr->tm));
    mtr->tree = qsearch_tree_add_labels(improved, mtr->mat);
    qsearch_maketree_write_tree_file(mtr);
    qsearch_labeledtree_free(mtr->tree);
  }
}

  void tree_search_done(gsl_matrix *dm, QSearchTree *final,
                           void *user_data)
{
  MakeTreeResult *mtr = (MakeTreeResult *) user_data;
  printf("%f\n", qsearch_tree_score_tree(final, dm));
  mtr->tree = qsearch_tree_add_labels(final, mtr->mat);
  qsearch_maketree_write_tree_file(mtr);
  qsearch_labeledtree_free(mtr->tree);
}


#line 100 "qsearch-maketree.c"
/* self casting macros */
#define SELF(x) QSEARCH_MAKETREE(x)
#define SELF_CONST(x) QSEARCH_MAKETREE_CONST(x)
#define IS_SELF(x) QSEARCH_IS_MAKETREE(x)
#define TYPE_SELF QSEARCH_TYPE_MAKETREE
#define SELF_CLASS(x) QSEARCH_MAKETREE_CLASS(x)

#define SELF_GET_CLASS(x) QSEARCH_MAKETREE_GET_CLASS(x)

/* self typedefs */
typedef QSearchMakeTree Self;
typedef QSearchMakeTreeClass SelfClass;

/* here are local prototypes */
static void ___object_set_property (GObject *object, guint property_id, const GValue *value, GParamSpec *pspec);
static void ___object_get_property (GObject *object, guint property_id, GValue *value, GParamSpec *pspec);
static void qsearch_maketree_init (QSearchMakeTree * o) G_GNUC_UNUSED;
static void qsearch_maketree_class_init (QSearchMakeTreeClass * c) G_GNUC_UNUSED;

enum {
	PROP_0,
	PROP_OUTPUT_NEXUS,
	PROP_DOT_SHOW_RING,
	PROP_DOT_SHOW_DETAILS,
	PROP_FILESTEM,
	PROP_DOT_TITLE
};

/* pointer to the class of our parent */
static GObjectClass *parent_class = NULL;

/* Short form macros */
#define self_get_output_nexus qsearch_maketree_get_output_nexus
#define self_set_output_nexus qsearch_maketree_set_output_nexus
#define self_get_dot_show_ring qsearch_maketree_get_dot_show_ring
#define self_set_dot_show_ring qsearch_maketree_set_dot_show_ring
#define self_get_dot_show_details qsearch_maketree_get_dot_show_details
#define self_set_dot_show_details qsearch_maketree_set_dot_show_details
#define self_get_filestem qsearch_maketree_get_filestem
#define self_set_filestem qsearch_maketree_set_filestem
#define self_get_dot_title qsearch_maketree_get_dot_title
#define self_set_dot_title qsearch_maketree_set_dot_title
#define self_new qsearch_maketree_new
#define self_top qsearch_maketree_top
#define self_process_options qsearch_maketree_process_options
#define self_write_tree_file qsearch_maketree_write_tree_file
#define self_print_help_and_exit qsearch_maketree_print_help_and_exit
GType
qsearch_maketree_get_type (void)
{
	static GType type = 0;

	if ___GOB_UNLIKELY(type == 0) {
		static const GTypeInfo info = {
			sizeof (QSearchMakeTreeClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) qsearch_maketree_class_init,
			(GClassFinalizeFunc) NULL,
			NULL /* class_data */,
			sizeof (QSearchMakeTree),
			0 /* n_preallocs */,
			(GInstanceInitFunc) qsearch_maketree_init,
			NULL
		};

		type = g_type_register_static (G_TYPE_OBJECT, "QSearchMakeTree", &info, (GTypeFlags)0);
	}

	return type;
}

/* a macro for creating a new object of our type */
#define GET_NEW ((QSearchMakeTree *)g_object_new(qsearch_maketree_get_type(), NULL))

/* a function for creating a new object of our type */
#include <stdarg.h>
static QSearchMakeTree * GET_NEW_VARG (const char *first, ...) G_GNUC_UNUSED;
static QSearchMakeTree *
GET_NEW_VARG (const char *first, ...)
{
	QSearchMakeTree *ret;
	va_list ap;
	va_start (ap, first);
	ret = (QSearchMakeTree *)g_object_new_valist (qsearch_maketree_get_type (), first, ap);
	va_end (ap);
	return ret;
}


static void
___finalize(GObject *obj_self)
{
#define __GOB_FUNCTION__ "QSearch:MakeTree::finalize"
	QSearchMakeTree *self G_GNUC_UNUSED = QSEARCH_MAKETREE (obj_self);
	gpointer priv G_GNUC_UNUSED = self->_priv;
	if(G_OBJECT_CLASS(parent_class)->finalize) \
		(* G_OBJECT_CLASS(parent_class)->finalize)(obj_self);
}
#undef __GOB_FUNCTION__

static void 
qsearch_maketree_init (QSearchMakeTree * o G_GNUC_UNUSED)
{
#define __GOB_FUNCTION__ "QSearch:MakeTree::init"
	o->_priv = G_TYPE_INSTANCE_GET_PRIVATE(o,QSEARCH_TYPE_MAKETREE,QSearchMakeTreePrivate);
}
#undef __GOB_FUNCTION__
static void 
qsearch_maketree_class_init (QSearchMakeTreeClass * c G_GNUC_UNUSED)
{
#define __GOB_FUNCTION__ "QSearch:MakeTree::class_init"
	GObjectClass *g_object_class G_GNUC_UNUSED = (GObjectClass*) c;

	g_type_class_add_private(c,sizeof(QSearchMakeTreePrivate));

	parent_class = g_type_class_ref (G_TYPE_OBJECT);

	g_object_class->finalize = ___finalize;
	g_object_class->get_property = ___object_get_property;
	g_object_class->set_property = ___object_set_property;
    {
	GParamSpec   *param_spec;

	param_spec = g_param_spec_boolean
		("output_nexus" /* name */,
		 _("output-nexus") /* nick */,
		 _("flag indicating whether to use Nexus output format") /* blurb */,
		 FALSE /* default_value */,
		 (GParamFlags)(G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT));
	g_object_class_install_property (g_object_class,
		PROP_OUTPUT_NEXUS,
		param_spec);
	param_spec = g_param_spec_boolean
		("dot_show_ring" /* name */,
		 _("dot-show-ring") /* nick */,
		 _("show dotted-line perimeter ring for dot tree") /* blurb */,
		 TRUE /* default_value */,
		 (GParamFlags)(G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT));
	g_object_class_install_property (g_object_class,
		PROP_DOT_SHOW_RING,
		param_spec);
	param_spec = g_param_spec_boolean
		("dot_show_details" /* name */,
		 _("dot-show-details") /* nick */,
		 _("show various extra data in dot output") /* blurb */,
		 TRUE /* default_value */,
		 (GParamFlags)(G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT));
	g_object_class_install_property (g_object_class,
		PROP_DOT_SHOW_DETAILS,
		param_spec);
	param_spec = g_param_spec_string
		("filestem" /* name */,
		 _("filestem") /* nick */,
		 _("initial part of output filename without extension") /* blurb */,
		 _("treefile") /* default_value */,
		 (GParamFlags)(G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT));
	g_object_class_install_property (g_object_class,
		PROP_FILESTEM,
		param_spec);
	param_spec = g_param_spec_string
		("dot_title" /* name */,
		 _("dot-title") /* nick */,
		 _("title for the output .dot tree file") /* blurb */,
		 _("tree") /* default_value */,
		 (GParamFlags)(G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT));
	g_object_class_install_property (g_object_class,
		PROP_DOT_TITLE,
		param_spec);
    }
}
#undef __GOB_FUNCTION__

static void
___object_set_property (GObject *object,
	guint property_id,
	const GValue *VAL G_GNUC_UNUSED,
	GParamSpec *pspec G_GNUC_UNUSED)
#define __GOB_FUNCTION__ "QSearch:MakeTree::set_property"
{
	QSearchMakeTree *self G_GNUC_UNUSED;

	self = QSEARCH_MAKETREE (object);

	switch (property_id) {
	case PROP_OUTPUT_NEXUS:
		{
#line 100 "qsearch-maketree.gob"
self->_priv->output_nexus = g_value_get_boolean (VAL);
#line 290 "qsearch-maketree.c"
		}
		break;
	case PROP_DOT_SHOW_RING:
		{
#line 108 "qsearch-maketree.gob"
self->_priv->dot_show_ring = g_value_get_boolean (VAL);
#line 297 "qsearch-maketree.c"
		}
		break;
	case PROP_DOT_SHOW_DETAILS:
		{
#line 116 "qsearch-maketree.gob"
self->_priv->dot_show_details = g_value_get_boolean (VAL);
#line 304 "qsearch-maketree.c"
		}
		break;
	case PROP_FILESTEM:
		{
#line 124 "qsearch-maketree.gob"
{ char *old = self->_priv->filestem; self->_priv->filestem = g_value_dup_string (VAL); g_free (old); }
#line 311 "qsearch-maketree.c"
		}
		break;
	case PROP_DOT_TITLE:
		{
#line 132 "qsearch-maketree.gob"
{ char *old = self->_priv->dot_title; self->_priv->dot_title = g_value_dup_string (VAL); g_free (old); }
#line 318 "qsearch-maketree.c"
		}
		break;
	default:
/* Apparently in g++ this is needed, glib is b0rk */
#ifndef __PRETTY_FUNCTION__
#  undef G_STRLOC
#  define G_STRLOC	__FILE__ ":" G_STRINGIFY (__LINE__)
#endif
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}
#undef __GOB_FUNCTION__

static void
___object_get_property (GObject *object,
	guint property_id,
	GValue *VAL G_GNUC_UNUSED,
	GParamSpec *pspec G_GNUC_UNUSED)
#define __GOB_FUNCTION__ "QSearch:MakeTree::get_property"
{
	QSearchMakeTree *self G_GNUC_UNUSED;

	self = QSEARCH_MAKETREE (object);

	switch (property_id) {
	case PROP_OUTPUT_NEXUS:
		{
#line 100 "qsearch-maketree.gob"
g_value_set_boolean (VAL, self->_priv->output_nexus);
#line 349 "qsearch-maketree.c"
		}
		break;
	case PROP_DOT_SHOW_RING:
		{
#line 108 "qsearch-maketree.gob"
g_value_set_boolean (VAL, self->_priv->dot_show_ring);
#line 356 "qsearch-maketree.c"
		}
		break;
	case PROP_DOT_SHOW_DETAILS:
		{
#line 116 "qsearch-maketree.gob"
g_value_set_boolean (VAL, self->_priv->dot_show_details);
#line 363 "qsearch-maketree.c"
		}
		break;
	case PROP_FILESTEM:
		{
#line 124 "qsearch-maketree.gob"
g_value_set_string (VAL, self->_priv->filestem);
#line 370 "qsearch-maketree.c"
		}
		break;
	case PROP_DOT_TITLE:
		{
#line 132 "qsearch-maketree.gob"
g_value_set_string (VAL, self->_priv->dot_title);
#line 377 "qsearch-maketree.c"
		}
		break;
	default:
/* Apparently in g++ this is needed, glib is b0rk */
#ifndef __PRETTY_FUNCTION__
#  undef G_STRLOC
#  define G_STRLOC	__FILE__ ":" G_STRINGIFY (__LINE__)
#endif
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}
#undef __GOB_FUNCTION__



#line 100 "qsearch-maketree.gob"
gboolean 
qsearch_maketree_get_output_nexus (QSearchMakeTree * self)
#line 397 "qsearch-maketree.c"
{
#define __GOB_FUNCTION__ "QSearch:MakeTree::get_output_nexus"
{
#line 100 "qsearch-maketree.gob"
		gboolean val; g_object_get (G_OBJECT (self), "output_nexus", &val, NULL); return val;
}}
#line 404 "qsearch-maketree.c"
#undef __GOB_FUNCTION__

#line 100 "qsearch-maketree.gob"
void 
qsearch_maketree_set_output_nexus (QSearchMakeTree * self, gboolean val)
#line 410 "qsearch-maketree.c"
{
#define __GOB_FUNCTION__ "QSearch:MakeTree::set_output_nexus"
{
#line 100 "qsearch-maketree.gob"
		g_object_set (G_OBJECT (self), "output_nexus", val, NULL);
}}
#line 417 "qsearch-maketree.c"
#undef __GOB_FUNCTION__

#line 108 "qsearch-maketree.gob"
gboolean 
qsearch_maketree_get_dot_show_ring (QSearchMakeTree * self)
#line 423 "qsearch-maketree.c"
{
#define __GOB_FUNCTION__ "QSearch:MakeTree::get_dot_show_ring"
{
#line 108 "qsearch-maketree.gob"
		gboolean val; g_object_get (G_OBJECT (self), "dot_show_ring", &val, NULL); return val;
}}
#line 430 "qsearch-maketree.c"
#undef __GOB_FUNCTION__

#line 108 "qsearch-maketree.gob"
void 
qsearch_maketree_set_dot_show_ring (QSearchMakeTree * self, gboolean val)
#line 436 "qsearch-maketree.c"
{
#define __GOB_FUNCTION__ "QSearch:MakeTree::set_dot_show_ring"
{
#line 108 "qsearch-maketree.gob"
		g_object_set (G_OBJECT (self), "dot_show_ring", val, NULL);
}}
#line 443 "qsearch-maketree.c"
#undef __GOB_FUNCTION__

#line 116 "qsearch-maketree.gob"
gboolean 
qsearch_maketree_get_dot_show_details (QSearchMakeTree * self)
#line 449 "qsearch-maketree.c"
{
#define __GOB_FUNCTION__ "QSearch:MakeTree::get_dot_show_details"
{
#line 116 "qsearch-maketree.gob"
		gboolean val; g_object_get (G_OBJECT (self), "dot_show_details", &val, NULL); return val;
}}
#line 456 "qsearch-maketree.c"
#undef __GOB_FUNCTION__

#line 116 "qsearch-maketree.gob"
void 
qsearch_maketree_set_dot_show_details (QSearchMakeTree * self, gboolean val)
#line 462 "qsearch-maketree.c"
{
#define __GOB_FUNCTION__ "QSearch:MakeTree::set_dot_show_details"
{
#line 116 "qsearch-maketree.gob"
		g_object_set (G_OBJECT (self), "dot_show_details", val, NULL);
}}
#line 469 "qsearch-maketree.c"
#undef __GOB_FUNCTION__

#line 124 "qsearch-maketree.gob"
gchar * 
qsearch_maketree_get_filestem (QSearchMakeTree * self)
#line 475 "qsearch-maketree.c"
{
#define __GOB_FUNCTION__ "QSearch:MakeTree::get_filestem"
{
#line 124 "qsearch-maketree.gob"
		gchar* val; g_object_get (G_OBJECT (self), "filestem", &val, NULL); return val;
}}
#line 482 "qsearch-maketree.c"
#undef __GOB_FUNCTION__

#line 124 "qsearch-maketree.gob"
void 
qsearch_maketree_set_filestem (QSearchMakeTree * self, gchar * val)
#line 488 "qsearch-maketree.c"
{
#define __GOB_FUNCTION__ "QSearch:MakeTree::set_filestem"
{
#line 124 "qsearch-maketree.gob"
		g_object_set (G_OBJECT (self), "filestem", val, NULL);
}}
#line 495 "qsearch-maketree.c"
#undef __GOB_FUNCTION__

#line 132 "qsearch-maketree.gob"
gchar * 
qsearch_maketree_get_dot_title (QSearchMakeTree * self)
#line 501 "qsearch-maketree.c"
{
#define __GOB_FUNCTION__ "QSearch:MakeTree::get_dot_title"
{
#line 132 "qsearch-maketree.gob"
		gchar* val; g_object_get (G_OBJECT (self), "dot_title", &val, NULL); return val;
}}
#line 508 "qsearch-maketree.c"
#undef __GOB_FUNCTION__

#line 132 "qsearch-maketree.gob"
void 
qsearch_maketree_set_dot_title (QSearchMakeTree * self, gchar * val)
#line 514 "qsearch-maketree.c"
{
#define __GOB_FUNCTION__ "QSearch:MakeTree::set_dot_title"
{
#line 132 "qsearch-maketree.gob"
		g_object_set (G_OBJECT (self), "dot_title", val, NULL);
}}
#line 521 "qsearch-maketree.c"
#undef __GOB_FUNCTION__

#line 139 "qsearch-maketree.gob"
GObject * 
qsearch_maketree_new (void)
#line 527 "qsearch-maketree.c"
{
#define __GOB_FUNCTION__ "QSearch:MakeTree::new"
{
#line 140 "qsearch-maketree.gob"
	
    GObject *ret = (GObject *) GET_NEW;
    qsmaketree = QSEARCH_MAKETREE(ret);
    qsmaketree->_priv->matrix_filename = NULL;
    return G_OBJECT (ret);
  }}
#line 538 "qsearch-maketree.c"
#undef __GOB_FUNCTION__

#line 146 "qsearch-maketree.gob"
QSearchMakeTree * 
qsearch_maketree_top (void)
#line 544 "qsearch-maketree.c"
{
#define __GOB_FUNCTION__ "QSearch:MakeTree::top"
{
#line 147 "qsearch-maketree.gob"
	
  if (qsmaketree == NULL)
    qsearch_maketree_new();
  g_assert(qsmaketree != 0);
  return qsmaketree;
  }}
#line 555 "qsearch-maketree.c"
#undef __GOB_FUNCTION__

#line 153 "qsearch-maketree.gob"
MakeTreeResult * 
qsearch_maketree_process_options (char ** argv)
#line 561 "qsearch-maketree.c"
{
#define __GOB_FUNCTION__ "QSearch:MakeTree::process_options"
{
#line 154 "qsearch-maketree.gob"
	
    gsl_matrix *dm;
    LabeledMatrix *lm;
    GString *matstr = NULL;
    char **cur;
    QSearchMakeTree *qsmt = qsearch_maketree_top();
    for (cur = argv+1; *cur; cur += 1) {
      if (strcmp(*cur, "-o") == 0) {
        if (cur[1] == NULL)
          g_error("-o requires an argument");
        qsearch_maketree_set_filestem(qsearch_maketree_top(), cur[1]);
        cur += 1;
        continue;
      }
      if (strcmp(*cur, "-v") == 0 || strcmp(*cur, "--version") == 0) {
        printf("%s\n", qsearch_package_version);
        exit(0);
      }
      if (strcmp(*cur, "-n") == 0) {
        qsearch_maketree_set_output_nexus(qsearch_maketree_top(), TRUE);
        continue;
      }
      if (qsmt->_priv->matrix_filename == NULL) {
        qsmt->_priv->matrix_filename = *cur;
        continue;
    }
    g_error("Unrecognized argument: %s", *cur);
  }
  if (qsmt->_priv->matrix_filename == NULL)
    qsearch_maketree_print_help_and_exit();
  matstr = complearn_read_whole_file(qsmt->_priv->matrix_filename);
  lm = complearn_load_any_matrix(matstr);
  dm = lm->mat;
  if (dm->size1 != dm->size2)
    g_error(_("Must have square distance matrix as input."));
  if (is_asymmetric(dm)) {
    fprintf(stderr, _("Warning, averaging asymmetric matrix.\n"));
    lm->mat = complearn_average_matrix(dm);
    gsl_matrix_free(dm);
    dm = lm->mat;
  }
  printf(_("Starting search on matrix size %d.\n"), dm->size1);
  QSearchTreeMaster *cltm = qsearch_treemaster_new(dm);
  MakeTreeResult *mtr = calloc(sizeof(*mtr), 1);
  mtr->mat = lm;
  mtr->tm = cltm;
  qsearch_treemaster_add_observer(cltm, tree_search_started,
      tried_to_improve, tree_search_done, mtr);
  qsearch_treemaster_find_best_tree(cltm);
  return mtr;
  }}
#line 617 "qsearch-maketree.c"
#undef __GOB_FUNCTION__

#line 205 "qsearch-maketree.gob"
void 
qsearch_maketree_write_tree_file (const MakeTreeResult * mtr)
#line 623 "qsearch-maketree.c"
{
#define __GOB_FUNCTION__ "QSearch:MakeTree::write_tree_file"
{
#line 205 "qsearch-maketree.gob"
	
  gboolean do_nexus = qsearch_maketree_get_output_nexus(qsearch_maketree_top());
  if (do_nexus) {
    char *fname = g_strdup_printf("%s.nex", qsearch_maketree_get_filestem(qsearch_maketree_top()));
    char *nex_str = qsearch_tree_to_nexus_full(mtr->tree, mtr->mat);
    GString *gf = g_string_new(nex_str);
    complearn_write_file(fname, gf);
    g_string_free(gf, TRUE);
    g_free(nex_str);
  }
  else {
    char *fname = g_strdup_printf("%s.dot", qsearch_maketree_get_filestem(qsearch_maketree_top()));
    complearn_write_file(fname, qsearch_tree_to_dot(mtr->tree));
  }
  }}
#line 643 "qsearch-maketree.c"
#undef __GOB_FUNCTION__

#line 220 "qsearch-maketree.gob"
void 
qsearch_maketree_print_help_and_exit (void)
#line 649 "qsearch-maketree.c"
{
#define __GOB_FUNCTION__ "QSearch:MakeTree::print_help_and_exit"
{
#line 220 "qsearch-maketree.gob"
	
  fprintf(stderr, "Usage:\n\n");
  fprintf(stderr, "maketree [-v] [-n] <distmatrix>\n");
  fprintf(stderr, "          -v  print version\n");
  fprintf(stderr, "          -n  nexus instead of dot output format\n");
  exit(0);
}}
#line 661 "qsearch-maketree.c"
#undef __GOB_FUNCTION__
