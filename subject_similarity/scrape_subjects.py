import requests
from tqdm import tqdm
import os

def filter_to_content(txt):
    ret = []
    while txt.find('<body>') != -1:
        txt = txt[txt.find('<body>')+6:]
        ret.append(txt[:txt.find('</body>')])
        if ret[-1].find('<strong>Instructional Modes</strong>') != -1:
            ret[-1] = ret[-1][:ret[-1].find('<strong>Instructional Modes</strong>')]
        if ret[-1].find('<strong>Werkvormen</strong>') != -1:
            ret[-1] = ret[-1][:ret[-1].find('<strong>Werkvormen</strong>')]
        while ret[-1].find('<') != -1:
            nindx = ret[-1].find('<')

            ret[-1] = ret[-1][:nindx] + ret[-1][ret[-1].find('>', nindx)+1:]
        while ret[-1].find('&nbsp;') != -1:
            ret[-1] = ret[-1][:ret[-1].find('&nbsp;')] + ret[-1][ret[-1].find('&nbsp;')+6:]
    return ret

def scrape_subject(subj_code):
    res = requests.get('https://www.ru.nl/osiris-student/OnderwijsCatalogusSelect.do?cursus=%s&selectie=cursus&collegejaar=2020' % subj_code)
    return ' '.join(filter_to_content(res.text))

def get_subject_codes():
    res = requests.post('https://ru.osiris-student.nl/student/osiris/student/cursussen/zoeken', headers={'taal': 'NL'}, data='{"from":0,"size":700,"sort":[{"cursus_korte_naam.raw":{"order":"asc"}},{"cursus":{"order":"asc"}},{"collegejaar":{"order":"desc"}}],"aggs":{"agg_terms_inschrijfperiodes_cursus.datum_vanaf":{"filter":{"bool":{"must":[{},{"terms":{"collegejaar":[2020]}},{"terms":{"faculteit_naam":["Faculteit der Natuurwetenschappen, Wiskunde en Informatica"]}},{"range":{"inschrijfperiodes_cursus.datum_vanaf":{"lte":"now"}}},{"range":{"inschrijfperiodes_cursus.datum_tm":{"gte":"now"}}},{"nested":{"path":"blokken_nested","query":{"bool":{"must":[{}]}}}}]}},"aggs":{"agg_inschrijfperiodes_cursus.datum_vanaf_buckets":{"terms":{"field":"inschrijfperiodes_cursus.datum_vanaf","size":2500,"order":{"_term":"asc"}}}}},"agg_terms_inschrijfperiodes_toets.datum_vanaf":{"filter":{"bool":{"must":[{},{"terms":{"collegejaar":[2020]}},{"terms":{"faculteit_naam":["Faculteit der Natuurwetenschappen, Wiskunde en Informatica"]}},{"range":{"inschrijfperiodes_toets.datum_vanaf":{"lte":"now"}}},{"range":{"inschrijfperiodes_toets.datum_tm":{"gte":"now"}}},{"nested":{"path":"blokken_nested","query":{"bool":{"must":[{}]}}}}]}},"aggs":{"agg_inschrijfperiodes_toets.datum_vanaf_buckets":{"terms":{"field":"inschrijfperiodes_toets.datum_vanaf","size":2500,"order":{"_term":"asc"}}}}},"agg_terms_blokken_nested.beschikbaarheid":{"filter":{"bool":{"must":[{},{"terms":{"collegejaar":[2020]}},{"terms":{"faculteit_naam":["Faculteit der Natuurwetenschappen, Wiskunde en Informatica"]}}]}},"aggs":{"agg_blokken_nested.beschikbaarheid":{"terms":{"field":"blokken_nested.beschikbaarheid","size":2500,"order":{"_term":"asc"}}},"nested_aggs":{"nested":{"path":"blokken_nested"},"aggs":{"nested_aggs":{"filter":{"bool":{"must":[{},{"terms":{"blokken_nested.beschikbaarheid":["J"]}}]}},"aggs":{"agg_blokken_nested.beschikbaarheid_buckets":{"terms":{"field":"blokken_nested.beschikbaarheid","size":2500,"order":{"_term":"asc"}},"aggs":{"items":{"reverse_nested":{}}}}}}}}}},"agg_terms_collegejaar":{"filter":{"bool":{"must":[{},{"terms":{"faculteit_naam":["Faculteit der Natuurwetenschappen, Wiskunde en Informatica"]}},{},{"nested":{"path":"blokken_nested","query":{"bool":{"must":[{}]}}}}]}},"aggs":{"agg_collegejaar_buckets":{"terms":{"field":"collegejaar","size":2500,"order":{"_term":"desc"}}}}},"agg_terms_blokken_nested.periode_omschrijving":{"filter":{"bool":{"must":[{},{"terms":{"collegejaar":[2020]}},{"terms":{"faculteit_naam":["Faculteit der Natuurwetenschappen, Wiskunde en Informatica"]}}]}},"aggs":{"agg_blokken_nested.periode_omschrijving":{"terms":{"field":"blokken_nested.periode_omschrijving","size":2500,"order":{"_term":"asc"},"exclude":"Periode: [0-9][0-9]-[0-9][0-9]-[0-9][0-9][0-9][0-9]"}},"nested_aggs":{"nested":{"path":"blokken_nested"},"aggs":{"nested_aggs":{"filter":{"bool":{"must":[{},{}]}},"aggs":{"agg_blokken_nested.periode_omschrijving_buckets":{"terms":{"field":"blokken_nested.periode_omschrijving","size":2500,"order":{"_term":"asc"},"exclude":"Periode: [0-9][0-9]-[0-9][0-9]-[0-9][0-9][0-9][0-9]"},"aggs":{"items":{"reverse_nested":{}}}}}}}}}},"agg_terms_blokken_nested.aanvangsmaand":{"filter":{"bool":{"must":[{},{"terms":{"collegejaar":[2020]}},{"terms":{"faculteit_naam":["Faculteit der Natuurwetenschappen, Wiskunde en Informatica"]}}]}},"aggs":{"agg_blokken_nested.aanvangsmaand":{"terms":{"field":"blokken_nested.aanvangsmaand","size":2500,"order":{"_term":"asc"}}},"nested_aggs":{"nested":{"path":"blokken_nested"},"aggs":{"nested_aggs":{"filter":{"bool":{"must":[{},{}]}},"aggs":{"agg_blokken_nested.aanvangsmaand_buckets":{"terms":{"field":"blokken_nested.aanvangsmaand","size":2500,"order":{"_term":"asc"}},"aggs":{"items":{"reverse_nested":{}}}}}}}}}},"agg_terms_faculteit_naam":{"filter":{"bool":{"must":[{},{"terms":{"collegejaar":[2020]}},{},{"nested":{"path":"blokken_nested","query":{"bool":{"must":[{}]}}}}]}},"aggs":{"agg_faculteit_naam_buckets":{"terms":{"field":"faculteit_naam","size":2500,"order":{"_term":"asc"}}}}},"agg_terms_cursustype_omschrijving":{"filter":{"bool":{"must":[{},{"terms":{"collegejaar":[2020]}},{"terms":{"faculteit_naam":["Faculteit der Natuurwetenschappen, Wiskunde en Informatica"]}},{},{"nested":{"path":"blokken_nested","query":{"bool":{"must":[{}]}}}}]}},"aggs":{"agg_cursustype_omschrijving_buckets":{"terms":{"field":"cursustype_omschrijving","size":2500,"order":{"_term":"asc"}}}}},"agg_terms_voertalen.voertaal_omschrijving":{"filter":{"bool":{"must":[{},{"terms":{"collegejaar":[2020]}},{"terms":{"faculteit_naam":["Faculteit der Natuurwetenschappen, Wiskunde en Informatica"]}},{},{"nested":{"path":"blokken_nested","query":{"bool":{"must":[{}]}}}}]}},"aggs":{"agg_voertalen.voertaal_omschrijving_buckets":{"terms":{"field":"voertalen.voertaal_omschrijving","size":2500,"order":{"_term":"asc"}}}}},"agg_terms_docenten.docent":{"filter":{"bool":{"must":[{},{"terms":{"collegejaar":[2020]}},{"terms":{"faculteit_naam":["Faculteit der Natuurwetenschappen, Wiskunde en Informatica"]}},{},{"nested":{"path":"blokken_nested","query":{"bool":{"must":[{}]}}}}]}},"aggs":{"agg_docenten.docent_buckets":{"terms":{"field":"docenten.docent","size":2500,"order":{"_term":"asc"}}}}}},"post_filter":{"bool":{"must":[{},{"terms":{"collegejaar":[2020]}},{"terms":{"faculteit_naam":["Faculteit der Natuurwetenschappen, Wiskunde en Informatica"]}},{"nested":{"path":"blokken_nested","query":{"bool":{"must":[{}]}}}}]}},"query":{"bool":{"must":[]}}}')
    res = res.json()
    course_codes = [(res['hits']['hits'][i]['_source']['cursus_korte_naam'], res['hits']['hits'][i]['_source']['cursus'], res['hits']['hits'][i]['_source']['coordinerend_onderdeel_oms']) for i in range(len(res['hits']['hits']))]
    return course_codes

subj_codes = get_subject_codes()

for (subj_name, subj_code, study) in tqdm(subj_codes):
    txt = scrape_subject(subj_code)

    filename = 'subjects/%s/%s_(%s)' % (study, subj_name, subj_code)
    os.makedirs(os.path.dirname(filename), exist_ok=True)
    f = open(filename, 'w+')
    f.write(txt)
    f.close()
scrape_subject('NWI-MOL086')
