After following this course, the student will be able to:
<ul>
	<li style="text-align:justify">Know how to use a handheld spectrometer.</li>
	<li style="text-align:justify">Define a research project and question.</li>
	<li style="text-align:justify">Apply experimental design and create a measurement plan.</li>
	<li style="text-align:justify">Analyse the data using basic chemometric data analysis methods and translate the results into experimentally-relevant information.</li>
	<li style="text-align:justify">Communicate these results orally and written to an audience of peer scientists/students</li>
</ul>
 Traditional methods of quantifying chemicals in a wet chemistry lab are usually time-consuming, destructive and/or use expensive equipment. Recent development of <span style="color:#333333">low-cost visible-shortwave Near InfraRed (NIR) instruments has allowed fast and non-invasive measurements. Moreover, handheld spectrometers allows the measurements to take place outside the lab, e.g. in the supermarket or at home. These handheld spectrometers have applications in food quality, polymer recycling or even textile fraud detection. During the course the students will set up their own research project and develop an analysis strategy. We encourage students from all fields of Natural Sciences to participate, as the principles raised in the course apply to all branches of quantitative research.</span><br />
<br />
 Experimental design: Statistics (NWI-MOL028) or Data and Programming (NWI-MOL150) or equivalent. In week 3 the student will present its research plan (unevaluated). In week 7/8 the student will present their results in a final presentation. The final presentation will be graded (20%) and the written report (75%).