
<div>After this course you are&nbsp;acquainted with working on a chemical lab and you are able to individually:</div>

<ul>
	<li>use volumetric glassware and analytical balances to make solutions of known concentration in a precise and accurate way.&nbsp;&nbsp;</li>
	<li>design and quantitatively perform a simple calibration experiment.&nbsp;&nbsp;&nbsp;</li>
	<li>employ single-beam and double-beam UV-VIS spectrometers for measuring spectra and for quantitative experiments.&nbsp;</li>
	<li>perform basic liquid chromatography and / or gel electrophoresis experiments.&nbsp;&nbsp;</li>
	<li>critically interpret the results of simple quantitative chemical experiments using the appropriate statistical methods.</li>
	<li>choose an appropriate analysis technique to solve a basic analytical (chemical) problem.</li>
</ul>
 This practical course is aimed at strengthening, applying and extending the knowledge of <em>quantitative</em> chemical analysis techniques obtained in MOL121 and at making a start with acquiring the laboratory skills of a molecular scientist. At the start of the course, the focus wil be on attaining a number of basic skills (weighing, pipetting, titration, making a dilution series, etc.). Later on, these skills will be applied in experiments of one day in which simple chemical analyses are conducted. Apart from that, much attention will be given to preparing experiments, record-keeping in laboratory notebooks, reporting, error analyses and - importantly - knowledge of safety rules. The latter will be tested in a digital test at the beginning of the course.<br />
<br />
 In the case of not being able to attend one or more practical courses/lab days due to corona measures, the course coordinator will decide if the student is obligated to re-take the missed meeting and how this will take place.<br />
<br />
You are obliged to:
<ul>
	<li>hand in / have handed in a good digital passport photo of yourself on the first day of the lab course, if one is not (yet) available from Osiris or if the quality of the Osiris photo is too low. Handing in the photo will be facilitated by the education institute;</li>
	<li>hand in / have handed in a debit mandate for broken or missing glassware and other equipment. The debit mandate will be provided by the education institute; handing it in will be facilitated by the education institute.</li>
	<li>possess a durable laboratory notebook (bound and with a hard cover; can be bought via the education institute);</li>
	<li>possess a white labcoat made of 100% cotton (can be bought via the education institute);</li>
	<li>possess proper safety glasses (can be bought via the education institute);</li>
	<li>possess a dish cloth / tea towel for drying glassware (can be bought via the education institute).</li>
</ul>
