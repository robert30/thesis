
<div>After having completed a literature thesis, you are able to:</div>

<ul>
	<li>use primary literature databases to research a topic in any of the sub-disciplines of Chemistry, Molecular Life Sciences, or (Natural) Science</li>
	<li>identify relevant and higher impact scientific papers on a topic, and critically evaluate and extract the major findings</li>
	<li>summarize the major findings and conclusions of selected papers concisely and accurately in a written report</li>
</ul>
 
<div>The aim of a literature thesis is to evaluate the competence of a student to compose an informative document on a topic that is new for the student and do so in a limited period of time. The thesis should be based on scientific literature.<br />
You can compare a literature thesis with a scientific review paper.</div>

<div>A literature thesis is usually linked to a master internship: in that case the topic will be chosen by or together with a member of the scientific staff of the department where the internship is performed. &nbsp;But the topic should be different from the subject of your internship.<br />
The supervisor of a literature thesis can be another person than the internship supervisor. A second reviewer is not required.</div>

<div>In some cases (e.g. if you take an internship outside the faculty of science) you can ask any member of the academic staff in a research group mentioned in the list of approved departments for internships (elsewhere in this prospectus) for a topic and to supervise your thesis.</div>

<div>A literature thesis is written in English and will usually contain up to 15-20 pages. The standard outline is: title page, contents, summary, introduction, some chapters with figures and/or tables, conclusion, list of references (including article titles). The reference list includes only work that served as a direct source of information and that is actually cited in the text.</div>

<div>The thesis should be written within (the equivalent of) four weeks (6 ec). With your supervisor you plan a date on which you submit a first version of the thesis and a date for submitting the final version.</div>

<div>You can submit one first version of the thesis that will be commented upon by the supervisor and will receive a preliminary mark.<br />
You can then make improvements and submit a final version to be graded.<br />
<br />
<strong>Instructional Modes</strong></div>
