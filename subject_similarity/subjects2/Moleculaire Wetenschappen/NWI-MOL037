At the end of the course, students should be able to:
<ol>
	<li>Have a basic understanding of the organisation and funding of scientific research.</li>
	<li>Have a basic understanding of the current problems and tensions in the organisation of science, including the publication system, science funding and evaluation.</li>
	<li>Have a basic understanding of the challenges in the relation between science and society, with an appreciation for the difference of perspective involved.</li>
	<li>Have a thorough understanding of research integrity and of its importance for your own research, also as a student.</li>
	<li>Form your own well-founded opinion on the current state of research and universities and on the importance of the values that are at stake.</li>
</ol>
<br />
<style type="text/css">body { font-size: 9pt;
</style>
 Science is not just about experiments, theories and knowledge. Science is also the work of people, working together in organisations, cooperating teams, international networks. All of this is only possible because there is a society that is willing to invest part of its surplus in research, either as tax money or as private investment. In return, citizens and their organisations expect things: contributions to innovations that will improve our lives; sollutions for pressing world problems such as climate change, malaria or cancer; knowledge about who we are, where we come from, where we might be going to.<br />
<br />
None of this is obvious or simple. The organisation of science is difficult and complex and wherever people work together, things sometimes go wrong. The scientific publication system of research journals is in crisis, while society is demanding access to all the knowledge that has been collected through public investment. While scientists sometimes think they have discovered promising new techniques to improve crops or generate car fuel from plants, they are sometimes baffled that citizens do not embrace such new discoveries with great enthusiasm, but meet them with hesitation or even outright refusal. The continued resistance against genetic modification, the decade-old objections against nuclear energy, or the unexpected resistance against bio-fuels are all cases in point. Research and its potential public benefits require careful fostering.<br />
<br />
As a future scientist, but also as a citizen, you need to be aware of these issues; first, simply for selfish reasons. Even with the best and most honest intentions, scientists who do not know about the challenges that await them from the social side of science can get into serious trouble. Scientists have ruined their careers by making over-enthusiastic promises about research results they could not meet, such as claiming they had found a cure for HIV/AIDS. Or they have antagonised society by ignoring public concerns about the consequences of genetic modification. Perhaps most dramatic of all, some scientists have been expelled from science because they gave in to the temptation to tweak data or plagiarise work from others, under the research system’s constant pressure to be ‘productive’. Perhaps most importantly of all, if you want your work to contribute to a better world, you need to understand that society is an increasingly critical partner.<br />
<br />
These are more than just individual challenges. Second, as scientist and citizens, we together face difficult questions about science and how to organise it. We not only need to find ways to do our job and ‘stay out of trouble’, but we also have the responsibility to care for this wonderful public resource that is scientific research and all the immense benefits it has produced. Currently, there are several threats to this common treasure: misconceived attempts to manage science, shady journals that try to steal research money, or the growing economisation of science. In spite of enormous public benefits and enormous growth in science, public investment in research is low and there is pittle public attention for the problems of science.<br />
<br />
However, in the end, we not only want to keep ourselves and science out of trouble: we also want to make science better. Finding ways out of the current troubles of research is a big challenge, but also an exciting puzzle. What can we do to make science better? What would a better science look like? And in order to find that out, we probably first need to ask what we mean by ‘better’ and perhaps even what we mean by ‘science’.<br />
<br />
That leads to the central question fo the course:
<div><strong>How could we make science better?&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></div>
This course explores the social side of science: the organisation of research and its current problems, but also the interactions between research and its patrons in society. In seven weeks, we will focus on a series of questions. All of these questions contribute to the central problem for this course: How could we make science better?<br />
&nbsp;<br />
That is what we will actually do: develop ideas about how to make science better. In order to get there, we will address smaller, more specific questions and themes every week:<br />
&nbsp;
<ol>
	<li>What is science? +&nbsp;Science and its problems.</li>
	<li>What is ‘good’? +&nbsp;Can we trust scientists?</li>
	<li>What is bad? +&nbsp;Misconduct and integrity</li>
	<li>Is science independent? +&nbsp;Controversies over risks and dangers.</li>
	<li>Who knows? +&nbsp;Forms of knowledge, citizen science.</li>
	<li>Is science fair? +&nbsp;Access to science and rewards.</li>
	<li>What are universities for? +&nbsp;Presentation of project and evaluation.</li>
</ol>
&nbsp;

<h1>Course format</h1>
The course consists of:

<ul>
	<li><strong>Seven weekly lectures</strong>: the lecturer presents the material for a basic understanding of key concepts, processes and theories.</li>
	<li><strong>Seven weekly seminars</strong>: in two smaller groups, divided during the first lecture, we discuss specific issues and make exercises around the week’s topic.<br />
	Attendance is obligatory.&nbsp;<strong>For some of the exercises, a laptop or tablet will be very useful.</strong></li>
	<li><strong>Mini-assignment:</strong>your assignment is to explore and understand one of the current problems of science (or universities) and come up with a proposal for improvement.<br />
	The assignment is split in small weekly tasks.</li>
	<li><strong>A written exam</strong>, testing both understanding and application (lecture and seminar).</li>
</ul>
<br />
<strong>Instructional Modes</strong><br />
<style type="text/css">body { font-size: 9pt;
</style>
