
<div>After this course the student is able to:</div>
<br />
<strong>Cellular trafficking and transport</strong><br />
· Describe the role of signal sequences in the transport of soluble and membrane proteins to the ER, mitochondria and nucleus<br />
· Describe the different types of endocytosis and intracellular transport routes between ER, Golgi, plasma membrane, endosomes, lysosomes and autophagosomes at the molecular level<br />
· Explain the role of the cytoskeleton and cytoskeletal motor proteins in vesicle trafficking<br />
<em>Keywords: Cytoskeleton (microtubules) and myosins; protein routing and transport; including membrane/vesicle trafficking (endocytosis and exocytosis)</em><br />
&nbsp;<br />
<strong>Cellular signaling </strong><br />
· Have knowledge of the molecular basis of cell biological signaling principles<br />
· Describe the basal signal transduction and regulatory systems (integration, feedback, cross-talk) based on key concepts<br />
· Be familiar with the mechanisms through which several types of receptors convert extracellular signals on the cell wall to an intracellular chemical change<br />
· Understand general models of signal transduction and regulatory systems and apply this knowledge for a basic understanding of developmental disorders like cancer<br />
<em>Keywords: RTK, G-protein coupled signaling including calcium signaling, ion channels/transporters, steroid signaling (“four main routes”), cross talk, feedback mechanisms, integration of signaling </em><br />
&nbsp;<br />
<strong>Cellular energy homeostasis</strong><br />
· Be familiar with the mechanism of cellular energy production and regulation<br />
· Understand the aberrant biochemical processes in cells of patients with inherited malfunction of the mitochondrial energy production system<br />
· Apply a basic understanding of nonlinear dynamics to build mathematical models of biological systems<br />
<em>Keywords: Glycolysis, TCA, Oxphos; coupling and regulation; mitochondrial dysfunction in patients</em><br />
&nbsp;<br />
&nbsp;<br />
<strong>Approaches to study biochemical and signaling processes</strong><br />
· Be familiar with the various principles on which biosensors for live-cell and fluorescence microscopy are based<br />
· Be familiar with biochemical and cell biological research methods and their role in determining the function of genes and analysis of cellular processes<br />
<em>Keywords: Microscopy, fluorescent proteins, fluorescent probes, biosensors, expression of genes in cells/cell lines</em><br />
&nbsp;<br />
<strong>Genetic processes</strong><br />
· Be able to describe the different phases of mitosis and meiosis and the role of microtubules in the mitotic spindle and the movement of chromosomes<br />
· Be familiar with the molecular basis of hereditary human diseases and the analysis of family trees<br />
· Be able to calculate the genetic distance between genes and molecular markers on a chromosome map<br />
· Be familiar with genetic technologies&nbsp;and their role in determining the function of genes and other DNA sequences<br />
<em>Keywords: family trees, genetic distance, genetic technologies (GWAS, NGS, RNAi, CRISPR/Cas)</em><br />
&nbsp; This course will introduce you in&nbsp;several important biochemical, genetic and cell biological processes of human cells. Cells are "at work" to live, proliferate and divide. Processes like cellular trafficking, signaling, energy homeostasis and cellular&nbsp;division will be&nbsp;explained&nbsp;and their relation to human diseases revealed. Techniques that are used to study these processes will be discussed (fluorescence probes/proteins and microscopy). In addition you will interpret and present (poster) scientific literature illustrating cells&nbsp;"at work" and the integration of the highlighted processes.
<p>Course keywords: biomolecules, cellular processes, genetics, cellular trafficking, energy homeostasis, signaling, fluorescence and biosensors.</p>

<p>Used formats: lectures, self study assignments, response lectures, tutorial (genetics), use of "classic papers" (to skill in academic reading), computer practical (Matlab), poster presentation.</p>
 The course is open for students Molecular life sciences, Chemistry, Science, and Biology. Some Fundamental knowledge of cell biology, biochemistry and (patho) physiology will be assumed. This is a course in the programme 'Molecular Life Sciences'. Written examination (individual) and poster presentation (group).<br />
Exam provides 75% of the final mark. Exam and poster must be at least 5.5. Course coordinator:<br />
Dr. Helma Pluk<br />
<a href="mailto:helma.pluk@radboudumc.nl">helma.pluk@radboudumc.nl</a>