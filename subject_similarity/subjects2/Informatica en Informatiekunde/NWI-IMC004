
<div>At the end of the course you can build a compiler for a small programming language in a well structured manner, and you know and can use the required algorithms and tools. You can create a grammar for a programming language, build a parser for it, perform static analysis, create an abstract syntax tree, and generate code.</div>
 
<div>It is important to know how compilers work because many applications actually act as a compiler: they translate input written in some language to output in some other. So, compiler construction is a basic discipline for any computer scientist.</div>

<div>The construction of a compiler is a natural meeting place of many computer science fields. You work with finite automata, grammars, parsers, tree structures and graphs, recursive algorithms, typing systems, code generation algorithms, to name a few.</div>

<div>In this course you will stepwise construct, in your own favourite language, a basic compiler for a small but representative language, SPL (Simple Programming Language). The main steps are 1) the construction of a parser for SPL which generates an abstract syntax tree; 2) the implementation of a type checker / type inferencing system for SPL; 3) a code generation for a simple stack machine.</div>

<div>As final project, you either choose&nbsp;to extend SPL with new features, or to generate code for some concrete&nbsp;machine architecture.<br />
<br />
