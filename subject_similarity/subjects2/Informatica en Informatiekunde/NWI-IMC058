After taking this course you will be able to train and use deep neural networks, and you will be able to understand and develop more complicated deep architectures and learning algorithms.<br />
&nbsp;<br />
After completing this course, the student<br />
* is able to implement deep learning algorithms from scratch.<br />
* will be able to train and use deep neural networks.<br />
* knows different network architectures and training algorithms, and knows when they are appropriate to use.<br />
* is able to read and understand academic literature about deep learning. Deep Learning is a flavor of machine learning that uses deep artificial<br />
neural networks, meaning networks with many layers and often millions of<br />
parameters. Over the last couple of years Deep Learning has shown huge<br />
successes in different applications such as image and speech<br />
recognition, game playing agents, image synthesis, computational<br />
biology, etc.<br />
&nbsp;<br />
In this course you will learn both how to apply Deep Learning to solve<br />
problems, as well as how these deep neural networks are implemented and<br />
how they work. We will treat many different architectures, and show<br />
which ones are appropriate in which situations. Because this is a very<br />
broad field that is continuously developing, we will not be focusing on<br />
any specific application, but rather lay the groundwork on which all<br />
deep learning techniques are built. The student is expected to have a working knowledge of<br />
&nbsp;<br />
* Linear algebra (vectors, matrices, matrix multiplication)<br />
* Calculus (derivatives, chain rule)<br />
* Probability theory<br />
&nbsp;<br />
Some programming experience is required, but experience with python is<br />
not expected.<br />
&nbsp; The final grade is determined by<br />
* Written exam (80%)<br />
* Practical exercises (20%)<br />
&nbsp;<br />
The grade for the written exam must be at least a 5.<br />
&nbsp;