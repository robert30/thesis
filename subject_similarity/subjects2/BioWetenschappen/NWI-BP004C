
<div>At the end of this course the student will be able to:</div>

<ul>
	<li>describe the distinguishing characteristics of the major animal clades;</li>
	<li>understand the evolutionary and phylogenetic relationships among and between different animal groups;</li>
	<li>describe structure - function relationships of the major animal clades at organ and&nbsp;tissue level;</li>
	<li>compare and contrast the adaptations required for living in interaction with a specific environment;</li>
	<li>develop the ability to perform directed dissections and independent study of animal specimens representative for&nbsp;major phyla or classes.</li>
</ul>
 This course focuses on the evolution and adaptations of animals. The core theme&nbsp;<em>evolution</em>&nbsp;is exemplified by a comparative morphological and anatomical approach. The core theme&nbsp;<em>adaptation</em>&nbsp;is filled in by giving special attention to changes in body plans that evolved during the transitions of life from water to land (and into the air).<br />
<br />
Topics addressed in this course include:<br />
Evolution and phylogeny<br />
Body plans and embryonic development<br />
Unicellularity versus multicellularity<br />
Radial versus bilateral symmetry<br />
Coelom formation and segmentation<br />
The superphylum Lophotrochozoa<br />
The superphylum Ecdysozoa<br />
Protostomia versus Deuterostomia<br />
Body plans and organ systems in vertebrates<br />
Adaptations of the skin to different environments<br />
Evolution and adaptations of skeleton and muscles<br />
Evolution and adaptations of the digestive system and the urogenital system<br />
Evolution and adaptations of the vascular system and the respiratory system<br />
<br />
 No specific prior knowledge is required. The exam will be taken on computer (Cirrus). Exam questions will cover the entire course matter and thus address topics from both lectures and practicals. The practicals are mandatory. Attendance will be registered.<br />
<br />
In the case of not being able to attend one or more practical courses/lab days due to corona measures, the course coordinator will decide if the student is obligated to re-take the missed meeting and how this will take place.<br />
&nbsp;