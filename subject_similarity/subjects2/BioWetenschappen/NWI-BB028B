At the end of this course, you are able to:
<ol>
	<li>Recognise basic connections and tensions between <strong>biology and society</strong> through historic examples, in order to better appreciate such tensions in the current practice of biology.</li>
	<li>Recognise and appreciate the <strong>variety of styles</strong> and organisation of research in research in reference to basic examples from the history of biology.</li>
	<li>Recognise and understand the <strong>importance of social processes</strong> (such as fame, selective attention) for the development of research, at a basic level.</li>
	<li>Understand how <strong>different approaches to history</strong> and current concerns affect how history is presented, based examples from the history of biology, at basic level.</li>
	<li>Refer to the <strong>basic canon</strong> in the history of biology, including key names and crucial stories that are essential to the identity of a biologist (such as Darwin or Mendel).</li>
</ol>
 The history of biology is rich and full of remarkable stories, which are often surprisingly relevant for the present. For example, some theoretical puzzles and tensions in taxonomy that date back to the 18<sup>th</sup> century remain unresolved today. Similarly, there are exciting analyses of the problematic assumptions at the historic roots of evolutionary theory. History also offers us important warnings, such as for what can go wrong once totalitarian regimes determine what is good science and what is not. With the right perspective and the right kind of questions, the history of biology can offer instructive insights for fundamental theoretical problems, for the sometimes difficult relations between biology and society, and even offer a glimpse of half-forgotten knowledge. History also offers reflection on what is probably the toughest question of the discipline: what actually <em>is</em> biology?<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; It may sound like a dreadful cliché, but <strong>the aim of this course is to <em>learn</em> from the past</strong>. The course will not present you with an endless stream of historic facts or isolated funny anecdotes (although some of the history of biology can be quite entertaining). The stress will be on the interpretation of history: we <strong>will try to understand <em>patterns and relations</em></strong> in the development of biology, rather than list who discovered what, where, and when. To this end, the course is constructed around three main points: biology is <strong>diverse, </strong>biology is always <strong>connected to society</strong>, and biology <strong>has made mistakes </strong>from which we can learn a lot. For these purposes, the course will focus on the history of biology since the 18<sup>th</sup> century, where the most concrete connections with the current practice of biology can be made.<br />
With respect to diversity, the course will offer an overview of <strong><em>styles</em> in doing biology</strong>, which will show that, historically, there were many different ways of doing biology. For example, whereas current biology is often dominated by laboratories and molecular biology, biology also has a rich heritage of a taxonomical style of research. In this style, the most important centres of biological research were museums of natural history, botanic gardens, and other collections of specimen that were carefully collected, often on long and adventurous expeditions to exotic places. We will see how the laboratory gained a foothold in biology towards the end of the 19<sup>th</sup> century and then gradually expanded its reign through the rise of genetics and later molecular biology.<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; We will not only look at the development of biological ideas and research practices, but pay special attention to the development of the <strong>relation between biology and society</strong>. We will find out where biologists got their research funds in the past, how biological thinking incorporated ideas from wider culture, but also how biology had a profound societal impact itself. We will follow biologists around as they tried to improve agriculture, fought infectious diseases, or became activists calling for nature conservation and environmental protection.<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; If we want to learn from history, then we should not only focus on the success stories and show cases, such as the discovery of the structure of DNA, or revered heroes such as Charles Darwin. We should also have the courage to see <strong>the dark side of biology’s history</strong>, such as the flirtations with eugenics or racism. For it is only when we have the courage to look in the dark corners that we may find the means to confront such monsters, should they once again rear their ugly heads.<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; While the course does not require you to learn a lot of dates and names by heart, there is a basic list of names and stories any biologist should know, as part as your professional identity and frame of reference: the <strong>canon</strong> of biology. After all, how can you be a biologist and not know about Darwin, Mendel, or Watson&amp;Crick. While any canon is somewhat aribitrary, this course too contains a list of essential names and stories in their historic setting.<br />
<br />
&nbsp;<br />
<strong>In eight lectures, the following topics will be addressed:</strong>
<ol>
	<li>History of biology: why and how?<br />
	(Dissecting history from the Greeks to the Scientific Revolution)</li>
	<li>The classification of nature<br />
	(Herbals, Linnaeus and taxonomy, natural history)</li>
	<li>Evolution<br />
	(Lamarck, Darwin, evolutionary biology)</li>
	<li>The rise of the laboratory<br />
	(From alchemy to Pasteur and the experimental style)</li>
	<li>From <em>generation</em> to genetics<br />
	(Inheritance, Mendel, fruit flies and eugenics)</li>
	<li>The molecularisation of biology<br />
	(The discovery of DNA, but also biology under Stalin)</li>
	<li>Agriculture and medicine<br />
	(How biology changed the world, and the world changed biology)</li>
	<li>The arrival of ‘the environment’<br />
	(Ecology, nature conservation, environmental protection, regulation)</li>
</ol>
To get an idea of what the course is about, you can also look at this video sample of the lecture about Darwin and evolution:&nbsp;http://www.science.ru.nl/wh/hisbio3pt1/<br />
<br />
