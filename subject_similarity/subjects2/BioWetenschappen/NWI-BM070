
<div>At the end of the course, the student:
<ul>
	<li>can describe the replication cycle and molecular properties of RNA viruses.</li>
	<li>understands how cellular and tissue tropism affects virus pathogenesis.</li>
	<li>understands the molecular basis of virus diversity and its implications for epidemiology and intervention strategies.</li>
	<li>can describe the life cycle of <em>Plasmodium falciparum.</em></li>
	<li>understands the differences between vaccine-induced and naturally acquired immunity to malaria.</li>
	<li>is able to describe the life cycle of <em>A. fumigatus</em> and the main human diseases it may cause.</li>
	<li>understands the epidemiology and clinical implications of triazole resistance in <em>A. fumigatus</em>.</li>
	<li>can describe ideal drug properties.</li>
	<li>understands the molecular basis for drug resistance.</li>
	<li>can describe different classes of vaccines and identify advantages and disadvantages of them.</li>
	<li>understands the mechanisms of adjuvantia in vaccination.</li>
	<li>can describe the major differences in next-generation sequencing technologies.</li>
	<li>can weigh pros and cons of different molecular diagnostic approaches.</li>
</ul>
</div>
&nbsp; 
<div>Microorganisms are a leading cause of disease. In this course, we will discuss different classes of microbes of medical importance. As several courses within the <em>Microbiology</em> MSc program focus on the biology of bacteria, we will discuss viruses, fungi and parasitic protozoa. Focusing on important representatives in each class, the biology, pathogenesis, diagnosis, treatment, and prospects for vaccine development will be presented.<br />
The course consists of lectures, interactive lectures, self-study assignments, and group assignments. Assignments will be posted on BrightSpace.</div>
<br />
&nbsp; Knowledge of important eukaryotic cell biological processes, including transcription, translation, endocytosis, as described in molecular biology text books, such as Stryer Biochemistry or Alberts Molecular Biology of the Cell. Knowledge of immunology at the BSc level. In case of knowledge gaps, students are encouraged to consult&nbsp;relevant text&nbsp;books or other resources.<br />
&nbsp; Written exam (100%) and&nbsp;group assignments&nbsp;(pass/fail) This course is mandatory for students in the Microbiology specialization of the MSc Biology. Students from other MSc programs can be placed as well. Please inquire with the course coordinator dr. Ronald van Rij (<a href="mailto:ronald.vanrij@radboudumc.nl">ronald.vanrij@radboudumc.nl</a>) about possibilities to do so.