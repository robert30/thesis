
<div>&nbsp;Upon successful completion of this course, the student
<ul>
	<li>can perform basic energy systems analysis (energy and mass balances, scaling and learning curves, basic micro-economic theory)</li>
	<li>understands the principles of modelling energy systems</li>
	<li>has experience with energy modelling in an educational model</li>
	<li>understands the basic assumptions, uncertainties and limitations of energy modelling</li>
	<li>understands the role of energy models in the policy cycle</li>
</ul>
</div>
 
<div>When working in the energy sector, whether it is in a power company, with the Ministry of Economic Affairs, in an oil and gas company, a bank financing energy investments, a renewable energy technology developer or an international organization, projecting what will happen to the energy sector is key. Major decisions on investments and policies involving many billions of euros depend on assumptions and projections of the availability of resources, energy prices, future economic conditions and energy demand.&nbsp;Such projections&nbsp;and decisions are often based on quantitative energy models, which can take many forms. Decision-makers in the public or private sector&nbsp;need to be&nbsp;able to appreciate what energy models can and cannot calculate, and have an understanding&nbsp;of&nbsp;the results they produce.<br />
<br />
This course provides students&nbsp;with a practical introduction to, and basic experience with different types of energy modelling.&nbsp;</div>

<div>The course will discuss the following topics:</div>

<ul>
	<li>Basic energy analysis</li>
	<li>Mass and energy balances</li>
	<li>Learning curves, scaling laws, basic micro-economic theory</li>
	<li>Common types of energy models, e.g, bottom-up, top-down, optimisation, simulation, etc.</li>
	<li>Modelling the electricity market</li>
	<li>Integrated assessment models</li>
	<li>Application of models in energy policy-making</li>
</ul>
 For students in the Science, Management and Innovation track: the Energy and Climate course (FMT022) should be completed. For students not in the SMI-track, it is recommended to have completed an energy-related course, such as a Huygens college on energy, Energy and Climate course (FMT022), or the BSc course Energy and Sustainability (GCSE002). For students outside the Faculty of Science, this course requires you to perform various calculations, so alongside the aforementioned courses, basic quantitative skills are required. The grade will consist of an exam (2/3) and on a group assignment (1/3).<br />
<br />
&nbsp;