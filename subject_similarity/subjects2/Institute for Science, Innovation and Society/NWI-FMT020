
<ul>
	<li>You will know what the bio-economy is in terms of physical characteristics, resources, processes and feedstocks, its trends across industrial sectors, and its economic, societal and policy context.</li>
	<li>You will understand the hopes and criticisms of the bio-economy, and be able to formulate your own views on it.</li>
	<li>You will know the relation between the bio-economy and other societal trends towards sustainability, such as the circular economy.</li>
	<li>You will learn of first-hand practical experiences of actors in the bio-economy and understand their challenges and opportunities. &nbsp;&nbsp;</li>
</ul>
 
<div>A Schematic view of the topics:</div>
&nbsp;

<table border="1" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td>
			<div><strong>Sources</strong></div>
			</td>
			<td>
			<div><strong>Enabling technology</strong></div>
			</td>
			<td>
			<div><strong>Application</strong></div>
			</td>
			<td rowspan="7">
			<div><em>Group project: Market questions</em></div>
			</td>
		</tr>
		<tr>
			<td rowspan="2">
			<div>Biomass concepts</div>
			</td>
			<td rowspan="2">
			<div>Chemistry</div>
			</td>
			<td>
			<div>Bio-energy</div>
			</td>
		</tr>
		<tr>
			<td>
			<div>Bio-materials</div>
			</td>
		</tr>
		<tr>
			<td colspan="3">
			<div>Sustainability of the bio-economy and related concepts</div>
			</td>
		</tr>
		<tr>
			<td colspan="3">
			<div>Policy support schemes and societal debate of the bio-economy</div>
			</td>
		</tr>
		<tr>
			<td colspan="3">Business models in the bio-economy and circular economy
			<div>&nbsp;</div>
			</td>
		</tr>
		<tr>
			<td colspan="3">
			<div>Innovation in the bio-economy: experiences of market parties</div>
			</td>
		</tr>
	</tbody>
</table>
<br />
<br />
This course introduces students to the exciting world of the emerging bio-economy. The bio-economy entails the large-scale replacement of the fossil resources of much of the world’s current energy and synthetic materials with renewable, biological and/or natural resources. It encompasses a wide range of industrial sectors, ranging from energy production, chemical processing and agriculture to building and waste management.
<div><br />
Initially enthusiastically embraced in circles of science, industry and policymaking, the bio-economy is currently facing&nbsp;public resistance, reluctant entrepreneurs and&nbsp;wavering policymakers. This course will make students familiar with the promise of the bio-economy, in terms of necessity, technology and impact, but also with its downsides and the criticism by various parties. It will also show some&nbsp;practical problems by inviting actors in the field to explain what they are up against.&nbsp;<br />
<br />
This course has a broad&nbsp;focus on many aspects of the bio-economy, including links to various other current&nbsp;approaches towards more&nbsp;sustainability. It does not include&nbsp;in-depth information on e.g. chemistry or technology, but rather provides an overview of different technologies and their relevance for the overall bio-economy development. This broad focus allows students to get a good impression&nbsp;about the societal debate around the bio-economy and sustainabilit,y and understand the complex links between various disciplines and societal actors herein.<br />
<br />
