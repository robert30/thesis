
<ul>
	<li>The student understands the most important physical and chemical processes governing molecules in space, such that they can interpret astronomical observations and laboratory experimental results.</li>
	<li>The student knows experimental and observational methods and can work with them.</li>
	<li>The student can apply this understanding to gain knowledge about scientific problems.</li>
	<li>The student can write a proposal for an experimental or observational facility.</li>
	<li>The student can collaborate in a team for the preparation of such a proposal</li>
</ul>
 
<div>This course discusses part of the interdisciplinary topic of astrochemistry, focusing on molecules in interstellar space. Galaxies contain many different molecules, either in the gas phase or as part of solids and ices. Although few in number, they play a crucial role in many physical and chemical processes in the interstellar medium in between the stars, e.g. gas heating/cooling, dense cloud dynamics, and, ultimately, the formation of life.<br />
In this course, we will discuss:
<ul>
	<li>Properties of molecules in interstellar space</li>
	<li>Observational techniques</li>
	<li>Experimental techniques: spectroscopy and reaction kinetics</li>
	<li>Theoretical models (chemistry, hydrodynamic, radiative transfer, physical)</li>
	<li>Gas phase molecules and molecules in ices and comets</li>
	<li>Star formation, molecular clouds, interstellar dust</li>
</ul>
</div>

<div>&nbsp;</div>
 Third year elective course in the Astronomy, Chemistry and Science curricula. Either astronomy or chemistry 1<sup>st</sup>/2<sup>nd</sup> year courses are helpful. The examination of this course consists of two parts:<br />
(1) a written exam based on the knowledge learned in the lectures and tutorials<br />
(2) a project proposal written in small groups This course is geared towards science, chemistry and astronomy students.