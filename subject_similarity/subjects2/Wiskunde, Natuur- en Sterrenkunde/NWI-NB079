
<p><strong>Aims:</strong></p>

<ul>
	<li>To understand how astrophysical systems can probe physics in the most extreme conditions.&nbsp;</li>
	<li>To appreciate the kinds of astrophysical objects -- white dwarfs, neutrons and black holes -- where these conditions are most naturally found.</li>
	<li>To develop a knowledge of the sources of power in these systems.</li>
	<li>To have a knowledge of recent breakthroughs and central unanswered questions in the field.&nbsp;</li>
</ul>
 Astrophysical environments can probe regions with extremes of temperature, density and gravitational field, well beyond those that can be reached in any laboratory setting. This in turn provides a route to testing how physics works in these extreme conditions: What holds dense objects up against gravitational collapse? What happens when this fails? How does matter behave at extreme temperature and pressure, or when accelerated to close to the speed of light? Does general relativity work in the strongest gravitational fields? How can we find and measure the properties of black holes? In this course we will investigate these questions, and more, providing insight into physics and astronomy at its most extreme, highlighting recent developments and looking to the future.&nbsp;<br />
&nbsp; Second year BSC courses in physics and astronomy. Standard written Exam.