
<div>The students are able to acquire knowledge of a new topic by autonomously studying suitable literature.</div>

<div>The students are able to present their topic in a way comprehensible for their fellow students who have not studied the topic themselves.</div>

<div>The students pick up new mathematical concepts from presentations of their fellow students.</div>

<div>The students gain experience in working in teams, and presenting their work in several ways.</div>
<div>The students are able to present newly acquired mathematical knowledge orally and written in a coherent way.</div> 
<div><strong>Important Note:</strong> The presence at the seminar sessions is compulsory for all students taking part in the seminar.</div>


<div>In the first part of this seminar students work on a topic in teams of 3-4 persons. The topics can be chosen from a list of proposals to be published on Brightspace. Each proposal provides a short introduction, a guide to the literature and suggestions of issues to address. The goals of the seminar are manifold; each team acquires deep knowledge of their own subject, by working in a team the members benefit from each other's strengths. The entire group of students gets acquainted with a wide range of new mathematical ideas, by following the presentations of fellow students. It lies in the hands of the participants to make this a rewarding experience, by providing an attractive and accessible series of lectures. Moreover the students are expected to actively participate in the lectures of other groups.</div>

<div>Each group will present its topic during two seminar sessions of 90 minutes each, so every member will give one presentation of 45 minutes. The general setup is that the teams work largely autonomously on their topics. They themselves should decide how deep they dig into the subject and which specific questions they address. If they get stuck at some point, they are welcome to ask assistance from staff members (including postdocs and PhD-students). However, there will be no coaching on a regular basis.</div>

<div>It is the responsibility of the team that the presentations are comprehensible for their fellow students. This requires on the one hand that the team has to gain a substantial understanding of its topic, and on the other hand that sensible choices have to be made for the presentations (e.g. on the level of the technical details to discuss).<br />
At the end of each seminar session, the other teams will be asked to briefly evaluate the given presentation. These evaluations and the impressions of the instructors will be used to provide feedback to the presenting team.<br />
Before giving the lectures, we expect the students to plan their lectures carefully. For this we will take a look at several aspects of a successful mathematics presentation. Moreover, the students are required to hand in a 1-2 page planning of their lecture to the instructors just before the lecture. This will be taken into account in the evaluation by the instructors. Moreover, each of the students will have to write a self-evaluation of their own lecture based on various inputs.<br />
<br />
During the second semester, the students are divided into two bigger groups according to the track which they have chosen. Each group will work on a&nbsp;specific and rather substantial piece of literature (e.g. a recent monograph or survey paper). The goal is then to organize a mini-course of 1 hour per week around this topic, taught by the students themselves . This means that each student will need to present a small part of the material to his/her peers, and to provide a polished written account (of at most 5 pages) of his/her contribution. For the lectures in the second semester a set-up similar as for the lectures in the first semester will be used. The organisers of the seminar will be available for help and advice, but it is the responsibility of the students themselves to ensure that the course (i.e. both the oral and written content) is coherent, well-organized and accessible. Attendance for both mini-courses will be mandatory for all students, and they will be required to provide individual feedback for their peers.<br />
&nbsp;</div>

<div>The final component of the Master seminar, which should take place during the first term, will be to lay out the organisation of the entire MSc programme for each individual student. This will be done in one or two meetings of the student with the study advisor and the programme director, leading to a written plan for the rest of the programme.<br />
<br />
The catalogue of suggested topics contains references for the various topics<br />
&nbsp;</div>
