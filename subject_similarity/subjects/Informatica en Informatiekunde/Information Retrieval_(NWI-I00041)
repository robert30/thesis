





The objective is that participants in the course


	are familiar with the classic retrieval models
	understand the limitations and assumptions associated with these models
	have insight and proficiency in the design and construction of search engines
	are familiar with the standard evaluation methods for IR systems
	are familiar with interaction techniques to support searchers in their quest for information
	have an understanding of how the searcher's context and behaviour can be used to enhance retrieval effectiveness
	have gained familiarity with recent scientific literature in this field






 





While the rise of the internet hashelped strengthen the field of Information Retrieval (IR), the area stretches far beyond plain websearch, as a discipline situated between information science andcomputer science.In 1968, Gerard Salton defined information retrieval as "a field concerned with the structure, analysis, organization, storage, searching, and retrieval of information". Even though the area has seen many changes since that time and made a tremendous impact (who has never used a search engine?!), that definition is still accurate.
IR takes the notion of "relevance" as its coreconcept. As the scope of IR is limited to those cases where computerstry to identify therelevanceof information objects given a user'sinformation need (as opposed to humans doing that, the common scenarioin information science), perhaps "ComputationalRelevance" would havebeen a better term for the research in this area.

In this course, we cover the following aspects of Information Retrieval:


	How do people search for information, and how can this be formalized?
	How can we take advantage of term statistics, structure and annotations to capture the meaning of texts?
	How can these elements be combined in order to find "relevant" information?
	What techniques are necessary to scale to large text collections?

 Master