
As our daily lives depend increasingly on digital systems, the reliability of these systems becomes a crucial concern, and as the complexity of the systems grows, their reliability can no longer be sufficiently controlled by traditional approaches of testing and simulation. It becomes essential to build mathematical models of these systems, and to use (algorithmic) verification methods to analyse these models. In model checking, specifications about the system are expressed as (temporal) logic formulas, and efficient symbolic algorithms are used to traverse the model defined by the system and check if the specification holds or not. As model checking is fully automatic, requiring no complex knowledge of proof strategies or biases of simulation tools, it is the method of choice for industry-scale verification.

This course introduces several variants of model checking, and techniques and tools in particular:


	Explicit-state and symbolic algorithms for model checking linear-time (LTL) and branching-time (CTL) temporal logics for finite machines
	Symbolic model checking using BDDs
	Bisimulation abstraction techniques
	Model checking in machine learning applications
	PCTL for Markov chains and Markov decision processes
	Abstraction techniques for probabilistic model checking
	Counterexamples for probabilistic model checking
	Model checking tools (NuSMV and PRISM)
	Applications of model checking for analysis of distributed algorithms and for fault-tree analysis

