
At the end of this course, the student is able to


	reason and argue about what type of algorithms and efficient source code to develop and apply to tackle real-life problems;
	understand the basic principles underlying effective machine learning methods;
	use and adapt state-of-the-art machine learning algorithms to tackle a challenge;
	properly evaluate a machine learning algorithm's performance in a real-life context.

 
Machine learning addresses the fundamental problem of developing computer algorithms that can harness the vast amounts of digital data available in the 21st century and then use this data in an intelligent way to solve a variety of real-world problems. Examples of such problems are recommender systems, (neuro) image analysis, intrusion detection, spam filtering, automated reasoning, systems biology, medical diagnosis, microarray genomics, and many more. The goal of this course is to learn how to tackle specific real-life problems through the selection and application of state-of-the-art machine learning algorithms, by entering international machine learning competitions organized at Kaggle.

 Bachelor course "Data Mining". Group work, flash talk presentations, and reports. Individual performance will also be assessed through peer review. Examples of running machine learning competitions can be found at http://www.kaggle.com. If none of the ones actually running is appropriate, we will organize our own competition using data related to ongoing research at the Radboud University. Students interested in the mathematical backgrounds of machine learning are advised to do the course Statistical Machine Learning, students interested in background on deep learning are advised to do the course Deep Learning.