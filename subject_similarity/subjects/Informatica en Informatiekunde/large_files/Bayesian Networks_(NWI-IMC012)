
At the end of this course, you will be able to demonstrate knowledge of the following theoretical concepts:


	Foundations of Bayesian networks and other types of probabilistic graphical models.
	Model testing, statisticalequivalence,and parsimoniousness (Occam's razor).
	Message passing and other foundations of probabilistic inference.
	Key causal inference concepts such as intervention and confounding.



Further, you will have acquired the following practical skills:


	Build a Bayesian network model of a problem domain that you are familiar with.
	Use inference algorithms for probabilistic reasoning in Bayesian networks.
	Statistically evaluate the model fit of a Bayesian network to a given dataset.
	Use structure learning algorithms to generate plausible network structures for given datasets.
	Use a Bayesian network to help answer causal inference questions from observational data --that is, learn how to tackle questions such as "will getting a Master's degree increase my future salary?"

 Bayesian networks are powerful, yet intuitive tools for knowledge representation, reasoning under uncertainty, inference, prediction, and classification. The history of Bayesian Networks dates back to the groundbreaking work of Judea Pearl and others in the late 1980s, for which Pearl was given the Turing Award in 2012.

Bayesian networks are used in many application domains, notably medicine and molecular biology. This course will cover the necessary theory to understand, build, and work with Bayesian networks. It will also introduce how Bayesian networks provide a much needed foundation for causal inference, giving rise to what is sometimes called the "causal revolution".

