

After the course, you will be able to:

	Define research and enumerate the criteria by which the quality of research is judged.
	Carry out literature search using professional tools, and create an inventory of related work; reflect critically on the results presented in research literature.
	Formulate research questions.
	Apply frequently used methods for gathering data (both qualitative and quantitative); determine in which situations the methods are applicable.
	Apply an understanding of research ethics to ensure that a research study adheres to ethical standards.
	Exercise critical thinking skills to avoid common traps in reasoning about or carrying out research.
	Set up a research plan following a professional format.
	Present the ideas in a research paper as well as your own ideas effectively to fellow researchers.

Give feedback to research colleagues, and providing them with advice and suggestions.

 
The course is about planning and carrying out research. The concept of `research’ is interpreted broadly, and includes:

	Identifying, analyzing, and solving problems
	using precisely formulated research questions
	that are addressed with the appropriate methods
	so that the research results are reliable and valid.


Questions covered include: What is scientific research? How can we formulate an intangible problem into a good research question? How can we ensure our research is ethical? How does the scientific world work? How can we judge the quality of research results?

You will learn to create your own research study by writing a research plan. You will also master the application of common research methods, with a special emphasis on methods that are not frequently covered in other courses, namely methods for carrying out user studies, human subject research, and for handling stakeholders.

This course contributes to preparing students for the bachelor thesis, but also for their studies and careers beyond the bachelor thesis, in which both critical thinking skills and research skills are important.

