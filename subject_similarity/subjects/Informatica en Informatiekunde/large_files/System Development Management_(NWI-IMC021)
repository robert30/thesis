SDM has the aim that the student, at the end of the course, has all the professional skills of an IT project manager.
This involves

	pro-active communication with stakeholders about project progress and potential issues
	showing leadership both in a team's daily activities and in resolving strategic issues
	taking the role of a team's Scrum Master 
	negotiating both progress issues and project scope with a real client
	taking advantage of modern project management tools both in creating the project results and in tracking progress
	guarding the team synergy as an effective team member
	managing the project risks by estimating these risks and pro-actively preparing mitigating measures.

 
SDM resembles the phase in an IT career in which the project leader takes responsibility for the management of an agile software development project. Within SDM we address the project management aspects of the whole life cycle of an agile system development project: requirements management, system design, development and system implementation, including continuous integration, delivery of the system and communication with a real customer and accountability to superiors.


The course consists of a theoretical and a practical component. The theoretical component offers concepts, methods and tools for modern software development management. This theory is applied in the practical component which is being carried out within "GiP-House", managing students from the "Software Engineering" course.

GiP-House closely resembles a real-life modern software house in which the students of this course perform roles as: Project manager, Scrum Master, Quality manager, Director. These roles can be adjusted depending on the specific situation of a given semester (e.g. number of students). All students work, within the management structure of GiP-House, under the supervision of the student Director(s), with the aim to create an effective and efficient software house management structure.

Instructional modes


	Lecture
	Assignments

 A relevant Bachelor. 
The work is judged based on 3 criteria:


	Managing and leadership of a Scrum team
	Communication with and expectation management of the client
	Quality of reports to superiors

Information on these items is obtained through:


	project reports
	input from teaching assistants on team visits
	client feedback
	peer reviews of team member performance
	cross-team reviews of project performance
	intermediate and final presentations
	final project discussion to answer questions

 The course is taught in English.

It is essential to be present at the first meeting of the course. At that meeting the project teams are formed. It is also highly recommended to register your project preference at the GipHouse website before this meeting.
This will be linked on the Brightspace page for the course.
If you have a very good reason why you cannot be present, then inform the GipHouse directors at directors@giphouse.nl

