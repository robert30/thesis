

	The student understands the basic building blocks of images and the different types of images that are encountered in the real-world (2D/3D, grayscale/color, data types, noise)
	The student can implement basic image analysis algorithms such as thresholding, region growing and connected component analysis
	The student understands and can work with convolutions and filtering operations such as Gaussian blurring, sharpening and mean/median filters
	The student has basic understanding of some machine learning algorithms such as k-Nearest Neighbor
	The student understands metrics and tools to evaluate the quality of image analysis algorithms, such as the F1-score, ROC analysis and the Dice coefficient
	The student is able to explain the mechanisms of deep neural networks and apply them in practice.


 Images are an important source of data in many fields, from industrial engineering to medicine. However, it is far from trivial to analyze this data effectively and use images to make meaningful decisions. In this course, we will start with introducing basic image analysis algorithms and how to implement them yourself. In the final part of the course, we move to state-of-the-art machine learning tools such as convolutional neural networks and how they work in practice. You will learn to apply these algorithms to real-world problems, such as those in medical imaging.

 All 2nd - 3rd year bachelor students, specifically those following the campus-wide minor Data Science
 Some programming experience is recommended, preferably in Python.
 The evaluation will consist of two parts (an assignment and written examination). The assignment (60% weight of the final score) will consist of the implementation of an image analysis solution for a real-world problem. This assignment can be done in teams of two. The students will need to divide the problem into different steps suited for different algorithms and subsequently implement their solution. We will use Jupyter Notebooks as a final deliverable, which combines code to execute the student’s solution in combination with a report. Especially learning objectives 2 - 5 will be tested in this assignment. The written examination (40% weight of the final score) will consist of a multiple choice test in which all theory is covered.

To successfully complete the module, both components must be passed with a minimum of 5.5 each.
 
Lectures, practices and presentations will be given in English. Written reports will be submitted in English.

This course requires that you "Bring your own device” (laptop).

