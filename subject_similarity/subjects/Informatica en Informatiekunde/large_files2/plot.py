import pandas as pd
from scipy.cluster import hierarchy
import matplotlib.pyplot as plt

dst = pd.read_csv('distmatrix_readable2.csv', delimiter=' ', header=None)
print(dst)

Z = hierarchy.linkage(dst.iloc[:, 1:].to_numpy(), 'average')

dn = hierarchy.dendrogram(Z, orientation='right', leaf_label_func=lambda x: dst.iloc[x, 0][:min(dst.iloc[x, 0].find('(')-1, 30)].replace('_', ' ') + '...'[:min(3, 3*max(dst.iloc[x, 0].find('(')-1-30, 0))])
plt.gca().axes.xaxis.set_visible(False)

plt.savefig('fig.png', figsize=(8, 6), dpi=300)
plt.show()
