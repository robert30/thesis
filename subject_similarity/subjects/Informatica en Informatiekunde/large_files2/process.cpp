#include <fstream>
#include <sstream>

using namespace std;

int main(){
    ifstream in("distmatrix_readable.csv");
    ofstream out("distmatrix_readable2.csv");
    string line;
    while(getline(in, line)){
	stringstream str;
	str << line;
	string name = "";
	string word;

	while(str>>word){
	    if(word[0]=='0'){
		out << name << " " << word;
		break;
	    } else {
		if(name != "")
		    name += "_";
		name += word;
	    }
	}

	while(str>>word)
	    out << " " << word;
	out << endl;
    }

}
