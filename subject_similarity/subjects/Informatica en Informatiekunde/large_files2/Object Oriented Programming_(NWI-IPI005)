

This course teaches you to design and implement object oriented programs. This includes:


	using inheritance and interfaces.
	using and extending given classes.
	understanding the memory model of Java.
	using generics.
	using enumeration types as classes.
	understanding and applying collections like lists and the associated iterators.
	applying UML in the design of object oriented programs.
	apply software patterns.
	develop and use JUnit tests.
	use object oriented GUI libraries and event driven programming.
	divide the work over threads, synchronize threads, handle race-conditions, avoid deadlock.


 
Object orientation (OO) offers a framework to design and implement reusable software. We will use Java to illustrate the principles of object orientation.


Objects and their relations provide a conceptual framework that can be used for analysis, design and implementation. In this course you will learn the principles of OO analysis (OOA) and OO design (OOD). You also learn to realize these designs in an object-oriented program.
Abstraction and encapsulation are the most important properties of the object-oriented paradigm. Through these concepts, a representation of the behavior and structure of an object can be given independently of its realization.

This course is aimed at the development of object-oriented design and programming skills. The programming language used is Java. In this language the concept of class is central. A class is a blueprint for the objects, and consists of attributes (the condition of the object) together with operations (the so-called methods) with which we can edit an object.

