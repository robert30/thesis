1. Het concept van statistische ensembles begrijpen: microcanonical, canonical en grand canonical
2. Het begrip entropie in de drie ensembles begrijpen
3. De partitiefunctie en alle basisthermodynamische grootheden voor een gegeven niet-interacterend systeem kunnen berekenen met behulp van zowel kwantum- als klassieke beschrijving
4. Vertrouwd raken met het concept van tweede kwantisering
5. Fermi-Dirac en Bose-Einstein-distributies en hun relatie tot de distributie van Planck en Maxwell-Boltzmann begrijpen
6. Het begrip van de ordeparameter in Landau en Ginzburg-Landau theorie van faseovergangen begrijpen
7. Begrijpen van de concepten van Bose-condensatie en de basisprincipes van superfluiditeit en supergeleiding 
De statistische mechanica ontleent macroscopische wetten van de thermodynamica aan microscopische principes. De afleiding is gebaseerd op wetten van statistieken. De thermodynamische wetten lijken consequenties te hebben van het statistische principe dat microstaten met dezelfde energie in een volledig geïsoleerd en ergodisch systeem met dezelfde waarschijnlijkheid worden gerealiseerd.

De cursus geeft een inleiding tot statistische mechanica en richt zich daarom voornamelijk op (i) evenwichtseigenschappen en op (ii) verschillende modelsystemen. Niettemin worden ook begrippen als tweede kwantisatie en Ginzburg-Landau theorie van fase-overgangen geïntroduceerd.

Deze cursus behandelt volgende onderwerpen:
Ensemble-theorie en relatie tot thermodynamica, ergodiciteit
Micro-canonieke, canonieke en grand-canonieke ensembles van statistische fysica
Klassiek ideaal gas, ideale paramagnetische vaste stof, Debye-vaste stof en ideale magneet
Maxwell-Boltzmann distributie en equipartition
Bose- en Dirac-distributies, elementen van tweede kwantisatie
Black-body straling
Ideaal Bose-gas en Bose-Einstein-condensatie
Ideaal Fermi-gas
Fase-overgangen: Landau-theorie
Systemen met interacties: model, virale expansie, van der Waals gas
Ginzburg-Landau theorie van superfluiditeit en supergeleiding

