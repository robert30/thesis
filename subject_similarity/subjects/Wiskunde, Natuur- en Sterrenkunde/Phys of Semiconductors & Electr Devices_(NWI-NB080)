After completing this course, the student should:
• Understand the key characteristics of semiconductors and be aware of the role ofsemiconductor physics in condensed matter physics and in industry.
• Recognise and be able to quantitatively desribe important crystal lattice structuresand their reciprocal lattices.
• Understand and be able to assign Miller indices to label crystal planes and directions.
• Understand and be able to show how electronic bandstructure arises when atoms are
arranged in a periodic lattice.
• Recognise direct and indirect bandgaps and understand the energy and momentumconservation requirements associated with excitation across the bandgap.
• Understand the concepts of the chemical potential and the Fermi energy, and theirrole in determining the properties of a semiconductor.
• Be able to determine the carrier density, density of states and mobility of chargecarriers in semiconductors, and be aware of how these quantities can be measured experimentally.
• Understand the concept of doping in semiconductors.
• Quantitatively understand how the bandstructure and properties of semiconductorscan be tuned using doping and electric field, and qualitatively understand ways of tuning with other parameters.
• Understand what is meant by a ‘device’.
• Understand the properties and function of p-n junctions, and how this translates touse in various diodes, transistors and sensors.
• Understand how dimensionality affects the properties of a semiconductor, and beable to determine the density of states for 3-, 2-, and 1-dimensional systems.
• Be aware of the importance of surfaces and interfaces, how reduced dimensionalityunderlies many semiconductor devices, and how confinement can lead to quantisationand novel effects.
• Understand how tunnelling devices work, particularly the operation of a tunnel diode.
• Quantitatively understand the process of optical absorption in semiconductors, andhow this leads to devices such as LEDs, lasers, photodetectors and solar cells.
• Understand the concept of, and be able to determine, quantum efficiencies.
• Be aware of some current development directions on LED’s and solar cells.
• Be aware of some practical challenges in developing and manufacturing real devices,and also of possible methods to address these challenges.
• Be aware of some of the state-of-the-art fundamental research directions that havegrown from semiconductor research and industry. 
Introduction to Semiconductors:

Special characteristics;
Important examples.


The crystal lattice and basic band structure:

Real and reciprocal space, nomenclature;
Periodicity in a solid;
Conduction and valence bands;
Band structure along different crystal directions;
Energies and energy gaps.


The Fermi energy:

Chemical potential;
The Fermi-Dirac distribution.


Charge carriers:

Carrier density, density of states;
Transport and mobility;
Doping, tuning, and making a ‘device’.


Devices (physics and applications):

p-n junction, diodes, transistors (bipolar junction, field effect);
Ionisation detectors;
Three-, two- and one-dimensional systems;
Bulk versus surface, interfaces;
Dots and wires;
Confinement and quantisation;
The quantum Hall effect, possibilities with field effect devices;
Quantum tunnelling and tunnelling devices.


Optical absorption in semiconductors:

Light emitting diodes, quantum efficiency;
Lasers;
Photodetectors and solar cells.


Issues with real devices in real materials.

What is coming now?

Multifunctionality, energy efficiency, new materials, new devices;
New physics?


The course is loosely based on Chapters 1-4 and short sections of some later chaptersof Semiconductor Devices, Physics and Technology by S. Sze and M-K. Lee (third edition), as well as material that can be found in many introductory condensed matterphysics textbooks, for example:


	Solid State Physics, N.W. Ashcroft and N.D. Mermin.
	Introduction to Solid State Physics, C. Kittel.

Other reference material will be recommended where it is relevant for specific parts ofthe course.

 

	Introduction to Quantum Mechanics, NP022B.
	Electricity and Magnetism, NP037.
	Electromagnetism, NB002D (runs at the same time as this course)

 1 written exam, 1 oral exam: combined 100 % of assessment 
