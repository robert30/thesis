After following this course the student

	understands the physical processes playing a role in advanced stages of stellar evolution,
	is able to discuss the late stages of evolution of stars of different masses,
	understands the processes of orbital evolution and mass exchange in binary systems and their stability,
	can describe the main evolution paths followed by binary stars and is able to relate known classes of binaries to these evolution paths,
	can use an existing computer code to compute stellar evolution models, and is able to interpret the results and to write a report on the outcome,
	is able to read and interpret a research paper in the field and present this to his/her fellow students in a short talk.

 
This course covers the advanced stages of evolution of stars, both single and in binary systems. The first part deals with the physical processes that determine how stars of different masses evolve and end their lives, in particular the importance of mass loss, convection and other mixing processes, and rotation. The evolutionary life cycles of stars of different masses will be discussed in detail: the advanced stages of nuclear burning as a luminous red giant or AGB star, the final supernova explosion or the ejection of a planetary nebula, and the endpoints of stellar evolution: white dwarfs, neutron stars and black holes.


The majority of stars are part of binary or multiple systems, which display an enormous variety of properties. The study of binary evolution links the many observed types of binary stars together. The second part of the course treats the evolutionary processes occurring in close binaries, such as tidal interaction and mass transfer, which can have drastic effects on the binary orbit and on the evolution of the stars themselves. We will use this to derive the main binary evolution channels and see how the various observed types of binary system fit into this evolutionary framework.

 Bachelor in Physics or Astrophysics, with a course covering the fundamentals of Stellar Evolution. Assessment of the course consists of three parts:

	a selection of hand-in assignments (30%)
	a presentation on a relevant research article (30%)
	a written report on the computer project (40%)

A minimum score of 5 for each part is required to pass the course. This course will be given annually.