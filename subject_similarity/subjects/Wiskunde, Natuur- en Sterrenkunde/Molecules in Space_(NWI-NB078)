

	The student understands the most important physical and chemical processes governing molecules in space, such that they can interpret astronomical observations and laboratory experimental results.
	The student knows experimental and observational methods and can work with them.
	The student can apply this understanding to gain knowledge about scientific problems.
	The student can write a proposal for an experimental or observational facility.
	The student can collaborate in a team for the preparation of such a proposal

 
This course discusses part of the interdisciplinary topic of astrochemistry, focusing on molecules in interstellar space. Galaxies contain many different molecules, either in the gas phase or as part of solids and ices. Although few in number, they play a crucial role in many physical and chemical processes in the interstellar medium in between the stars, e.g. gas heating/cooling, dense cloud dynamics, and, ultimately, the formation of life.
In this course, we will discuss:

	Properties of molecules in interstellar space
	Observational techniques
	Experimental techniques: spectroscopy and reaction kinetics
	Theoretical models (chemistry, hydrodynamic, radiative transfer, physical)
	Gas phase molecules and molecules in ices and comets
	Star formation, molecular clouds, interstellar dust




 Third year elective course in the Astronomy, Chemistry and Science curricula. Either astronomy or chemistry 1st/2nd year courses are helpful. The examination of this course consists of two parts:
(1) a written exam based on the knowledge learned in the lectures and tutorials
(2) a project proposal written in small groups This course is geared towards science, chemistry and astronomy students.