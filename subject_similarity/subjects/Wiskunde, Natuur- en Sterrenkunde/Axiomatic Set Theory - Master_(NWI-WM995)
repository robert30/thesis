The student will know

	the story of the formalization of set theory
	and some of its heroic results and open questions.

 
We explain how set theory started with Cantor's diagonal argument and his continuum hypothesis. We consider Zermelo's Axiom of Choice and some famous applications, including Zorn's Lemma and the Banach-Tarski-paradox. We then list the axioms given by Zermelo and Fraenkel and develop set theory from them including the theory of ordinals and cardinals. We go on to consider Gödel's constructible sets and his proof of the consistency of the continuum hypothesis. We try to obtain some idea of the forcing method developed by Cohen for proving the consistency of the negation of the continuum hypothesis. If time permits, we also discuss Mycielski's Axiom of Determinacy, and/or Aczel's Anti-foundation axiom.
