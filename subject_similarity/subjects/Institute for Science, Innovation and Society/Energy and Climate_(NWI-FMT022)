
At the end of this course, the student is able to


	Summarise, value, select and critically reflect on energy data and emissions modelling
	Argue his position around the science of climate change
	Formulate an acceptable sectoral agreement taking into account international climate policy processes
	Effectively communicate these aspects orally and in writing attuned to a variety of audience, including researchers, policymakers and investors.

 
The reliance of our global energy system on fossil fuels is influencingthe climate system and global mean temperature. In the 2015 Paris Climate Agreement countries have agreed to limit global warming to "well below 2C" and make efforts to limit warming to 1.5C, but progress is limited while the estimated impacts of climate change seem to get more severe with each report by the Intergovernmental Panel on Climate Change (IPCC), the UN scientific panel on climate change.

This core theme course for the Science, Management and Innovation master specialisation Climate and Energy theme will give a thorough introduction to the full physical chain of events – from human activities and technology to greenhouse gas emissions and concentrations to global mean temperature rise and impacts on the climate system - and to what we can do about it. Basic introductions to energy technology, atmospheric physics and chemistry, and energy systems and policy will be given. Students will practise with current cases in the energy and climate change mitigation fields.


The course is obligatory for SMI-students doing the Climate and Energy theme, and is an elective for master students in Physics, Chemistry, Science, Biology and Mathematics with an interest in energy and climate change issues. Students from other faculties are welcome but should realise that some limited quantitive skills are required.

The following elements will be discussed:


	The atmospheric basis of climate change and why there is climate change scepticism
	Basics of energy andworking with energy data
	Climate change mitigation options, sources of greenhouse gas emissions and energy modelling
	Fossil energy, renewable energy, energy efficiency and CO2 capture and storage
	International and national policy for energy and climate change

 

	Group assignment “Climate scepticism” (16.7%)
	Group assignment “Energy data in countries” (16.7%)
	Group assignment “Climate mitigation policy” (16.7%)
	Individual assignment (50%)

