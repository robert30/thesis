
Upon successful completion of this course, the student

	can perform basic energy systems analysis (energy and mass balances, scaling and learning curves, basic micro-economic theory)
	understands the principles of modelling energy systems
	has experience with energy modelling in an educational model
	understands the basic assumptions, uncertainties and limitations of energy modelling
	understands the role of energy models in the policy cycle


 
When working in the energy sector, whether it is in a power company, with the Ministry of Economic Affairs, in an oil and gas company, a bank financing energy investments, a renewable energy technology developer or an international organization, projecting what will happen to the energy sector is key. Major decisions on investments and policies involving many billions of euros depend on assumptions and projections of the availability of resources, energy prices, future economic conditions and energy demand.Such projectionsand decisions are often based on quantitative energy models, which can take many forms. Decision-makers in the public or private sectorneed to beable to appreciate what energy models can and cannot calculate, and have an understandingofthe results they produce.

This course provides studentswith a practical introduction to, and basic experience with different types of energy modelling.

The course will discuss the following topics:


	Basic energy analysis
	Mass and energy balances
	Learning curves, scaling laws, basic micro-economic theory
	Common types of energy models, e.g, bottom-up, top-down, optimisation, simulation, etc.
	Modelling the electricity market
	Integrated assessment models
	Application of models in energy policy-making

 For students in the Science, Management and Innovation track: the Energy and Climate course (FMT022) should be completed. For students not in the SMI-track, it is recommended to have completed an energy-related course, such as a Huygens college on energy, Energy and Climate course (FMT022), or the BSc course Energy and Sustainability (GCSE002). For students outside the Faculty of Science, this course requires you to perform various calculations, so alongside the aforementioned courses, basic quantitative skills are required. The grade will consist of an exam (2/3) and on a group assignment (1/3).

