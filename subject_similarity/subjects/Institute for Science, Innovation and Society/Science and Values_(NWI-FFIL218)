Students will meet the following objectives at the end of the course:
• Knowledge of different types of values
• Insight into relations between science and values
• Knowledge of and insight into the debates about the question of the value-freedom of science
• A well-argued own view concerning the above issues 
What is the role of values in scientific practice? What should this role ideally be? These questions have been hotly debated in recent years. The traditional view is that science should be value-free: any influence of values should be avoided. The intuition behind this view is that scientific research influenced by (for example) moral or ideological values is biased and cannot produce objective results. A look at scientific practice, however, shows that science is permeated with various types of values. The question is which kind of values are conducive to scientific progress and which ones are detrimental to it. Can we distinguish between legitimate and illegitimate values? How can scientists protect their integrity in situations where society places demands on them (e.g. when they have to advise on policy)? These issues will be discussed from a philosophy of science perspective, with ample attention for concrete cases from scientific practice.

  Bachelor of Science Individual paper (90%), group presentation (10%)
