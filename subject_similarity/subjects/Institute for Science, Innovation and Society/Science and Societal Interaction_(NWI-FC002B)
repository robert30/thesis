
After this course, you…


	understand the basic principles and problems of public participation in scientific issues
	are able to apply this knowledge by means of designing and writing a well-founded participation plan, which entails the goals and different levels of participation, a description of relevant stakeholders, the role of public perceptions, and methods and tools for participation.
	are able to critically reflect on the complexity and interaction of these different dimensions of public participation plans
	are able to present a participation plan in poster format to your fellow students

 

Science communication is usually not a linear process, but is characterized by societal interaction. This course deals with ways to involve citizens and other stakeholders in scientific topics that matter in society, by means of an interactive process of public participation. Examples of research fields which are characterized by a strong link to society are nanotechnology, ‘big data’, sustainable energy sources, nature development, and food, health or life sciences. During the course, questions such as why to involve stakeholders (and why not), who to involve and on which level will be discussed. With regard to the question who to involve, it is important to get a grip on ‘the public’; who are willing and able to participate? What are the benefits for people to participate in such a process? Furthermore, the course will address the role of public perceptions in participation processes. Finally, you will learn about the different methods and tools that can be used in the planning of a participation project, such as debates and focus groups, and how to evaluate a participation process.



During this course, you will design a participation plan on a topic of your interest and present this to your fellow students.

