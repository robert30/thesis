This course provides a brief introduction to research methods from the social sciences and humanities and prepares you for your master thesis project within the Science in Society track.
In the first lecture, the student will be asked to actively reflect on e.g. topics, themes or problems that motive him or her to engage in a Science in Society research project. What do you really want to know? And why? And how can your initial motivation be turned into a concrete research problem, and research question? In this first meeting, the student will also get an introduction into what it entails to do social science research and the differences and similarities with doing research in a beta-field will be discussed.
In the following six lectures of the first quarter, different researchers of the Institute for Science in Society (ISiS) will provide an overview (‘encyclopedia’) of the different methodologies used in social science and the humanities. Please note that this course does not provide detailed insights in each methodology. Instead, it aims to provide an understanding of which methodologies are adequate to deal with a particular research question or research problem.
Each session will also provide the opportunity to practice a bit with the different methodologies.
In the second quarter, the students will start developing their own research question, approach and theoretical framework in close contact (e.g. via feedback sessions) with their supervisor. The supervisor is assigned to each student after the first quarter by the coordinators LK and RvdB. In the second quarter, two former SiS students will also present their experiences in developing a research question, choosing a proper methodology and they will explain how they developed a theoretical framework. In the last two sessions of the second quarter, your fellow students (7th meeting) will provide feedback on your draft research plan. During the last meeting, you will present your final research plan to your fellow students.

Teaching methods
Plenary lectures
Guided group project work
Individual assignments
Student presentation
Individual study period

Pre-requisites
This course is intended for students of the Science in Society track only


Objectives
In this course, you will be introduced to the basics of methodologies in the social sciences and humanities. As such, this course provides the basic framework for your Science in Society master thesis. We have focused on typical challenges and problems students face in their master thesis project. Hence, the learning objectives of this course are:

	You are able to formulate clear and feasible research goals and questions.
	You are acquainted with the difference between quantitative and qualitative research and with the strengths and limitations of different social science methodologies (e.g. interviews, focus groups, literature reviews, questionnaires, discourse analysis)
	You are able to select a methodology that fits your research goals and questions.
	You are able to argue why developing a theoretical framework is necessary for a Science in Society master thesis.
	You are able to make a first step in building a theoretical framework that fits your research goals and research questions.
	You are equipped to make a brief research plan including problem definition, research question, methodology, data collection and data analysis.

 