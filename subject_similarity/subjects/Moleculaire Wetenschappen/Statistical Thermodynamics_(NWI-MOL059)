
Aan het einde van deze cursus kun je:


	microkanonieke, kanonieke en groot-kanonieke partitiefuncties opstellen en interpreteren voor ideaal-gassystemen
	deze partitiefuncties toepassen om thermodynamische grootheden als energie, vrije energie en warmtecapaciteit te verkrijgen
	met de Boltzmann, Bose-Einstein en Fermi-Dirac verdelingen werken om de bezettingsgraad van energieniveaus te bepalen
	evenwichts- en snelheidsconstanten bepalen voor gasfasereacties op basis van molaire partitiefuncties
	de basisconcepten van Monte Carlo simulaties beschrijven om thermodynamische grootheden te bepalen voor niet-ideale systemen
	de achtergrond van viriaal coefficienten voor niet-ideale gassen beschrijven

 
In dit college wordt een fysicochemische basis onder de thermodynamica gelegd door de relatie tussen de microscopische en de macroscopische eigenschappen van een systeem te beschrijven.
De thermodynamica beschrijft relaties tussen macroscopische grootheden als temperatuur, druk, energie, etc., van een systeem (gas, vloeistof, vaste stof of anderszins) bestaande uit zeer veel deeltjes zonder een uitspraak te doen over de eigenschappen van die deeltjes.
De statistische thermodynamica gebruikt de energietoestanden van de atomen en moleculen van het systeem, zoals die volgen uit de theorie of experimenten, om die macroscopische grootheden te berekenen.
De centrale rol van Boltzmann-distributie en partitiesommen wordt in detail behandeld. Vervolgens worden voor een aantal, relatief eenvoudige systemen expliciete berekeningen uitgevoerd. Hierbij komt de relatie tussen spectroscopische en thermodynamische metingen naar voren.
Een deel van de toepassingen zal aan bod komen in een computerpracticum waarin fysicochemische processen zullen worden gesimuleerd met behulp van eenvoudige computerprogramma's. Met de resultaten van die simulaties zullen allerlei thermodynamische grootheden van die processen kunnen worden berekend.

