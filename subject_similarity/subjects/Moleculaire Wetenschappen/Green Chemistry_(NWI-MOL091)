
Main goal:


	You are able to analyze the environmental impacts of chemical processes on the basis of chemical- and process data and with help of scientific environmental models and to provide solutions to environmental questions based on the outcomes.


Subgoals:


	You are capable to reflect on the principles of green chemistry
	You are able to apply the principles of green chemistry within the chemistry
	You are able to independently perform a life cycle assessment on a chemical process, interpret the results and provide improvement options.

 Chemistry is inextricably linked to the currently existing environmental challenges. On the other hand, chemistry is also required to solve these environmental issues. Consider, for example, the production of fuel from CO2, or the use of a catalyst to convert bio-oil to biofuel. Furthermore, there are numerous ways to make current chemical processes ‘greener’. Examples include the reduction of solvents, or replacement of chlorine with oxygen or ozone in bleach processes. In industrial processes, it is crucial to operate as environmentally friendly as possible. In order to do so, one needs to know how to judge the extent to which chemical processes will have an impact on the environment.
This course introduces the twelve principles of green chemistry. Green chemistry, is the design of chemical products and processes that reduce or eliminate the use or generation of hazardous substances. Green chemistry applies across the life cycle of a chemical product, including its design, manufacture, and use. After finishing this course, students are able to organize chemical processes in such a way that the environment will be minimally damaged. For this, life cycle assessment will be used as a method. Moreover, students are able to judge environmental concerns related to chemistry better in their future professional practice.

