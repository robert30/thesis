After completing the course, the student

	is able to apply in an independent manner a variety of metal-catalyzed transformations to chemo-, regio-, and stereoselectively form new CC-, CN- and CO-bonds;
	has developed a basic feeling for general strategies that one can apply for the construction of complex molecular scaffolds that are present in natural products;
	can distinguish between important strategic synthetic steps and synthetically less important transformations in a given reaction sequence.

 Catalytic reactions (involving metal, organo- or biocatalysts) are becoming increasingly important tools for the
synthetic organic chemist. In this course, synthesis strategies for making complex molecular scaffolds will be discussed, as well as a wide range of catalytic transformations that can be efficiently used for the formation of CC-, CN- and CO-bonds in functionalized organic molecules. An overview will be provided of recently
published transition metal-catalyzed reactions (involving a.o. Pd, Fe, Cu, Au and Co), as well as biocatalytic and organocatalytic transformations, and their application in recent syntheses of biologically active compounds and natural products.

Topics
• Strategies in total synthesis
• Palladium-catalyzed reactions
• Cobalt-catalyzed reactions
• Iron-catalyzed reactions
• Gold-catalyzed reactions
• Organocatalysis
• Biotransformations

 This is an advanced synthesis course which requires that you have followed the compulsory master course Advanced Organic Synthesis. Structure of Atoms and Molecules (NWI-MOL079), Molecular Structure (NWI-MOL080), Synthesis of Biomolecules (NWI-MOL047), Organic Chemistry (NWI-MOL095), Organic Chemistry of Biomolecules (NWI-MOL403) (all compulsory), Metal Organic Chemistry (NWI-MOL096) (elective), Stereoselective synthesis (NWI-MOL093) (elective), Advanced organic synthesis (NWI-SM302) (compulsory).