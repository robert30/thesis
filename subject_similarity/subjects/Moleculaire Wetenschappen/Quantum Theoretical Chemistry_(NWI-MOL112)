
After following this course, the student is able to:

- Write down the Hamiltonian of a small molecule or a molecular complex in a suitable coordinate system to describe rotation and vibration
- Use angular momentum theory and group theory to setup suitable basis functions for a variational calculation of molecular energy levels
- Write small computer programs to solve the time-independent and time-dependent Schroedinger equation to compute rotational and vibrational states and compute spectra an other properties of these systems.
- Derive the proper functional form for intermolecular interactions using first and second order perturbation theory
- Compute energy shifts due to external electric and magnetic fields
 
The focus of this course is the molecular quantum mechanics required to describe the nuclear dynamics of gas phase molecules and clusters of molecules, and to compute their spectra and physical properties. Most principles will first be applied to diatomic molecules, but the approach will be mathematical and form the basis of the quantum mechanical study of molecules in general.

Topics
- Coordinate systems, in particular Jacobi coordinates
- The nuclear kinetic energy operator in internal coordinates
- Angular momentum operators and states
- Wigner rotation matrices
- Angular momentum coupling and Clebsch-Gordan coefficients
- Integrals over Wigner rotation matrices
- Elementary group theory and the use of symmetry
- The use of discretization to solve the anharmonic vibrational oscillator

