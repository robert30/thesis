

In this course, you will learn how plants interact with their abiotic and biotic environment. Key are the traits that determine the survival and reproduction of plants. Environmental conditions typically vary in time and space, and plants must respond to these variations to be successful, both at the level of the individual and (ultimately) at the level of the population. Moreover, the less variable but extreme conditions in some habitats amy also challenge plant performance. The ways that plants adapt to these environmental conditions will be the main focus of this course. You will learn the results of acclimation and adaptation of plants in the short and the long term (e.g., analysis of ultimate costs and benefits of responses), but also you will gain insight into the mechanisms that trigger these adaptations, including their regulatory pathways.


The course will be taught based on lectures that will support you in learning the theory. This part will be concluded with an written exam.


The practical part of this course will teach you to define research questions and hypotheses, to work out an experimental protocol, and to perform experiments to test your hypotheses. The time planning for this experimentation period will largely be the responsibility of you and your team. This part will finish with an oral presentation that shows your capacity to integrate theory and experimental results.
​

After successfully completing this course, you will have acquired the following skills:
1. you will be able to identify under which environmental conditions abiotic resources, such as carbon, light, water, nutrients and space, will become limiting for plants, and how these limitations come about
2. you will be able to quantitatively describe and analyse plant growth, and based on this analysis deduct how changes in allocation patterns lead to optimum growth
3. you will be able to predict how adaptation and acclimation of the phenotype (morfologically, anatomically and metabolically) of plants in response to abioc and biotic stress optimizes performance
4. you will be able to explain which variation in resource availability may be expected, and how plants occupy their own niche to compete for these resources
5. you will be able to place variation in plant traits in an evolutionary context
Moreover, you will be able to:

6. design experiments that test hypotheses within the above mentioned theory
7. perform experiments in a team, in which plants are subjected to treatments, and in which the response of these plants is carefully monitored with previously practised measuring methods
8. select a suitable statistical test and apply this to the acquired data
9. present your data orally in a concise way and discuss them in a theoretical context

 This course addresses the ecophysiology of plants. The following topics will be discussed:

	acquisition of resources (how plants intercept their natural resources - nutrients, water, light, space etc.)
	growth and allocation (which factors determine growth, and what is causing differences in biomass and energy allocation to the various organs and processes)
	stress factors and responses (how plants acclimate to changes in their environment and to suboptimal conditions)
	heterogeneity and diversity (how easy is it to apply the concepts learned in the previous lectures to "the real world", where plants interact in vegetation structures and where only rarely one environmental factor is limiting growth

 

	Evolution and Development of Plants
	Plant physiology

Please contact the course coordinator if you haven't done these courses 

	exam, counts for 60%, minimum grade 5.5
	lab class, counts for 40%, minimum grade 5.5

 This course will be taught for two days a week. Attending the days scheduled for the practical part is mandatory, which will occupy a large part of the last two-third of the course. These days typically are full, eight-hour workdays!