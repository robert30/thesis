
After this course:


	Based on the structure, function and growth of a microorganism, you can analyse and predict how a cell functions and which microbial processes and interactions with other (micro)organisms and the environment can occur

	
		You know the history of earth and microbial evolution
		You know the discovery and history of microbiology as a science
		You know the structure and function of a microbial cell
		You know how microorganisms grow and divide
		You know the structure, function, replication and diversity of viruses
		You know microbial symbioses with humans
		You know microbial infection and pathogenesis
		You know a variety of experimental techniques used in (clinical) microbiology
	
	
	You can work in a team; you divide tasks and share results
	You can decide which microbiological techniques to use in order to identify and quantify microorganisms in a given sample
	You can perform microbiological experiments
	You can work with safe microbiological techniques
	You can report your results in a written report
	Based on a scientific topic, you can structure and write a university press release (BIS assignment)

 Microbiology studies microorganisms and viruses from genes to ecosystems. Microorganisms include bacteria, archaea and some eukaryotes. The course Microbiology provides a general introduction to (medical) microbiology and is followed by the courses Microbial Metabolism, Physiology of Microorganisms, Genomics for Health and Environment and Applied and Environmental Microbiology in the learning trajectory “Microbiology” of the BSc Biology. In addition, the course Microbiology can serve as a basis for any other learning trajectory in Biology and Medical Biology.

Theory:
The topics covered in this course are essential for performing (medical) microbiological research. With this knowledge you will be able to explain and predict the interaction of microorganisms and viruses with their environment (other organisms and the inanimate world).

Topics are (Brock 15th edition, in bold the core topics of the course):
Ch1: Introduction to Microbiology: evolution, discovery, microscopy 
Ch2: Microbial cell structure and function
Ch3.1, 3.2: Microbial nutrients &amp; nutrient uptake
Ch5: Microbial growth and its control
Ch7 (not 7.6, 7.7, 7.8): Molecular biology of microbial growth
Ch8: Viruses and their replication
Ch9.1-9.4, 9.8-9.11: Microbial systems biology (genomics &amp; functional omics)
Ch10.1, 10.13: Viral genomics, diversity and ecology
Ch13.1-13.4: Microbial evolution and systematics
Ch24: Microbial symbioses with humans
Ch25: Microbial infection and pathogenesis
Ch28.1-28.8: Clinical microbiology and immunology
Ch29 (examples): Epidemiology (only examples used in lectures)

Practical course:
The practical part of this course consists of three to six (depending on COVID-19 regulations at the time of the course) 4hr-blocks where you perform several different lab experiments in parallel onsite. In addition, the practical course will contain a virtual lab module. The practical course will train you to choose the appropriate microbiological techniques and to perform and analyse microbiological experiments.

During the (lab and virtual) practical course you will 1) learn basic microbiological techniques, 2) culture, identify, quantify and characterise microorganisms, 2) perform a quality control on food, and 3) perform microbial diagnostics. During the practical course you will: keep a lab book, answer preparatory questions, discuss results, write one report (including a feedback round), and you will perform a professional learning attitude (PLA) evaluation to evaluate team work of you and a fellow student. 

Safe Microbiological Techniques:
During the microbiology practical course you will learn how to work with safe microbiological techniques. In addition, there will be a 2h-lecture about this topic. If you pass the practical course and follow the SMT-lectures, you will receive a SMT certificate that can be important for future lab work / internships.

Biology in Society assignment:
In quarter 2, the BIS assignment is imbedded in the microbiology course. The BIS assignment consists of writing a university press release. First you will get a lecture about how to structure and write a press release. Next, you and a fellow student, will write a press release about a scientific publication and give peer feedback on the press release from two other groups. Finally, you will implement the feedback and hand in a final version to the lecturers through Brightspace.
 Level 1 course Your final grade will be based on:

	a multiple-choice Cirrus exam (65%)
	the practical course (25%: of which work attitude &amp; team work (including preparatory questions, PLAs and discussion) 50%, lab book 10% and a written report 40%)
	the BIS assignment (10%)

You must obtain a 5.5 minimum for all three examination parts (BIS, practical course and exam).
 Literature and other

	Brock ‘Biology of Microorganisms’ plus Pearson Mastering Microbiology with Pearson eText, Global Edition, 15th edition, ISBN: 9781292235226
	Practical course manual will be made available via Brightspace
	Lab book: for reporting on the experiments and analyzing and evaluating obtained results
	Lab coat: obligatory in the practical course
	The lecture slides (PDF) will be made available on Brightspace after the lectures
	You have to bring your lab coat, (printed) practical manual, lab book and a microscope to the practical


Teaching methods 


	Lectures (24 hours)
	Lab practical &amp; virtual lab module (6 x 4hr-blocks / 24 hours)
	Group project (BIS assignment) (20 hours)
	Self-study, lab preparations &amp; report writing (34 hours)
	Question session (2 hours)
	Cirrus exam (3 hours)


COVID-19
Depending on the COVID-19 regulations at the time of the course, specific course components may be adjusted, rescheduled or given online. In this case, we will communicate specific changes as soon as possible via Brightspace.

In the case of not being able to attend one or more practical courses/lab days due to corona measures, the course coordinator will decide if the student is obligated to re-take the missed meeting and how this will take place.

