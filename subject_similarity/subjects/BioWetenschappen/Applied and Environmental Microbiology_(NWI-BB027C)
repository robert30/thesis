

	You will learnabout the microorganisms responsible for the conversion of elements at different redox states (e.g. NO3- reduction to N2 or CH4 oxidation to CO2) and key types of energy metabolisms of microorganisms (e.g. denitrification, photosynthesis, methanogenesis, fermentation, ammonium and methane oxidation).
	You will understandthe biogeochemical processes within the carbon, nitrogen, and sulphur cycleand the impact of humans on these cycles.
	You will obtainan overview of the biodiversity of prokaryotes and the evolutionary relations between ecologically relevant species including current theories and concepts concerning microbial evolution.
	You will gain a detailed understanding of molecular ecology in theory and practice.
	You will designand planyour own experiments inteams of two.
	You will applystate-of-the art techniques and tools such as PCR, fluorescence in situ hybridisation, metagenomics and gas chromatography complemented by bioinformaticanalyses.
	You will write a scientific report of the synthesis of your own results in the format of a scientific article, discussyour article with the lecturer and your peers (performed in teams of two).
	You will learnhow to critically read and discuss scientific literature, and present your analyses (performed inteams of two).

 

This course is a must for all biology and MLW students interested in biogeochemical cycles and the role of microorganisms in natural and manmade ecosystems. The lectures and practicals show the importance of unicellular organisms for the biogeosphere and how humans are dependent on their activities: the air we breathe, the water we drink, the temperature of the earth and the occurrence of epidemics. The lectures present many key themes in microbial ecology and focus on the multidisciplinary approach of this field of research. Recent articles from the scientific literature will be discussed during a mini-symposium at the end. The influence of human activity on the environment and human health (medical microbiology from an ecological point of view) will also receive attention.


Practicals: Laboratory classes, in which you work together with a partner, generally take place in the morning and afternoon, with lectures or "werkcolleges" in between. The experimental part will provide you with hands-on experience in several complementary approaches to answer questions in microbial ecology and to study the biogeochemical element cycles: DNA-based microbial community analysis (including in silico phylogenetic analysis), measuring microbial activities, enrichment of microorganisms and visualisation of these by fluorescence in situ hybridisation and microscopy.
The overall approach of this practicum is to keep things as close to "real life" scientific situations as possible. Therefore, you will have a certain degree of freedom and responsibility to plan your own experiments and monitor them with the methods you will learn. Before you can start your experiments, we want to make sure that you understand both the methods you use and the general idea behind the experiments.For this reason, an assignment for each of the experimental approaches has to be answered and discussed with the assistants before you start. Keep in mind that there are often several ways to achieve the same goal, different answers may be correct, things may go wrong and results may not be what you expected. Consider this course as a first taste of these methods. To become really proficient you have to practice much longer, for example by conducting an internship at our department.

As a case study, we have selected a wastewater treatment plant (wwtp). In this wwtp, different clades of microorganisms are used to degrade pollutants to relatively less harmful compounds such as CO2 and N2, or CH4 (biogas) that may be re-used, making it a perfect site to study the elemental cycles. We will measure the rates of several processes in the C and N cycles and will try to detect and identify the responsible microbes. Activated sludge (biomass) from two different treatment steps - anaerobic digestion (granular sludge) and nitrogen removal (flocculent) - will be used.
In this course we will perform three lines of experiments. (1) In week one and two we will start by constructing an inventory of the microbial biodiversity of two different types sludge. (2) Afterwards, we will measure the rates of several biogeochemical processes and try to cultivate the responsible microorganisms. (3) We will detect (or identify) these microorganisms with fluorescent in situ hybridisation (FISH) microscopy. Ultimately we aim to reach an understanding how these three different approaches may be used to understand a complex environment, with the wastewater treatment plant as an example.

Communicating your results to the scientific community is an essential part of scientific practice. Therefore, we will discuss your report drafts in small groups, where you receive feedback from each other and the lecturers to improve your writing skills.

Instructional modes


	Lecture
	Presentation
	Tutorial
	Self-study


 