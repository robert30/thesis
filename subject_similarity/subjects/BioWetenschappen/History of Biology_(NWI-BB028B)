At the end of this course, you are able to:

	Recognise basic connections and tensions between biology and society through historic examples, in order to better appreciate such tensions in the current practice of biology.
	Recognise and appreciate the variety of styles and organisation of research in research in reference to basic examples from the history of biology.
	Recognise and understand the importance of social processes (such as fame, selective attention) for the development of research, at a basic level.
	Understand how different approaches to history and current concerns affect how history is presented, based examples from the history of biology, at basic level.
	Refer to the basic canon in the history of biology, including key names and crucial stories that are essential to the identity of a biologist (such as Darwin or Mendel).

 The history of biology is rich and full of remarkable stories, which are often surprisingly relevant for the present. For example, some theoretical puzzles and tensions in taxonomy that date back to the 18th century remain unresolved today. Similarly, there are exciting analyses of the problematic assumptions at the historic roots of evolutionary theory. History also offers us important warnings, such as for what can go wrong once totalitarian regimes determine what is good science and what is not. With the right perspective and the right kind of questions, the history of biology can offer instructive insights for fundamental theoretical problems, for the sometimes difficult relations between biology and society, and even offer a glimpse of half-forgotten knowledge. History also offers reflection on what is probably the toughest question of the discipline: what actually is biology?
 It may sound like a dreadful cliché, but the aim of this course is to learn from the past. The course will not present you with an endless stream of historic facts or isolated funny anecdotes (although some of the history of biology can be quite entertaining). The stress will be on the interpretation of history: we will try to understand patterns and relations in the development of biology, rather than list who discovered what, where, and when. To this end, the course is constructed around three main points: biology is diverse, biology is always connected to society, and biology has made mistakes from which we can learn a lot. For these purposes, the course will focus on the history of biology since the 18th century, where the most concrete connections with the current practice of biology can be made.
With respect to diversity, the course will offer an overview of styles in doing biology, which will show that, historically, there were many different ways of doing biology. For example, whereas current biology is often dominated by laboratories and molecular biology, biology also has a rich heritage of a taxonomical style of research. In this style, the most important centres of biological research were museums of natural history, botanic gardens, and other collections of specimen that were carefully collected, often on long and adventurous expeditions to exotic places. We will see how the laboratory gained a foothold in biology towards the end of the 19th century and then gradually expanded its reign through the rise of genetics and later molecular biology.
 We will not only look at the development of biological ideas and research practices, but pay special attention to the development of the relation between biology and society. We will find out where biologists got their research funds in the past, how biological thinking incorporated ideas from wider culture, but also how biology had a profound societal impact itself. We will follow biologists around as they tried to improve agriculture, fought infectious diseases, or became activists calling for nature conservation and environmental protection.
 If we want to learn from history, then we should not only focus on the success stories and show cases, such as the discovery of the structure of DNA, or revered heroes such as Charles Darwin. We should also have the courage to see the dark side of biology’s history, such as the flirtations with eugenics or racism. For it is only when we have the courage to look in the dark corners that we may find the means to confront such monsters, should they once again rear their ugly heads.
 While the course does not require you to learn a lot of dates and names by heart, there is a basic list of names and stories any biologist should know, as part as your professional identity and frame of reference: the canon of biology. After all, how can you be a biologist and not know about Darwin, Mendel, or Watson&amp;Crick. While any canon is somewhat aribitrary, this course too contains a list of essential names and stories in their historic setting.


In eight lectures, the following topics will be addressed:

	History of biology: why and how?
	(Dissecting history from the Greeks to the Scientific Revolution)
	The classification of nature
	(Herbals, Linnaeus and taxonomy, natural history)
	Evolution
	(Lamarck, Darwin, evolutionary biology)
	The rise of the laboratory
	(From alchemy to Pasteur and the experimental style)
	From generation to genetics
	(Inheritance, Mendel, fruit flies and eugenics)
	The molecularisation of biology
	(The discovery of DNA, but also biology under Stalin)
	Agriculture and medicine
	(How biology changed the world, and the world changed biology)
	The arrival of ‘the environment’
	(Ecology, nature conservation, environmental protection, regulation)

To get an idea of what the course is about, you can also look at this video sample of the lecture about Darwin and evolution:http://www.science.ru.nl/wh/hisbio3pt1/

