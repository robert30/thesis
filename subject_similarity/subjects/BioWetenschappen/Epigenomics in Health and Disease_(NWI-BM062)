After successful completion of this course you are able to

	compare the latest technologies for epigenetic profiling and choose the appropriate method to investigate a research question related to basic or translational research
	demonstrate how epigenetic knowledge can be translated into clinical practice
	critically evaluate research papers in the field of medical epigenetics by examining their strengths and weaknesses
	compose and orally present a follow-up study or complementary research plan based on a primary research article, including detailed experimental approaches
	defend a proposed research plan, in a scientific discussion

 Contents
This course provides an overview of current concepts of epigenetics in health and disease, with a focus on mechanisms that are misregulated in, or unique to, diseases such as cancer. Various epigenetic features including protein binding sites, DNA methylation and chromatin structure will be discussed in the light of medical applications. The course is largely divided into three modules: (1) Proposal writing (2) Epigenomic technologies (including data integration) &amp; (3) (Mis)regulation of epigenetic mechanisms. The emphasis of the lectures will be on design and controlled execution of various research project in the field of medical epigenomic: how were the research questions formulated?, how did the researchers set up the experiments?, how were the results interpreted?, and what are interesting follow-ups?
These lectures will provide you with a framework that enables you to design your own research proposal. Such a case study represents an important skill for both master theses that will follow after this course. During the afternoons, you will work in groups (of ~ 5 students) to write a research proposal based on a research paper in the field of medical epigenomics. This proposal should include detailed information on a first proof-of-principle experiment. Groups will be coached by one of the lecturers. Additionally, you will orally present the research proposal.

Practical assignment
Writing and presenting a project proposal.

	This proposal should consist of 3-4 printed A4 pages, with 3 figures maximally. One of these has to be a graphical overview of the design of your proposal. The proposal should contain a project for the duration of a PhD project.
	The presentation should be 25 minutes in total, including 10 minutes discussion.

Instructional modes


	Lecture
	Presentation
	Discussion groups
	Tutorial
	Self-study

 Genomics and Big Data, Advanced Molecular Biology, and Functional Genomics (or equivalent). This prior knowledge can be found in Lodish 7thedition chapters 4, 5, 6, 7, 8, 15, 16, 19, 21, 24. In 2020/2021 this 3EC course will be given for the last time. From 2021/2022 on the course will be combined with BM064 to one 6EC course. This course will be taught and examined in English. Due to the mandatory proposal writing and presentations it is very difficult, if not impossible, to participate simultaneously in other activities / courses.