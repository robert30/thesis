After completing the course, you should be able to handle research and management models for environmental, ecological and water issues in internships and jobs, at a basic MSc-level, in particular
1. systematically analyse issues (day 1, 8)
2. critically select model types and orderly follow modelling phases (day 1, 4-7)
3. thoroughly understand and dominant equations and modules (day 2, 4-7)
4. easily estimate of parameter values from overarching principles (day 3, 4-5) 
Relevance
If we prepare an outdoor trip, we check the weather forecast, as projected by meteorological models. Political parties submit their programs for calculation of the expected economic benefits. Mathematical models now also have become indispensable in environmental and ecological sciences. At the BSc level you obtained most information from standard text books. At the MSc level however, you are expected be able to deal with many loose, incomplete and contradictory data. In addition to statistical tools taught in other parts of the curriculum, in this course you will learn to develop and interpret mathematical models, needed for abstract thinking, quantitative assessments and projections of the future.

With the information acquired in this course, graduates should be able to work as:

- researchers that develop models for scientific questions

- consultants that apply models for practical issues

- managers that base their decisions on uncertain model output

Contents
The course starts with an introduction, discussing the relevance, types and stages of modelling. You will be made familiar with a few equations often applied in ecology and environmental science. These equations are the building blocks of models that you build yourself in the second part of the course.
Starting with pre-cooked exercises, you will gradually learn to build your own model for a given system, going through different the stages of model development (design, implementation, sensitivity analysis etc.). We will address different environmental problems, including eutrophication and chemical pollution (food chain accumulation).
The assignments are linked to "hot" ecological concepts, such as "allometric size scaling" and "alternative stable states". Guest-lectures demonstrate how models are used in different settings, ranging from fundamental research to consultancy. Your progress is monitored by the products (answers on questions, models built) handed in during and after assignments.
The course concludes with an exam testing knowledge and skills acquired during all sessions.

Instructional modes


	Lecture
	Tutorial
	Self-study

 BSc Environmental Science(s), Biology, Chemistry, Moleculair Sciences or Natural Sciences. The course is part of the MSc Biology and MSc Environmental Sciences.

This course requires basic mathematical handling skills at the level taught in the BSc Biology and secondary school. In addition, basic use of Excel is required.

If you doubt about your background, please, contact the coordinator. The course is graded with an open book exam.

To be eligible to sit the exam, assignments need to be carried out and uploaded during the course.