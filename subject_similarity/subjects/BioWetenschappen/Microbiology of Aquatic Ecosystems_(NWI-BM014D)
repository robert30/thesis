The course will make you understand the main biogeochemical cycles on earth, exemplified in aquatic ecosystems, and the diversity of microorganisms and microbial energy metabolisms that are driving these element cycles. After having followed this course you will be able to

	understand on a global level the microbial processes involved in aquatic carbon, nitrogen, sulfur and iron cycles
	understand the various forms of (redox) energy metabolism of microorganisms and predict which processes are likely to occur under given circumstances
	choose the most appropriate method(s) currently used in microbial ecology to answer specific questions on the role of microorganisms in aquatic ecosystems, and critically evaluate their advantages and shortcomings
	critically read and evaluate a scientific manuscript, and explain its essence and your judgment to your peers
	collaborate in asmall group to discuss and present scientific work,and incorporateconstructivefeedback in preparation of presentations

Based on this, you are able to formulate relevant research questions and approaches about the role of microorganisms in aquatic ecosystems. This course is an integral part of the Master's specialization Conservation and Restoration Ecology, but also relevant for the MSc specializations Adaptive Organisms,Water and Environment and Transnational ecosystem-based water management. It introduces you to different aquatic ecosystems, their overall functioning and highlights the essential role of microbes in the biogeochemical cycles of nitrogen, sulfur, iron and carbon/methane. Special attention is given to microbial production and consumption of greenhouse gases.

In addition to lectures concerning the element cycles and methods to study microorganisms, you will prepare (in pairs) a presentation on current methods for microbiological research or a case study focusing on the role of microorganisms in different aquatic ecosystems and element cycles.

Course material
Links to relevant literature (scientific papers and Brock - Biology of Microorganisms) and lecture slides will be provided through Brightspace

Course set up (hours per student)
Lectures: 14 hours
Tutorial: 3 hours
Presentations: 8 hours
Individual feedback sessions: 1 hour
Practice exam: 1 hour
QA session: 2 hours
Group project (presentation preparation) &amp; self-study (including tutorial assignment): 35 hours
Oral examination: 0.5 hour




 Knowledge on fundamental aspects of microbiology and biogeochemistry. This knowledge can be acquired by previous courses like Microbiology, Biology of Microorganisms and/or Applied and Environmental Microbiology or by individual study of relevant chapters of the textbook "Brock - Biology of Microorganisms".
 The final mark is composed of the presentation grade (20%) and the oral exam grade (80%). Both grades need to be at least 5.5.

 This course is tailored for students from the Master specializations: Conservation and Restoration Ecology, Adaptive organisms, Water and Environment and Transnational ecosystem-based water management. Students from the Master specialization Microbiology please note that this course has major overlap with other Microbiology master courses in your program!

Note: Depending on the COVID-19 regulations at the time of the course, specific course components may be adjusted, rescheduled or given online. In this case, we will communicate specific changes as soon as possible via Brightspace. In addition, if necessary, we will provide online alternatives for all course components to allow students that cannot be present onsite to follow the course.
