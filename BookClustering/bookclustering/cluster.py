import os
import subprocess
from tqdm import tqdm

s_vals = []
file_size = []
for i in tqdm(range(0, 800, 100)):
    j = 950 - i
    subprocess.run('truncate -s ' + str(j) + 'K ../bookstrimmed/*', shell=True)
    proc = subprocess.run(['ncd', '-c', 'bzlib', '-b', '-d', '../bookstrimmed', '../bookstrimmed'], capture_output=True)
    s_val = None
    # while s_val == None:
    #    s_val = True
        #try:
        #    matr = subprocess.run(['maketree', 'distmatrix.clb'], capture_output=True, timeout=30)
        #except subprocess.TimeoutExpired:
        #    s_val = None
    # s_val = matr.stdout.decode('utf-8')[:-1]
    # while s_val.find('\n') != -1:
    #    s_val = s_val[s_val.find('\n')+1:]
    # s_vals.append(float(s_val))
    file_size.append(j)
print(s_vals)
print(file_size)
